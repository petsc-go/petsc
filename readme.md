PETSc
=====

A [Go](http://golang.org/) package for calling functions from
[PETSc](http://www.mcs.anl.gov/petsc/) (Portable, Extensible Toolkit for
Scientific Computation).

Prerequisites
-------------

PETSc

    export PETSC_DIR=${HOME}/tmp/petsc-3.5.2/
    export PETSC_ARCH=arch-linux2-c-debug
    export LD_LIBRARY_PATH=${PETSC_DIR}/${PETSC_ARCH}/lib
    export PKG_CONFIG_PATH=${PETSC_DIR}/${PETSC_ARCH}/lib/pkgconfig

Install
-------

    export PKG_CONFIG_PATH=$PETSC_DIR/$PETSC_ARCH/lib/pkgconfig

    export C_INCLUDE_PATH=/usr/include/openmpi:$PETSC_DIR/include:$PETSC_DIR/$PETSC_ARCH/include
    export LD_LIBRARY_PATH=/usr/lib/openmpi/lib:$PETSC_DIR/$PETSC_ARCH/lib

    go get -u github.com/rwl/petsc

Docker
------

    $ docker build -t YOUR_DOCKER_ACCOUNTNAME/petsc .
    $ docker login
    $ sudo docker push YOUR_DOCKER_ACCOUNTNAME/petsc
