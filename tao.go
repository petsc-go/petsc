package petsc

/*
#cgo pkg-config: PETSc
#cgo LDFLAGS: -lpetsc -lmpi

#include "mpi.h"
#include "petsc.h"
#include "petsctao.h"

static char** makeCharArray(int size) {
        return calloc(sizeof(char*), size);
}

static void setArrayString(char **a, char *s, int n) {
        a[n] = s;
}

static void freeCharArray(char **a, int size) {
        int i;
        for (i = 0; i < size; i++) {
                free(a[i]);
        }
        free(a);
}
*/
import "C"
import "runtime"
import "unsafe"

/*func initVars() {
}

func Initialize(args []string, file, help string) {
	cargc := C.int(len(args))
	cargs := C.makeCharArray(cargc)
	defer C.freeCharArray(cargs, cargc)
	for i, s := range args {
		C.setArrayString(cargs, C.CString(s), C.int(i))
	}

	errorCode := C.TaoInitialize(&cargc, &cargs, nil/*C.CString(file) /, C.CString(help))
	CheckError(errorCode)

	initVars()
}

func Finalize() {
	errorCode := C.TaoFinalize()
	CheckError(errorCode)
}*/

type TAO struct {
	tao_p      C.Tao
	taoFn      TAOFunction
	taoCtx     interface{}
	taoHessFn  HessianFunction
	taoHessCtx interface{}
}

// NewTAO creates a TAO solver.
func NewTAO(comm MPI_Comm) *TAO {
	t := &TAO{}
	runtime.SetFinalizer(t, destroyTAO)
	errorCode := C.TaoCreate(C.MPI_Comm(comm), &t.tao_p)
	CheckError(errorCode)
	return t
}

func destroyTAO(t *TAO) C.PetscErrorCode {
	return C.TaoDestroy(&t.tao_p)
}

// Destroy destroys the TAO context that was created with NewTAO().
func (t *TAO) Destroy() {
	errorCode := destroyTAO(t)
	CheckError(errorCode)
}

type TAOType string

const (
	TAO_NLS TAOType      = "tao_nls"      // Newton's method with line search for unconstrained minimization.
	TAO_NTR TAOType      = "tao_ntr"      // Newton's method with trust region for unconstrained minimization.
	TAO_NTL TAOType      = "tao_ntl"      // Newton's method with trust region, line search for unconstrained minimization.
	TAO_LMVM TAOType     = "tao_lmvm"     // Limited memory variable metric method for unconstrained minimization.
	TAO_CG TAOType       = "tao_cg"       // Nonlinear conjugate gradient method for unconstrained minimization.
	TAO_NM TAOType       = "tao_nm"       // Nelder-Mead algorithm for derivate-free unconstrained minimization.
	TAO_TRON TAOType     = "tao_tron"     // Newton Trust Region method for bound constrained minimization.
	TAO_GPCG TAOType     = "tao_gpcg"     // Newton Trust Region method for quadratic bound constrained minimization.
	TAO_BLMVM TAOType    = "tao_blmvm"    // Limited memory variable metric method for bound constrained minimization.
	TAO_Pounders TAOType = "tao_pounders" // Model-based algorithm pounder extended for nonlinear least squares.
)

// SetType sets the method for the unconstrained minimization solver.
func (t *TAO) SetType(typ TAOType) {
	tt := C.CString(string(typ))
	defer C.free(unsafe.Pointer(tt))
	errorCode := C.TaoSetType(t.tao_p, tt)
	CheckError(errorCode)
}

// SetInitialVector sets the initial guess for the solve.
func (t *TAO) SetInitialVector(x0 *Vec) {
	errorCode := C.TaoSetInitialVector(t.tao_p, x0.vec_p)
	CheckError(errorCode)
}

type TAOFunction func(TAO, Vec, *float64, Vec, interface{}) ErrorCode

// SetObjectiveAndGradientRoutine sets a combined objective function and
// gradient evaluation routine for minimization.
func (t *TAO) SetObjectiveAndGradientRoutine(fn TAOFunction, ctx interface{}) {
	t.taoFn = fn
	t.taoCtx = ctx
	//errorCode := C.setTaoObjectiveAndGradientRoutine(t.tao_p, unsafe.Pointer(ctx))
	//CheckError(errorCode)
}

type HessianFunction func(TAO, Vec, *Mat, *Mat, interface{}) ErrorCode

// SetHessianRoutine sets the function to compute the Hessian as well as the
// location to store the matrix.
func (t *TAO) SetHessianRoutine(H, Hpre *Mat, fn HessianFunction, ctx interface{}) {
	t.taoHessFn = fn
	t.taoHessCtx = ctx
	//errorCode := C.setTAOHessianRoutine(t.tao_p, H.mat_p, Hpre.mat_p, unsafe.Pointer(ctx))
	//CheckError(errorCode)
}

// SetFromOptions sets various TAOSolver parameters from user options.
func (t *TAO) SetFromOptions() {
	errorCode := C.TaoSetFromOptions(t.tao_p)
	CheckError(errorCode)
}

// Solve solves an optimization problem:
// 	min F(x) s.t. l <= x <= u
func (t *TAO) Solve() {
	errorCode := C.TaoSolve(t.tao_p)
	CheckError(errorCode)
}

var (
	TAOConvergedFATol C.TaoConvergedReason = C.TAO_CONVERGED_FATOL   // f(X)-f(X*) <= fatol
	TAOConvergedFRTol                              = C.TAO_CONVERGED_FRTOL   // |F(X) - f(X*)|/|f(X)| < frtol
	TAOConvergedGATol                              = C.TAO_CONVERGED_GATOL   // ||g(X)|| < gatol
	TAOConvergedGRTol                              = C.TAO_CONVERGED_GRTOL   // ||g(X)|| / f(X)  < grtol
	TAOConvergedGTTol                              = C.TAO_CONVERGED_GTTOL   // ||g(X)|| / ||g(X0)|| < gttol
	TAOConvergedStepTol                            = C.TAO_CONVERGED_STEPTOL // Step size small.
	TAOConvergedMinF                               = C.TAO_CONVERGED_MINF    // F < F_min
	TAOConvergedUser                               = C.TAO_CONVERGED_USER    // User defined.
	// Diverged.
	TAODivergedMaxIts      = C.TAO_DIVERGED_MAXITS
	TAODiverged            = C.TAO_DIVERGED_NAN
	TAODivergedMaxFcn      = C.TAO_DIVERGED_MAXFCN
	TAODivergedLSFailure   = C.TAO_DIVERGED_LS_FAILURE
	TAODivergedTRReduction = C.TAO_DIVERGED_TR_REDUCTION
	TAODivergedUser        = C.TAO_DIVERGED_USER // User defined.
	// Keep going.
	TAOContinueIterating = C.TAO_CONTINUE_ITERATING
)

// ConvergedReason gets the reason the TAOSolver iteration was stopped.
func (t *TAO) ConvergedReason() C.TaoConvergedReason {
	var reason C.TaoConvergedReason
	errorCode := C.TaoGetConvergedReason(t.tao_p, &reason)
	CheckError(errorCode)
	return reason
}
