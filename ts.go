package petsc

/*
#cgo pkg-config: PETSc
#cgo LDFLAGS: -lpetsc -lmpi

#include "mpi.h"
#include "petscts.h"
*/
import "C"
import (
	"runtime"
	"unsafe"
)

type TS struct {
	ts_p C.TS
}

// NewTS creates an empty timestepper.
func NewTS(comm MPI_Comm) *TS {
	ts := &TS{}
	runtime.SetFinalizer(ts, destroyTS)
	errorCode := C.TSCreate(C.MPI_Comm(comm), &ts.ts_p)
	CheckError(errorCode)
	return ts
}

func destroyTS(ts *TS) C.PetscErrorCode {
	return C.TSDestroy(&ts.ts_p)
}

// Destroy destroys the timestepper that was created with NewTS().
func (ts *TS) Destroy() {
	errorCode := destroyTS(ts)
	CheckError(errorCode)
}

type TSProblemType C.TSProblemType

var (
	TSLinear TSProblemType = C.TS_LINEAR
	TSNonLinear = C.TS_NONLINEAR
)

// SetProblemType sets the type of problem to be solved.
func (ts *TS) SetProblemType(typ TSProblemType) {
	errorCode := C.TSSetProblemType(ts.ts_p, C.TSProblemType(typ))
	CheckError(errorCode)
}

type TSType string//C.TSType

var (
	TSEuler TSType = C.TSEULER
	TSBeuler = C.TSBEULER
	TSPseudo = C.TSPSEUDO
	TSCN = C.TSCN
	TSSundials = C.TSSUNDIALS
	TSRK = C.TSRK
	TSPython = C.TSPYTHON
	TSTheta = C.TSTHETA
	TSAlpha = C.TSALPHA
	TSGL = C.TSGL
	TSSSP = C.TSSSP
	TSARKIMEX = C.TSARKIMEX
	TSROSW = C.TSROSW
	TSEIMEX = C.TSEIMEX
)

// Sets the method to be used as the timestepping solver.
func (ts *TS) SetType(typ TSType) {
	t := C.CString(string(typ))
	defer C.free(unsafe.Pointer(t))
	errorCode := C.TSSetType(ts.ts_p, C.TSType(t))
	CheckError(errorCode)
}

type TSMonitorFunction func(TS, int, float64, *Vec, interface{}) ErrorCode

// MonitorSet sets an additional function that is to be used at every
// timestep to display the iteration's progress.
func (ts *TS) MonitorSet(fn TSMonitorFunction, mctx interface{}, monitordestroy func(interface{}) ErrorCode) {

}

type TSRHSFunction func(*TS, float64, *Vec, *Vec, interface{}) ErrorCode

// SetRHSFunction sets the routine for evaluating the function, where
// 	U_t = G(t,u).
func (ts *TS) SetRHSFunction(r *Vec, fn TSRHSFunction, ctx interface{}) {
	//ts.rhsFn = fn
	//tx.rhsCtx = ctx
	//errorCode := C.setTSRHSFunction(ts.ts_p, r.vec_p)
	//CheckError(errorCode)
}

type TSRHSJacobian func(TS, float64, Vec, *Mat, *Mat, /**MatStructure,*/ interface{}) ErrorCode

// SetRHSJacobian sets the function to compute the Jacobian of F,
// where U_t = G(U,t), as well as the location to store the matrix.
func (ts *TS) SetRHSJacobian(Amat, Pmat *Mat, fn TSRHSJacobian, ctx interface{}) {
	//ts.rhsJacFn = fn
	//ts.rhsJacCtx = ctx
	//errorCode := C.setTSRHSJacobian(ts.ts_p, Amat.mat_p, Pmat.mat_p)
	//CheckError(errorCode)
}

// SetFromOptions sets various TS parameters from user options.
func (ts *TS) SetFromOptions() {
	errorCode := C.TSSetFromOptions(ts.ts_p)
	CheckError(errorCode)
}

// SetInitialTimeStep sets the initial timestep to be used, as well as the
// initial time.
func (ts *TS) SetInitialTimeStep(initial_time, time_step float64) {
	errorCode := C.TSSetInitialTimeStep(ts.ts_p, C.PetscReal(initial_time), C.PetscReal(time_step))
	CheckError(errorCode)
}

// SetDuration sets the maximum number of timesteps to use and maximum time
// for iteration.
func (ts *TS) SetDuration(maxsteps int, maxtime float64) {
	errorCode := C.TSSetDuration(ts.ts_p, C.PetscInt(maxsteps), C.PetscReal(maxtime))
	CheckError(errorCode)
}

// SetSolution sets the initial solution vector for use by the TS routines.
func (ts *TS) SetSolution(u *Vec) {
	errorCode := C.TSSetSolution(ts.ts_p, u.vec_p)
	CheckError(errorCode)
}

// Solve steps the requested number of timesteps.
func (ts *TS) Solve(u *Vec) {
	errorCode := C.TSSolve(ts.ts_p, u.vec_p)
	CheckError(errorCode)
}

// SolveTime gets the time after a call to Solve.
func (ts *TS) SolveTime() float64 {
	var ftime C.PetscReal
	errorCode := C.TSGetSolveTime(ts.ts_p, &ftime)
	CheckError(errorCode)
	return float64(ftime)
}

// TimeStepNumber gets the number of time steps completed.
func (ts *TS) TimeStepNumber() int {
	var iter C.PetscInt
	errorCode := C.TSGetTimeStepNumber(ts.ts_p, &iter)
	CheckError(errorCode)
	return int(iter)
}
