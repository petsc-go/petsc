package petsc

/*
#cgo pkg-config: PETSc
#cgo LDFLAGS: -lpetsc -lmpi

#include "petsc.h"
*/
import "C"

import "runtime"

var (
	ScatterForward      C.ScatterMode = C.SCATTER_FORWARD       // Scatters the values as dictated by the NewScatter() call.
	ScatterReverse                    = C.SCATTER_REVERSE       // Moves the values in the opposite direction than the directions indicated in NewScatter()
	ScatterForwardLocal               = C.SCATTER_FORWARD_LOCAL // Scatters the values as dictated by the NewScatter() call except no parallel communication is done.
	ScatterReverseLocal               = C.SCATTER_REVERSE_LOCAL // Moves the values in the opposite direction than the directions indicated in NewScatter() except no parallel communication is done.
	ScatterLocal                      = C.SCATTER_LOCAL
)

// Scatter is used to manage communication of data between vectors in
// parallel. Manages both scatters and gathers.
type VecScatter struct {
	ctx_p C.VecScatter
}

// NewVecScatter creates a vector scatter context.
func NewVecScatter(xin *Vec, ix *IS, yin *Vec, iy *IS) *VecScatter {
	ctx := &VecScatter{}
	runtime.SetFinalizer(ctx, destroyVecScatter)
	errorCode := C.VecScatterCreate(xin.vec_p, ix.is_p, yin.vec_p, iy.is_p, &ctx.ctx_p)
	CheckError(errorCode)
	return ctx
}

func destroyVecScatter(ctx *VecScatter) C.PetscErrorCode {
	return C.VecScatterDestroy(&ctx.ctx_p)
}

// Destroy destroys a vector scatter.
func (ctx *VecScatter) Destroy() {
	errorCode := destroyVecScatter(ctx)
	CheckError(errorCode)
}

func (ctx *VecScatter) ScatterBegin(x, y *Vec, addv C.InsertMode, mode C.ScatterMode) {
	errorCode := C.VecScatterBegin(ctx.ctx_p, x.vec_p, y.vec_p, addv, mode)
	CheckError(errorCode)
}

func (ctx *VecScatter) ScatterEnd(x, y *Vec, addv C.InsertMode, mode C.ScatterMode) {
	errorCode := C.VecScatterEnd(ctx.ctx_p, x.vec_p, y.vec_p, addv, mode)
	CheckError(errorCode)
}
