package petsc

/*
#cgo pkg-config: PETSc
#cgo LDFLAGS: -lpetsc -lmpi

#include "petscmat.h"
*/
import "C"

type ZMat struct {
	Re Mat
	Im Mat
}

func NewZMat(re, im *Mat) *ZMat {
	return &ZMat{*re, *im}
}

// NewMatAIJ creates a sparse parallel matrix in AIJ format.
func NewZMatAIJ(comm MPI_Comm, m, n, M, N, d_nz int , d_nnz []int, o_nz int, o_nnz []int) *Mat {
	var A *Mat = &Mat{}
	_d_nnz := intSlice(d_nnz)
	_o_nnz := intSlice(o_nnz)
	errorCode := C.MatCreateAIJ(C.MPI_Comm(comm), C.PetscInt(m), C.PetscInt(n),
		C.PetscInt(M), C.PetscInt(N), C.PetscInt(d_nz), &_d_nnz[0],
		C.PetscInt(o_nz), &_o_nnz[0], &A.mat_p)
	CheckError(errorCode)
	return A
}

// NewMatSeqAIJ creates a sparse matrix in AIJ (compressed row) format.
func NewZMatSeqAIJ(comm MPI_Comm, m, n, nz int, nnz []int) *Mat {
	var A *Mat = &Mat{}
	_nnz := intSlice(nnz)
	errorCode := C.MatCreateSeqAIJ(C.MPI_Comm(comm), C.PetscInt(m), C.PetscInt(n),
		C.PetscInt(nz), &_nnz[0], &A.mat_p)
	CheckError(errorCode)
	return A
}

// Destroy destroys a matrix.
func (m *ZMat) Destroy() {
	m.Re.Destroy()
	m.Im.Destroy()
}

// SetFromOptions creates a matrix where the type is determined from the
// options database.
func (m *ZMat) SetFromOptions() {
	m.Re.SetFromOptions()
	m.Im.SetFromOptions()
}

// Mult computes the matrix-vector product, y = Ax.
func (m *ZMat) Mult(x *ZVec) *ZVec {
	return nil
}

// MatMult performs Matrix-Matrix Multiplication C=A*B.
func (m *ZMat) MatMult(B *ZMat, scall MatReuse, fill float64) *ZMat {
	return nil
}

// Conjugate computes the complex conjugate for each vector element.
func (m *ZMat) Conjugate() {
	m.Im.Scale(-1.0)
}
