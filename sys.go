package petsc

/*
#cgo pkg-config: PETSc
#cgo LDFLAGS: -lpetsc -lmpi

#include "petscsys.h"
*/
import "C"

import "runtime"

// Random is a PETSc type that manages generating random numbers.
type Random struct {
	rand_p C.PetscRandom
}

func NewRandom(comm MPI_Comm) *Random {
	r := &Random{}
	runtime.SetFinalizer(r, destroyRandom)
	errorCode := C.PetscRandomCreate(C.MPI_Comm(comm), &r.rand_p)
	CheckError(errorCode)
	return r
}

func destroyRandom(r *Random) C.PetscErrorCode {
	return C.PetscRandomDestroy(&r.rand_p)
}

// Destroy destroys a random number generator.
func (r *Random) Destroy() {
	errorCode := destroyRandom(r)
	CheckError(errorCode)
}

// SetFromOptions configures the random number generator from the
// options database.
func (r *Random) SetFromOptions() {
	errorCode := C.PetscRandomSetFromOptions(r.rand_p)
	CheckError(errorCode)
}

// Value generates a random number.
func (r *Random) Value() float64 {
	var val C.PetscScalar
	errorCode := C.PetscRandomGetValue(r.rand_p, &val)
	CheckError(errorCode)
	return float64(val)
}
