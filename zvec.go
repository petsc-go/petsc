package petsc

/*
#cgo pkg-config: PETSc
#cgo LDFLAGS: -lpetsc -lmpi

#include "mpi.h"
#include "petscvec.h"
*/
import "C"
//import "math"

// ZVec is a complex vector comprised of real and imaginary vectors.
type ZVec struct {
	Re *Vec
	Im *Vec
}

// NewZVec creates a new complex vector using the given communicator.
func NewZVec(comm MPI_Comm) *ZVec {
	re := NewVec(comm)
	im := NewVec(comm)
	return &ZVec{re, im}
}

// NewZVecReal creates a new complex vector with the given real part.
func NewZVecReal(re *Vec) *ZVec {
	//	if re == nil && im == nil {
	//		re = NewVec(CommWorld)
	//		im = NewVec(CommWorld)
	//	} else if re == nil {
	//		re = im.Duplicate()
	//	} else if im == nil {
	//		im = re.Duplicate()
	//	}
	im := re.Duplicate()
	return &ZVec{re, im}
}

// NewZVecPolar creates a new complex vector with the given polar coordinates.
func NewZVecPolar(radius, theta *Vec) *ZVec {
	//radians := true
	real := theta.Duplicate()
	theta.Copy(real)
	imag := theta.Duplicate()
	theta.Copy(imag)

	re := real.Array()
	im := imag.Array()
	r := radius.Array()
	Θ := theta.Array()

	/*if !radians {
		real.forEach(func.multiply(DEG_RAD));
		imag.forEach(func.multiply(DEG_RAD));
	}*/

//	for i, x := range re {
//		re[i] = r[i] * math.Cos(x)
//	}
//	re.forEach(cos)
//	re.forEachWith(r, mult)
//	im.forEach(sin)
//	im.forEachWith(r, mult)

	real.RestoreArray(re)
	imag.RestoreArray(im)
	radius.RestoreArray(r)
	theta.RestoreArray(Θ)

	return &ZVec{real, imag}
}

// NewZVecSeq creates a new complex vector of size n.
func NewZVecSeq(comm MPI_Comm, n int) *ZVec {
	re := NewVecSeq(comm, n)
	im := NewVecSeq(comm, n)
	return &ZVec{re, im}
}

// Destroy destroys a vector.
func (v *ZVec) Destroy() {
	v.Re.Destroy()
	v.Im.Destroy()
}

// Copy - Copies a vector. y <- x
func (v *ZVec) Copy(y *ZVec) {
	v.Re.Copy(y.Re)
	v.Im.Copy(y.Im)
}

// Duplicate creates a new vector of the same type as an existing vector.
func (v *ZVec) Duplicate() *ZVec {
	re := v.Re.Duplicate()
	im := v.Im.Duplicate()
	return &ZVec{re, im}
}

// SetFromOptions configures the vector from the PETSc options database.
func (v *ZVec) SetFromOptions() {
	v.Re.SetFromOptions()
	v.Im.SetFromOptions()
}

// SetSizes sets the local and global sizes.
func (v *ZVec) SetSizes(n, N int) {
	v.Re.SetSizes(n, N)
	v.Im.SetSizes(n, N)
}

// Set sets all components of a vector to a single value.
func (v *ZVec) Set(re, im float64) {
	v.Re.Set(re)
	v.Im.Set(im)
}

// SetRandom sets all components of the vector to random numbers.
func (v *ZVec) SetRandom() {
	v.Re.SetRandom()
	v.Im.SetRandom()
}

// SetType builds a vector, for a particular vector implementation.
func (v *ZVec) SetType(method VecType) {
	v.Re.SetType(method)
	v.Im.SetType(method)
}

// Size returns the global number of elements of the vector.
func (v *ZVec) Size() int {
	return v.Re.Size()
}

// Scale the vector.
func (v *ZVec) Scale(re, im float64) {
	v.Re.Scale(re)
	v.Im.Scale(im)
}

// PointwiseMult provides elementwise multiplication.
func (v *ZVec) PointwiseMult(x, y *ZVec) {
	// (a+bi)(c+di) = (ac-bd) + (bc+ad)i
	//re := x.Re*y.Re - x.Im*y.Im
	re := x.Re.Duplicate()
	re.PointwiseMult(x.Re, y.Re)
	re2 := x.Im.Duplicate()
	re2.PointwiseMult(x.Im, y.Im)
	re.AXPY(-1.0, re2)

	//im := x.Im*y.Re + x.Re*y.Im
	im := x.Im.Duplicate()
	im.PointwiseMult(x.Im, y.Re)
	im2 := x.Re.Duplicate()
	im2.PointwiseMult(x.Re, y.Im)
	im.AXPY(1.0, im2)

	v.Re = re
	v.Im = im
}

// PointwiseDivide provides elementwise division.
func (v *ZVec) PointwiseDivide(x, y *ZVec) {
	// \frac{a + bi}{c + di} = \left({ac + bd \over c^2 + d^2}\right) + \left( {bc - ad \over c^2 + d^2} \right)i.
}

// Abs computes the absolute value of each vector element.
func (v *ZVec) Abs() {

}

// AXPY computes y = alpha x + y.
func (v *ZVec) AXPY(re, im float64, x *ZVec) {

}

// Conjugate computes the complex conjugate for each vector element.
func (v *ZVec) Conjugate() {
	v.Im.Scale(-1.0)
}
