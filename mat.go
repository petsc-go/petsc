package petsc

/*
#cgo pkg-config: PETSc
#cgo LDFLAGS: -lpetsc -lmpi

#include "petscmat.h"
*/
import "C"

import (
	"runtime"
	"unsafe"
)

// PETSc matrix type used to manage all linear operators in PETSc, even
// those without an explicit sparse representation (such as matrix-free
// operators)
type Mat struct {
	mat_p C.Mat
}

// NewMat creates a matrix where the type is determined
// from either a call to SetType() or from the options database
// with a call to SetFromOptions(). The default matrix type is
// AIJ, using the routines NewMatSeqAIJ() or NewMatAIJ()
// if you do not set a type in the options database.
//
// If you never call MatSetType() or MatSetFromOptions() it will
// generate an error when you try to use the matrix.
func NewMat(comm MPI_Comm) *Mat {
	m := &Mat{}
	runtime.SetFinalizer(m, destroyMat)
	errorCode := C.MatCreate(C.MPI_Comm(comm), &m.mat_p)
	CheckError(errorCode)
	return m
}

// Creates a sequential dense matrix that is stored in column major order.
func NewMatSeqDense(comm MPI_Comm, m, n int, data []float64) *Mat {
	A := &Mat{}
	runtime.SetFinalizer(A, destroyMat)
	d := scalarSlice(data)
	errorCode := C.MatCreateSeqDense(C.MPI_Comm(comm), C.PetscInt(m), C.PetscInt(n), &d[0], &A.mat_p)
	CheckError(errorCode)
	return A
}

func destroyMat(m *Mat) C.PetscErrorCode {
	return C.MatDestroy(&m.mat_p)
}

// Destroy destroys a matrix.
func (m *Mat) Destroy() {
	errorCode := destroyMat(m)
	CheckError(errorCode)
}

// NewMatSeqAIJ creates a sparse matrix in AIJ (compressed row) format
// (the default parallel PETSc format).
func NewMatSeqAIJ(comm MPI_Comm, m, n, nz int, nnz []int) *Mat {
	A := &Mat{}
	//cnnz := intSlice(nnz)
	var cnnz C.PetscInt
	errorCode := C.MatCreateSeqAIJ(C.MPI_Comm(comm), C.PetscInt(m), C.PetscInt(n), C.PetscInt(nz), &cnnz, &A.mat_p)
	CheckError(errorCode)
	return A
}

// MatCreateSeqBAIJ - Creates a sparse matrix in block AIJ (block
// compressed row) format.
func NewMatSeqBAIJ(comm MPI_Comm, bs, m, n, nz int, nnz []int) *Mat {
	A := &Mat{}
	//cnnz := intSlice(nnz)
	var cnnz C.PetscInt
	errorCode := C.MatCreateSeqBAIJ(C.MPI_Comm(comm), C.PetscInt(bs), C.PetscInt(m), C.PetscInt(n), C.PetscInt(nz), &cnnz, &A.mat_p)
	CheckError(errorCode)
	return A
}

// Comm gets the MPI communicator for the solver.
func (m *Mat) Comm() MPI_Comm {
	p := uintptr(unsafe.Pointer(&m.mat_p))
	obj := (C.PetscObject)(unsafe.Pointer(p))
	var comm C.MPI_Comm
	errorCode := C.PetscObjectGetComm(obj, &comm)
	CheckError(errorCode)
	return MPI_Comm(comm)
}

// SetSizes sets the local and global sizes, and checks to determine
// compatibility.
func (mat *Mat) SetSizes(m, n, M, N int) {
	errorCode := C.MatSetSizes(mat.mat_p, C.PetscInt(m), C.PetscInt(n), C.PetscInt(M), C.PetscInt(N))
	CheckError(errorCode)
}

// SetFromOptions creates a matrix where the type is determined
// from the options database. Generates a parallel MPI matrix if the
// communicator has more than one processor.
func (m *Mat) SetFromOptions() {
	errorCode := C.MatSetFromOptions(m.mat_p)
	CheckError(errorCode)
}

// SetUp sets up the internal matrix data structures for the later use.
func (m *Mat) SetUp() {
	errorCode := C.MatSetUp(m.mat_p)
	CheckError(errorCode)
}

type MatType string

//C.MatType

var (
	MatAIJ    MatType = "aij"
	MatSeqAIJ MatType = "seqaij"
	MatDense MatType  = "dense"
)

// Builds matrix object for a particular matrix type.
func (m *Mat) SetType(matype MatType) {
	mt := C.CString(string(matype))
	defer C.free(unsafe.Pointer(mt))
	errorCode := C.MatSetType(m.mat_p, mt)
	CheckError(errorCode)
}

type MatDuplicateOption C.MatDuplicateOption

var (
	MatDoNotCopyValues MatDuplicateOption = MatDuplicateOption(C.MAT_DO_NOT_COPY_VALUES)
	MatCopyValues                         = MatDuplicateOption(C.MAT_COPY_VALUES)
	MatShareNonZeroPattern                = MatDuplicateOption(C.MAT_SHARE_NONZERO_PATTERN)
)

// Duplicate duplicates a matrix including the non-zero structure.
func (m *Mat) Duplicate(op MatDuplicateOption) *Mat {
	dup := &Mat{}
	runtime.SetFinalizer(dup, destroyMat)
	errorCode := C.MatDuplicate(m.mat_p, C.MatDuplicateOption(op), &dup.mat_p)
	CheckError(errorCode)
	return dup
}

// Context of matrix information.
type MatInfo struct {
	info_p                              C.MatInfo
	BlockSize                           float64 // Block size.
	NZAllocated, NZUsed, NZUnneeded     float64 // Number of nonzeros.
	Memory                              float64 // Memory allocated.
	Assemblies                          float64 // Number of matrix assemblies called.
	MAllocs                             float64 // Number of mallocs during SetValues().
	FillRatioGiven, FillRatioNeeded     float64 // Fill ratio for LU/ILU.
	FactorMAllocs                       float64 // Number of mallocs during factorization.
}

func newMatInfo(info C.MatInfo) *MatInfo {
	return &MatInfo{
		BlockSize: float64(info.block_size),
		NZAllocated: float64(info.nz_allocated),
		NZUsed: float64(info.nz_used),
		NZUnneeded: float64(info.nz_unneeded),
		Memory: float64(info.memory),
		Assemblies: float64(info.assemblies),
		MAllocs: float64(info.mallocs),
		FillRatioGiven: float64(info.fill_ratio_given),
		FillRatioNeeded: float64(info.fill_ratio_needed),
		FactorMAllocs: float64(info.factor_mallocs),
	}
}

type MatInfoType C.MatInfoType

var (
	MatLocal MatInfoType = MatInfoType(C.MAT_LOCAL)
	MatGlobalMax         = MatInfoType(C.MAT_GLOBAL_MAX)
	MatGlobalSum         = MatInfoType(C.MAT_GLOBAL_SUM)
)

// Returns information about matrix storage (number of nonzeros, etc.).
func (m *Mat) Info(flag MatInfoType) *MatInfo {
	var info C.MatInfo
	errorCode := C.MatGetInfo(m.mat_p, C.MatInfoType(flag), &info)
	CheckError(errorCode)
	return newMatInfo(info)
}

// Gets the values from a given column of a matrix.
/*func (m *Mat) Column(col int, yy *Vec) {
	errorCode := C.MatGetColumnVector(m.mat_p, yy.vec_p, C.PetscInt(col))
	CheckError(errorCode)
}

func (m *Mat) Row(row int) {
	var ncols C.PetscInt//(nil)
	var cols []C.PetscInt
	var vals []C.PetscScalar
	errorCode := C.MatGetRow(m.mat_p, C.PetscInt(row), ncols, cols, vals)
	CheckError(errorCode)
}*/

// Data passed into the matrix factorization routines.
type MatFactorInfo struct {
	info_p        C.MatFactorInfo
	DiagonalFill  float64 // force diagonal to fill in if initially not filled
	Usedt         float64
	Dt            float64 // drop tolerance
	Dtcol         float64 // tolerance for pivoting
	Dtcount       float64 // maximum nonzeros to be allowed per row
	Fill          float64 // expected fill, nonzeros in factored matrix/nonzeros in original matrix
	Levels        float64 // ICC/ILU(levels)
	PivotInBlocks float64 // for BAIJ and SBAIJ matrices pivot in factorization on blocks, default 1.0
	// factorization may be faster if do not pivot
	ZeroPivot   float64 /// pivot is called zero if less than this
	ShiftType   float64 // type of shift added to matrix factor to prevent zero pivots
	ShiftAmount float64 // how large the shift is
}

func matFactorInfo(mfi *MatFactorInfo) C.MatFactorInfo {
	var info C.MatFactorInfo
	info.diagonal_fill = C.PetscReal(mfi.DiagonalFill)
	info.usedt = C.PetscReal(mfi.Usedt)
	info.dt = C.PetscReal(mfi.Dt)
	info.dtcol = C.PetscReal(mfi.Dtcol)
	info.dtcount = C.PetscReal(mfi.Dtcount)
	info.fill = C.PetscReal(mfi.Fill)
	info.levels = C.PetscReal(mfi.Levels)
	info.pivotinblocks = C.PetscReal(mfi.PivotInBlocks)
	info.zeropivot = C.PetscReal(mfi.ZeroPivot)
	info.shifttype = C.PetscReal(mfi.ShiftType)
	info.shiftamount = C.PetscReal(mfi.ShiftAmount)
	return info
}

//func newMatFactorInfo(info C.MatFactorInfo) *MatFactorInfo {
//	return &MatFactorInfo{
//		info_p: info,
//		DiagonalFill: float64(info.diagonal_fill),
//		Usedt: float64(info.usedt),
//		Dt: float64(info.dt),
//		Dtcol: float64(info.dtcol),
//		Dtcount: float64(info.dtcount),
//		Fill: float64(info.fill),
//		Levels: float64(info.levels),
//		PivotInBlocks: float64(info.pivotinblocks),
//		ZeroPivot: float64(info.zeropivot),
//		ShiftType: float64(info.shifttype),
//		ShiftAmount: float64(info.shiftamount),
//	}
//}

// Performs in-place Cholesky factorization of a symmetric matrix.
func (m *Mat) CholeskyFactor(perm *IS) {//, info *MatFactorInfo) {
	//var info C.MatFactorInfo
	errorCode := C.MatCholeskyFactor(m.mat_p, perm.is_p, nil)
	CheckError(errorCode)
	//return newMatFactorInfo(info)
}

// Solves A x = b, given a factored matrix.
func (m *Mat) Solve(b, x *Vec) {
	errorCode := C.MatSolve(m.mat_p, b.vec_p, x.vec_p)
	CheckError(errorCode)
}

type MatSolverPackage string//C.MatSolverPackage

// String with the name of a PETSc matrix solver type.
var (
	MatSolverSuperLU MatSolverPackage = "superlu"
	MatSolverSuperLUDist = "superlu_dist"
	MatSolverUMFPACK = "umfpack"
	MatSolverCHOLMOD     = "cholmod"
	MatSolverESSL         = "essl"
	MatSolverLUSOL        = "lusol"
	MatSolverMUMPS        = "mumps"
	MatSolverMKL_PARDISO  = "mkl_pardiso"
	MatSolverPETSC MatSolverPackage = "petsc"
	MatSolverKLU          = "klu"
)

type MatFactorType C.MatFactorType

var (
	MatFactorNone MatFactorType = MatFactorType(C.MAT_FACTOR_NONE)
	MatFactorLU = MatFactorType(C.MAT_FACTOR_LU)
	MatFactorCholesky = MatFactorType(C.MAT_FACTOR_CHOLESKY)
	MatFactorILU = MatFactorType(C.MAT_FACTOR_ILU)
	MatFactorICC = MatFactorType(C.MAT_FACTOR_ICC)
	MatFactorILUDT = MatFactorType(C.MAT_FACTOR_ILUDT)
)


// Returns a matrix suitable to calls to XXFactorSymbolic()
func (m *Mat) Factor(typ MatSolverPackage, ftyp MatFactorType) *Mat {
	var f Mat
	t := C.CString(string(typ))
	defer C.free(unsafe.Pointer(t))
	errorCode := C.MatGetFactor(m.mat_p, t, C.MatFactorType(ftyp), &f.mat_p)
	CheckError(errorCode)
	return &f
}

// Performs numeric Cholesky factorization of a symmetric matrix.
func (m *Mat) CholeskyFactorNumeric(mat *Mat, info *MatFactorInfo) {
	i := matFactorInfo(info)
	errorCode := C.MatCholeskyFactorNumeric(m.mat_p, mat.mat_p, &i)
	CheckError(errorCode)
}

// Performs symbolic Cholesky factorization of a symmetric matrix.
func (m *Mat) CholeskyFactorSymbolic(mat *Mat, perm *IS, info *MatFactorInfo) {
	i := matFactorInfo(info)
	errorCode := C.MatCholeskyFactorSymbolic(m.mat_p, mat.mat_p, perm.is_p, &i)
	CheckError(errorCode)
}

// Performs in-place LU factorization of matrix.
func (m *Mat) LUFactor(row, col *IS, info *MatFactorInfo) {
	i := matFactorInfo(info)
	errorCode := C.MatLUFactor(m.mat_p, row.is_p, col.is_p, &i)
	CheckError(errorCode)
}

// Performs numeric LU factorization of a matrix.
func (m *Mat) LUFactorNumeric(mat *Mat, info *MatFactorInfo) {
	i := matFactorInfo(info)
	errorCode := C.MatLUFactorNumeric(m.mat_p, mat.mat_p, &i)
	CheckError(errorCode)
}

// Performs symbolic LU factorization of matrix.
func (m *Mat) LUFactorSymbolic(mat *Mat, row, col *IS, info *MatFactorInfo) {
	i := matFactorInfo(info)
	errorCode := C.MatLUFactorSymbolic(m.mat_p, mat.mat_p, row.is_p, col.is_p, &i)
	CheckError(errorCode)
}

// Solves A X = B, given a factored matrix.
func (m *Mat) MatSolve(B, X *Mat) {
	errorCode := C.MatMatSolve(m.mat_p, B.mat_p, X.mat_p)
	CheckError(errorCode)
}

// Computes Y = Y + a I, where a is a scalar and I is the identity matrix.
func (m *Mat) Shift(a float64) {
	errorCode := C.MatShift(m.mat_p, C.PetscScalar(a))
	CheckError(errorCode)
}

// Scale all matrix elements by the given number.
func (m *Mat) Scale(alpha float64) {
	errorCode := C.MatScale(m.mat_p, C.PetscScalar(alpha))
	CheckError(errorCode)
}

// Scales a matrix on the left and right by diagonal matrices that are stored
// as vectors. Either of the two scaling matrices can be nil.
func (m *Mat) ScaleDiagonal(l, r *Vec) {
	errorCode := C.MatDiagonalScale(m.mat_p, l.vec_p, r.vec_p)
	CheckError(errorCode)
}

// OwnershipRange returns the range of matrix rows owned by this processor.
func (m *Mat) OwnershipRange() (int, int) {
	var low C.PetscInt
	var high C.PetscInt
	errorCode := C.MatGetOwnershipRange(m.mat_p, &low, &high)
	CheckError(errorCode)
	return int(low), int(high)
}

// Gets a block of values from a matrix.
func (m *Mat) GetValues(idxm, idxn []int) []float64 {
	im := intSlice(idxm)
	in := intSlice(idxn)
	mn := len(idxm) * len(idxn)
	vv := make([]C.PetscScalar, mn)
	errorCode := C.MatGetValues(m.mat_p, C.PetscInt(len(idxm)), &im[0], C.PetscInt(len(idxn)), &in[0], &vv[0])
	CheckError(errorCode)
	return scalarArray(&vv[0], C.PetscInt(mn))
}

// SetValue sets a single entry into the matrix.
func (m *Mat) SetValue(row, col int, value float64, mode C.InsertMode) {
	errorCode := C.MatSetValue(m.mat_p, C.PetscInt(row), C.PetscInt(col), C.PetscScalar(value), mode)
	CheckError(errorCode)
}

// SetValues inserts or adds a block of values into a matrix.
func (m *Mat) SetValues(idxm, idxn []int, v []float64, addv C.InsertMode) {
	im := intSlice(idxm)
	in := intSlice(idxn)
	vv := scalarSlice(v)
	errorCode := C.MatSetValues(m.mat_p, C.PetscInt(len(idxm)), &im[0], C.PetscInt(len(idxn)), &in[0], &vv[0], addv)
	CheckError(errorCode)
}

var (
	MatFlushAssembly C.MatAssemblyType = C.MAT_FLUSH_ASSEMBLY
	MatFinalAssembly C.MatAssemblyType = C.MAT_FINAL_ASSEMBLY
)

// AssemblyBegin begins assembling the matrix. This routine should be
// called after completing all calls to SetValues().
func (m *Mat) AssemblyBegin(typ C.MatAssemblyType) {
	errorCode := C.MatAssemblyBegin(m.mat_p, typ)
	CheckError(errorCode)
}

// AssemblyEnd completes assembling the matrix. This routine should be
// called after AssemblyBegin().
func (m *Mat) AssemblyEnd(typ C.MatAssemblyType) {
	errorCode := C.MatAssemblyEnd(m.mat_p, typ)
	CheckError(errorCode)
}

// Norm calculates various norms of a matrix.
func (m *Mat) Norm(typ C.NormType) float64 {
	var nrm C.PetscReal
	errorCode := C.MatNorm(m.mat_p, typ, &nrm)
	CheckError(errorCode)
	return float64(nrm)
}

// Gives access to the array where the data for a SeqDense matrix is stored.
func (m *Mat) DenseArray() [][]float64 {
	//	errorCode := C.MatDenseGetArray(m.mat_p, PetscScalar **array)
	//	CheckError(errorCode)
	return nil
}

// Returns access to the array where the data for a SeqDense matrix is stored
// obtained by DenseArray.
func (m *Mat) RestoreDenseArray(a [][]float64) {
	//	errorCode := C.MatDenseRestoreArray(m.mat_p, PetscScalar **array)
	//	CheckError(errorCode)
}

// View visualizes a matrix object.
func (m *Mat) View(viewer *Viewer) {
	errorCode := C.MatView(m.mat_p, C.PetscViewer(viewer.viewer_p))
	CheckError(errorCode)
}

// Diagonal gets the diagonal of a matrix.
func (m *Mat) Diagonal(v *Vec) {
	errorCode := C.MatGetDiagonal(m.mat_p, v.vec_p)
	CheckError(errorCode)
}

type MatReuse C.MatReuse

var (
	MatInitialMatrix MatReuse = MatReuse(C.MAT_INITIAL_MATRIX)
	MatReuseMatrix = MatReuse(C.MAT_REUSE_MATRIX)
	MatIgnoreMatrix = MatReuse(C.MAT_IGNORE_MATRIX)
)

// Transpose computes an in-place or out-of-place transpose of a matrix.
func (m *Mat) Transpose(reuse MatReuse) *Mat {
	t := &Mat{}
	runtime.SetFinalizer(t, destroyMat)
	errorCode := C.MatTranspose(m.mat_p, C.MatReuse(reuse), &t.mat_p)
	CheckError(errorCode)
	return t
}

// Zeros all entries of the matrix.
func (m *Mat) ZeroEntries() {
	errorCode := C.MatZeroEntries(m.mat_p)
	CheckError(errorCode)
}

//type MatStructure C.MatStructure

var (
	DifferentNonZeroPattern C.MatStructure = C.DIFFERENT_NONZERO_PATTERN
	SubSetNonZeroPattern C.MatStructure    = C.SUBSET_NONZERO_PATTERN
	SameNonZeroPattern C.MatStructure      = C.SAME_NONZERO_PATTERN

	//DifferentNonZeroPattern MatStructure = MatStructure(C.DIFFERENT_NONZERO_PATTERN)
	//SubSetNonZeroPattern MatStructure = MatStructure(C.SUBSET_NONZERO_PATTERN)
	//SameNonZeroPattern MatStructure = MatStructure(C.SAME_NONZERO_PATTERN)
	//SamePreconditioner MatStructure = MatStructure(C.SAME_PRECONDITIONER)
)

// AXPY computes Y = a*X + Y.
func (m *Mat) AXPY(a float64, X *Mat, str C.MatStructure) {
	errorCode := C.MatAXPY(m.mat_p, C.PetscScalar(a), X.mat_p, str)
	CheckError(errorCode)
}

// AYPX computes Y = a*Y + X.
func (m *Mat) AYPX(a float64, X *Mat, str C.MatStructure) {
	errorCode := C.MatAYPX(m.mat_p, C.PetscScalar(a), X.mat_p, str)
	CheckError(errorCode)
}

// Mult computes the matrix-vector product, y = Ax.
func (m *Mat) Mult(x, y *Vec) /**Vec*/ {
	//y := x.Duplicate()
	errorCode := C.MatMult(m.mat_p, x.vec_p, y.vec_p)
	CheckError(errorCode)
	//return y
}

// Random sets all components of a matrix to random numbers.
func (m *Mat) Random(rctx *Random) {
	errorCode := C.MatSetRandom(m.mat_p, rctx.rand_p)
	CheckError(errorCode)
}

// MatMult performs Matrix-Matrix Multiplication C=A*B.
func (m *Mat) MatMult(B *Mat, scall MatReuse, fill float64) *Mat {
	CC := &Mat{}
	runtime.SetFinalizer(CC, destroyMat)
	errorCode := C.MatMatMult(m.mat_p, B.mat_p, C.MatReuse(scall), C.PetscReal(fill), &CC.mat_p)
	CheckError(errorCode)
	return CC
}

// MultTranspose computes matrix transpose times a vector.
func (m *Mat) MultTranspose(x, y *Vec) /**Vec*/ {
	//y := &Vec{}
	runtime.SetFinalizer(y, destroyVec)
	errorCode := C.MatMultTranspose(m.mat_p, x.vec_p, y.vec_p)
	CheckError(errorCode)
	//return y
}

// MultAdd computes v3 = v2 + A * v1.
func (m *Mat) MultAdd(v1, v2, v3 *Vec) /**Vec*/ {
	//v3 := &Vec{}
	runtime.SetFinalizer(v3, destroyVec)
	errorCode := C.MatMultAdd(m.mat_p, v1.vec_p, v2.vec_p, v3.vec_p)
	CheckError(errorCode)
	//return v3
}

// Computes v3 = v2 + A' * v1.
func (m *Mat) MultTransposeAdd(v1, v2, v3 *Vec) {
	errorCode := C.MatMultTransposeAdd(m.mat_p, v1.vec_p, v2.vec_p, v3.vec_p)
	CheckError(errorCode)
}

// Vecs gets vector(s) compatible with the matrix, i.e. with the same
// parallel layout.
func (m *Mat) Vecs(right *Vec, left *Vec) {
	var r *C.Vec = nil
	if right != nil {
		r = &right.vec_p
	}
	var l *C.Vec = nil
	if left != nil {
		l = &left.vec_p
	}
	errorCode := C.MatGetVecs(m.mat_p, r, l)
	CheckError(errorCode)
}

// Indicates if a matrix has been assembled and is ready for use.
func (m *Mat) Assembled() bool {
	var assembled C.PetscBool
	errorCode := C.MatAssembled(m.mat_p, &assembled)
	CheckError(errorCode)
	return petscBool(assembled)
}

// Sets the vector U at which matrix vector products of the Jacobian are
// computed.
func (m *Mat) MFFDSetBase(U, F *Vec) {
	errorCode := C.MatMFFDSetBase(m.mat_p, U.vec_p, F.vec_p)
	CheckError(errorCode)
}

// SeqDenseSetPreallocation sets the array used for storing the matrix
// elements.
func (m *Mat) SeqDenseSetPreallocation(data []float64) {
	d := scalarSlice(data)
	errorCode := C.MatSeqDenseSetPreallocation(m.mat_p, &d[0])
	CheckError(errorCode)
}

// SubMatrices extracts several submatrices from a matrix.
func (m *Mat) SubMatrices(n int, irow []IS, icol []IS, scall MatReuse) []*Mat {
	var iRow []C.IS
	var iCol []C.IS
	var subMat []*C.Mat
	errorCode := C.MatGetSubMatrices(m.mat_p, C.PetscInt(n), &iRow[0], &iCol[0], C.MatReuse(scall), &subMat[0])
	CheckError(errorCode)

	submat := make([]*Mat, n)
	for i, sm := range subMat {
		submat[i] = &Mat{*sm}
	}
	return submat
}

// RedundantMatrix creates redundant matrices and put them into
// processors of subcommunicators.
func (m *Mat) RedundantMatrix(nsubcomm int, subcomm MPI_Comm, reuse MatReuse) *Mat {
	var matredundant Mat
	errorCode := C.MatGetRedundantMatrix(m.mat_p, C.PetscInt(nsubcomm),
		C.MPI_Comm(subcomm), C.MatReuse(reuse), &matredundant.mat_p)
	CheckError(errorCode)
	return &matredundant
}
