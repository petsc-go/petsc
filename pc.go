package petsc

/*
#cgo pkg-config: PETSc
#cgo LDFLAGS: -lpetsc -lmpi

#include "petscpc.h"
*/
import (
	"C"
)

// String with the name of a PETSc preconditioner method.
type PCType string

const (
	PCNone         PCType = "none"
	PCJacobi              = "jacobi"
	PCSOR                 = "sor"
	PCLU                  = "lu"
	PCShell               = "shell"
	PCBJacobi             = "bjacobi"
	PCMG                  = "mg"
	PCEisenstat           = "eisenstat"
	PCILU                 = "ilu"
	PCICC                 = "icc"
	PCASM                 = "asm"
	PCGASM                = "gasm"
	PCKSP                 = "ksp"
	PCComposite           = "composite"
	PCRedundant           = "redundant"
	PCSPAI                = "spai"
	PCNN                  = "nn"
	PCCholesky            = "cholesky"
	PCPBJacobi            = "pbjacobi"
	PCMat                 = "mat"
	PCHYPRE               = "hypre"
	PCParams              = "parms"
	PCFieldSplit          = "fieldsplit"
	PCTFS                 = "tfs"
	PCML                  = "ml"
	PCGalerkin            = "galerkin"
	PCExotic              = "exotic"
	PCHMPI                = "hmpi"
	PCSupportGraph        = "supportgraph"
	PCASA                 = "asa"
	PCCP                  = "cp"
	PCBFBT                = "bfbt"
	PCLSC                 = "lsc"
	PCPython              = "python"
	PCPFMG                = "pfmg"
	PCSYSPFMG             = "syspfmg"
	PCRedistribute        = "redistribute"
	PCSVD                 = "svd"
	PCGAMG                = "gamg"
	PCSACUSP              = "sacusp" // These four run on NVIDIA GPUs using CUSP.
	PCSACUSPPOLY          = "sacusppoly"
	PCBICGSTABCUSP        = "bicgstabcusp"
	PCAINVCUSP            = "ainvcusp"
	PCBDDC                = "bddc"
)

/* Intermediate */

// SetType builds PC for a particular preconditioner type.
//func (pc *PC) SetType(pctype PCType) {
//	ctype := C.CString(pctype)
//	defer C.free(unsafe.Pointer(ctype))
//	errorCode := C.PCSetType(pc.pc_p, ctype)
//	CheckError(errorCode)
//}

/* Developer */

//type PC struct {
//	pc_p C.PC
//}
