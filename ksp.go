package petsc

/*
#cgo pkg-config: PETSc
#cgo LDFLAGS: -lpetsc -lmpi

#include "mpi.h"
#include "petscksp.h"
*/
import "C"

import "runtime"

// PETSc type that manages all Krylov methods. This is the type that manages
// the linear solves in PETSc (even those such as direct solvers that do no
// use Krylov accelerators).
type KSP struct {
	ksp_p C.KSP
}

// NewKSP creates the default KSP context.
func NewKSP(comm MPI_Comm) *KSP {
	s := &KSP{}
	runtime.SetFinalizer(s, destroyKSP)
	errorCode := C.KSPCreate(C.MPI_Comm(comm), &s.ksp_p)
	CheckError(errorCode)
	return s
}

func destroyKSP(s *KSP) C.PetscErrorCode {
	return C.KSPDestroy(&s.ksp_p)
}

// Destroy destroys a KSP context.
func (s *KSP) Destroy() {
	errorCode := destroyKSP(s)
	CheckError(errorCode)
}

// SetOperators sets the matrix associated with the linear system
// and a (possibly) different one associated with the preconditioner.
func (s *KSP) SetOperators(Amat, Pmat *Mat) {
	errorCode := C.KSPSetOperators(s.ksp_p, Amat.mat_p, Pmat.mat_p)
	CheckError(errorCode)
}

// SetFromOptions sets KSP options from the options database.
// This routine must be called before KSPSetUp() if the user
// is to be allowed to set the Krylov type.
func (s *KSP) SetFromOptions() {
	errorCode := C.KSPSetFromOptions(s.ksp_p)
	CheckError(errorCode)
}

// Solve solves the linear system.
func (s *KSP) Solve(b, x *Vec) {
	errorCode := C.KSPSolve(s.ksp_p, b.vec_p, x.vec_p)
	CheckError(errorCode)
}

/* Intermediate */

// SetTolerances sets the relative, absolute, divergence, and maximum
// iteration tolerances used by the default KSP convergence testers.
func (k *KSP) SetTolerances(rtol, abstol, dtol float64, maxits int) {
	errorCode := C.KSPSetTolerances(k.ksp_p, C.PetscReal(rtol), C.PetscReal(abstol), C.PetscReal(dtol), C.PetscInt(maxits))
	CheckError(errorCode)
}

/* Developer */

//func (k *KSP) PC() (*PC, ErrorCode) {
//	var pc *PC
//	errorCode := C.KSPGetPC(k, pc.pc_p)
//	CheckError(errorCode)
//}
