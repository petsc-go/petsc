FROM rlincoln/petsc

MAINTAINER Richard Lincoln, r.w.lincoln@gmail.com

RUN apt-get update

# Install and set up Go.
RUN apt-get install -y golang
ENV GOPATH /go
ENV PATH $PATH:$GOPATH/bin

# Add the PETSc wrappers to the Go workspace.
ADD . /go/src/github.com/rwl/petsc

#RUN go build github.com/rwl/petsc
#RUN go install github.com/rwl/petsc/examples/snes42
#RUN mpirun -np 1 snes42
