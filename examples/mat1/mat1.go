package main

import (
	"os"
	"github.com/rwl/petsc"
	"github.com/rwl/mpi"
)

const help = `Tests LU, Cholesky factorization and MatMatSolve() for a sequential dense matrix.
For MATSEQDENSE matrix, the factorization is just a thin wrapper to LAPACK \n`;

func main() {
//	Mat            mat,F,RHS,SOLU;
//	MatInfo        info;
//	PetscErrorCode ierr;
//	PetscInt       m = 10,n = 10,i,j,rstart,rend,nrhs=2;
//	PetscScalar    value = 1.0;
//	Vec            x,y,b,ytmp;
//	PetscReal      norm,tol=1.e-15;
//	PetscMPIInt    size;
//	PetscScalar    *rhs_array,*solu_array;
//	PetscRandom    rand;
//	PetscScalar    *array,rval;
	var m, n, nrhs int = 10, 10, 2
	value := 1.0
	tol := 1.e-15

	petsc.Initialize(os.Args, "", help)
	size := mpi.Comm_size(mpi.COMM_WORLD)
	if size != 1 {
		petsc.SetErrQ(petsc.CommWorld, petsc.ErrorSupport, "This is a uniprocessor example only!")
	}

	// Create single vectors.
	y := petsc.NewVec(petsc.CommWorld)
	y.SetSizes(petsc.Decide, m)
	y.SetFromOptions()
	x := y.Duplicate()
	ytmp := y.Duplicate()
	x.Set(value)
	b := petsc.NewVec(petsc.CommWorld)
	b.SetSizes(petsc.Decide, n)
	b.SetFromOptions()

	// Create multiple vectors RHS and SOLU.
	RHS := petsc.NewMat(petsc.CommWorld)
	RHS.SetSizes(petsc.Decide, petsc.Decide, n, nrhs)
	RHS.SetType(petsc.MatDense)
	RHS.SetFromOptions()
	RHS.SeqDenseSetPreallocation(nil)

	rand := petsc.NewRandom(petsc.CommWorld)
	rand.SetFromOptions()
	array := RHS.DenseArray()
	for j := 0; j < nrhs; j++ {
		for i := 0; i < n; i++ {
			rval := rand.Value()
			array[n*j+i] = []float64{rval}
		}
	}
	RHS.RestoreDenseArray(array)

	SOLU := RHS.Duplicate(petsc.MatDoNotCopyValues)

	// Create matrix.
	mat := petsc.NewMatSeqDense(petsc.CommWorld, m, n, nil)
	rstart, rend := mat.OwnershipRange()
	for i := rstart; i < rend; i++ {
		value = float64(i+1);
		mat.SetValue(i, i, value, petsc.InsertValues)
	}
	mat.AssemblyBegin(petsc.MatFinalAssembly)
	mat.AssemblyEnd(petsc.MatFinalAssembly)

	info := mat.Info(petsc.MatLocal)
	petsc.Printf(petsc.CommWorld, "matrix nonzeros = %d, allocated nonzeros = %d\n",
		info.NZUsed, info.NZAllocated)

	// Cholesky factorization - perm and factinfo are ignored by LAPACK
	// in-place Cholesky
	mat.Mult(x, b)
	F := mat.Duplicate(petsc.MatCopyValues)
	F.CholeskyFactor(nil)//, 0)
	F.Solve(b, y)
	F.Destroy()
	value = -1.0
	y.AXPY(value, x)
	norm := y.Norm(petsc.Norm2)
	if norm > tol {
		petsc.Printf(petsc.CommWorld, "Warning: Norm of error for Cholesky %g\n", norm)
	}

	// Out-place Cholesky.
	F = mat.Factor(petsc.MatSolverPETSC, petsc.MatFactorCholesky)
	F.CholeskyFactorSymbolic(mat, nil, nil)
	F.CholeskyFactorNumeric(mat, nil)
	F.Solve(b, y)
	value = -1.0
	y.AXPY(value, x)
	norm = y.Norm(petsc.Norm2)
	if norm > tol {
		petsc.Printf(petsc.CommWorld, "Warning: Norm of error for Cholesky %g\n", norm)
	}
	F.Destroy()

	// LU factorization - perms and factinfo are ignored by LAPACK.
	i, value := m-1, 1.0
	mat.SetValue(i, i, value, petsc.InsertValues)
	mat.AssemblyBegin(petsc.MatFinalAssembly)
	mat.AssemblyEnd(petsc.MatFinalAssembly)
	mat.Mult(x, b)
	F = mat.Duplicate(petsc.MatCopyValues)

	// In-place LU.
	F.LUFactor(nil, nil, nil)
	F.Solve(b, y)
	value = -1.0
	y.AXPY(value, x)
	norm = y.Norm(petsc.Norm2)
	if norm > tol {
		petsc.Printf(petsc.CommWorld, "Warning: Norm of error for LU %g\n", norm)
	}
	F.MatSolve(RHS, SOLU)
	solu_array := SOLU.DenseArray()
	rhs_array := RHS.DenseArray()
	for j := 0; j < nrhs; j++ {
		y.PlaceArray(solu_array+j*m)
		b.PlaceArray(rhs_array+j*m)

		mat.Mult(y, ytmp)
		ytmp.AXPY(-1.0, b) // ytmp = mat*SOLU[:,j] - RHS[:,j]
		norm := ytmp.Norm(petsc.Norm2)
		if norm > tol {
			petsc.Printf(petsc.CommWorld, "Error: Norm of residual for LU %g\n", norm)
		}

		b.ResetArray()
		y.ResetArray()
	}
	RHS.RestoreDenseArray(rhs_array)
	SOLU.RestoreDenseArray(solu_array)

	F.Destroy()

	// Out-place LU.
	F = mat.Factor(petsc.MatSolverPETSC, petsc.MatFactorLU)
	F.LUFactorSymbolic(mat, nil, nil, nil)
	F.LUFactorNumeric(mat, nil)
	F.Solve(b, y)
	value = -1.0
	y.AXPY(value, x)
	norm = y.Norm(petsc.Norm2)
	if norm > tol {
		petsc.Printf(petsc.CommWorld, "Warning: Norm of error for LU %g\n", norm)
	}

	// Free space.
	F.Destroy()
	mat.Destroy()
	RHS.Destroy()
	SOLU.Destroy()
	rand.Destroy()
	x.Destroy()
	b.Destroy()
	y.Destroy()
	ytmp.Destroy()
	petsc.Finalize();
}
