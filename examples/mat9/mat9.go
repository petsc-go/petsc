package main

import (
	"os"
	"github.com/rwl/petsc"
	"github.com/rwl/mpi"
)

const help = "Tests MPI parallel matrix creation. Test MatGetRedundantMatrix() \n\n"

func main() {
	one := 1.0

	petsc.Initialize(os.Args, "", help)
	m, ok := petsc.OptionsGetInt("", "-m")
	if !ok {
		m = 3
	}
	rank := mpi.Comm_rank(mpi.COMM_WORLD)
	size := mpi.Comm_size(mpi.COMM_WORLD)
	n    := 2*size

	C := petsc.NewMat(petsc.CommWorld)
	C.SetSizes(petsc.Decide, petsc.Decide, m*n, m*n)
	C.SetFromOptions()
	C.SetUp()

	// Create the matrix for the five point stencil, YET AGAIN
	for i:=0; i<m; i++ {
		for j:=2*rank; j<2*rank+2; j++ {
			v := -1.0;  Ii := j + n*i
			if i>0 {
				J := Ii - n
				C.SetValue(Ii, J, v, petsc.InsertValues)
			}
			if i<m-1 {
				J := Ii + n
				C.SetValue(Ii, J, v, petsc.InsertValues)
			}
			if j>0   {
				J := Ii - 1
				C.SetValue(Ii, J, v, petsc.InsertValues)
			}
			if j<n-1 {
				J := Ii + 1
				C.SetValue(Ii, J, v, petsc.InsertValues)
			}
			v = 4.0
			C.SetValue(Ii, Ii, v, petsc.InsertValues)
		}
	}

	// Add extra elements (to illustrate variants of MatGetInfo)
	Ii, J, v := n, n-2, 100.0
	C.SetValue(Ii, J, v, petsc.InsertValues)
	Ii, J, v = n-2, n, 100.0
	C.SetValue(Ii, J, v, petsc.InsertValues)

	C.AssemblyBegin(petsc.MatFinalAssembly)
	C.AssemblyEnd(petsc.MatFinalAssembly)

	// Form vectors.
	var x, y petsc.Vec
	C.Vecs(&x, &y)
	ldim := x.LocalSize()
	low, _/*high*/ := x.OwnershipRange()
	for i := 0; i<ldim; i++ {
		iglobal := i + low
		v       = one*float64(i) + 100.0*float64(rank)
		x.SetValue(iglobal, v, petsc.InsertValues)
	}
	x.AssemblyBegin()
	x.AssemblyEnd()

	C.Mult(&x, &y)

	//x.View(PETSC_VIEWER_STDOUT_WORLD)
	//y.View(PETSC_VIEWER_STDOUT_WORLD)
	if petsc.OptionsHasName("", "-view_info") {
		petsc.ViewerStdOutWorld.SetFormat(petsc.ViewerASCIIInfo)
		C.View(petsc.ViewerStdOutWorld)

		info := C.Info(petsc.MatGlobalSum)
		petsc.Printf(petsc.CommWorld, `matrix information (global sums):
		nonzeros = %D, allocated nonzeros = %D\n`, info.NZUsed, info.NZAllocated)
		info = C.Info(petsc.MatGlobalMax)
		petsc.Printf(petsc.CommWorld, `matrix information (global max):
		nonzeros = %D, allocated nonzeros = %D\n`, info.NZUsed, info.NZAllocated)
	}

	if petsc.OptionsHasName("", "-view_mat") {
		C.View(petsc.ViewerStdOutWorld)
	}

	// Test MatGetRedundantMatrix()
	/*if (size > 1) {
		var nsubcomms, subsize int//C.PetscMPIInt

		Nsubcomms, ok := petsc.OptionsGetInt("", "-nsubcomms")
		nsubcomms = Nsubcomms//petscMPIIntCast(Nsubcomms)
		if (nsubcomms > size) {
			petsc.SetErrQ2(petsc.CommSelf, petsc.ErrorArgWrong, "nsubcomms %d cannot > ncomms %d", nsubcomms, size)
		}

		Credundant := C.RedundantMatrix(nsubcomms, mpi.CommNull, petsc.MatInitialMatrix)
		Credundant = C.RedundantMatrix(nsubcomms, mpi.CommNull, petsc.MatReuseMatrix)

		subcomm := Credundant.Comm()
		subsize = mpi.Comm_size(subcomm)

		if subsize == 1 && flg_mat {
			fmt.Printf("\n[%d] Credundant:\n", rank)
			Credundant.View(petsc.ViewerStdOutSelf)
		}
		Credundant.Destroy()
	}*/

	x.Destroy()
	y.Destroy()
	C.Destroy()
	petsc.Finalize()
}
