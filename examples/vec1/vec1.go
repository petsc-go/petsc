package main

import (
	"fmt"
	"github.com/rwl/petsc"
	"os"
)

// $ mpirun -np 2 -mca btl ^openib ex1

func main() {
	defer func() {
		if r := recover(); r != nil {
			if err, ok := r.(*petsc.ErrorCode); ok {
				fmt.Printf("error: %s", err/*.Error()*/)
			}
			panic(r)
		}
	}()

	const n = 5
	const help = "Tests repeated VecSetType().\n\n"

	petsc.Initialize(os.Args, "", help)

	// Create vector.
	x := petsc.NewVec(petsc.CommSelf)
	x.SetSizes(n, petsc.Decide)
	x.SetType(petsc.VecMpi)
	x.SetType(petsc.VecSeq)
	y := x.Duplicate()
	x.SetType("mpi")

	x.Set(1.0)
	y.Set(2.0)

	//x.View(petsc.ViewerStdOutWorld)
	//y.View(petsc.ViewerStdOutWorld)

	x.Destroy()
	y.Destroy()

	petsc.Finalize()
	// Output:
}
