package main

import (
	"github.com/rwl/petsc"
	"os"
)

const help = `Tests various 1-dimensional DMDA routines.\n\n`

func main() {
	petsc.Initialize(os.Args, "", help)

	// Create viewers.
	viewer := petsc.ViewerDrawOpen(petsc.CommWorld, 0, "", 280, 480, 600, 200)
	draw := viewer.Draw(0)
	draw.SetDoubleBuffer()

	// Read options.
	M, _ := petsc.OptionsGetInt("", "-M")
	bx, _ := petsc.OptionsGetEnum("", "-wrap", petsc.DMDABoundaryTypes)
	dof, _ := petsc.OptionsGetInt("", "-dof")
	s, _ := petsc.OptionsGetInt("", "-s")

	// Create distributed array and get vectors.
	da := petsc.NewDMDA1D(petsc.CommWorld, bx, M, dof, s, nil)
	da.View(viewer)
	global := da.NewGlobalVector()
	local := da.NewLocalVector()

	// Set global vector; send ghost points to local vectors.
	value := 1.0
	global.Set(value)
	da.GlobalToLocalBegin(global, petsc.InsertValues, local)
	da.GlobalToLocalEnd(global, petsc.InsertValues, local)

	// Scale local vectors according to processor rank; pass to global vector.
	rank := petsc.Rank(petsc.CommWorld)
	value = rank+1
	local.Scale(value)
	da.LocalToGlobalBegin(local, petsc.InsertValues, global)
	da.LocalToGlobalEnd(local, petsc.InsertValues, global)

	global.View(viewer)
	petsc.Printf(petsc.CommWorld, "\nGlobal Vector:\n")
	global.View(petsc.ViewerStdOutWorld)
	petsc.Printf(petsc.CommWorld, "\n")

	// Send ghost points to local vectors.
	da.GlobalToLocalBegin(global, petsc.InsertValues, local)
	da.GlobalToLocalEnd(global, petsc.InsertValues, local)

	flg, _ := petsc.OptionsGetBool("", "-local_print")
	if flg {
		petsc.SynchronizedPrintf(petsc.CommWorld, "\nLocal Vector: processor %d\n", rank)
		sviewer := petsc.ViewerGetSingleton(petsc.ViewerStdOutWorld)
		local.View(sviewer)
		sviewer.RestoreSingleton(petsc.ViewerStdOutWorld)
		petsc.SynchronizedFlush(petsc.CommWorld)

		petsc.SynchronizedPrintf(petsc.CommWorld, "\nLocal to global mapping: processor %d\n", rank)
		sviewer := petsc.ViewerGetSingleton(petsc.ViewerStdOutWorld)
		is := da.GetLocalToGlobalMapping()
		is.View(sviewer)
		sviewer.RestoreSingleton(petsc.ViewerStdOutWorld)
		petsc.SynchronizedFlush(petsc.CommWorld)
	}

	// Free memory.
	viewer.Destroy()
	global.Destroy()
	local.Destroy()
	da.Destroy()
	petsc.Finalize()
}
