package main

import (
	"os"
	"github.com/rwl/petsc"
)

func main() {
	const help = "Test VecGetSubVector()\n\n"

	petsc.Initialize(os.Args, "", help)

	n := 10
	comm := petsc.CommWorld
	viewer := petsc.ViewerStdOutWorld
	size := petsc.Size(comm)
	rank := petsc.Rank(comm)

	X := petsc.NewVec(comm)
	X.SetSizes(n, petsc.Decide)
	X.SetFromOptions()
	rstart, rend := X.OwnershipRange()

	x := X.Array()
	for i := 0; i < rend-rstart; i++ {
		x[i] = float64(rstart+i)
	}
	X.RestoreArray(x)

	m := (rend-rstart)/3
	if rank > size/2 {
		m += 3
	}
	is0 := petsc.NewISStride(comm, m, rstart, 1)
	is1 := is0.Complement(rstart, rend)

	is0.View(viewer)
	is1.View(viewer)

	Y := X.SubVector(is0)
	Z := X.SubVector(is1)
	Y.View(viewer)
	Z.View(viewer)
	X.RestoreSubVector(is0, Y)
	X.RestoreSubVector(is1, Z)

	is0.Destroy()
	is1.Destroy()

	X.Destroy()

	petsc.Finalize()
}
