package main

import (
	"github.com/rwl/petsc"
	"os"
)

const help = `Newton's method to solve a two-variable system that comes from the Rosenbrock function.\n\n`


func main() {
	petsc.Initialize(os.Args, "", help)

	// Create nonlinear solver context.
	snes := petsc.NewSNES(petsc.CommWorld)

	// Create matrix and vector data structures; set corresponding routines.
	
	// Create vectors for solution and nonlinear function
	x := petsc.NewVec(petsc.CommWorld)
	x.SetSizes(petsc.Decide, 2)
	x.SetFromOptions()
	r := x.Duplicate()

	// Create Jacobian matrix data structure.
	J := petsc.NewMat(petsc.CommWorld)
	J.SetSizes(petsc.Decide, petsc.Decide, 2, 2)
	J.SetFromOptions()
	J.SetUp()

	// Set function evaluation routine and vector.
	snes.SetFunction(r, FormFunction1, nil)

	// Set Jacobian matrix data structure and Jacobian evaluation routine.
	snes.SetJacobian(J, J, FormJacobian1, nil)

	// Customize nonlinear solver; set runtime options.
	snes.SetFromOptions()

	// Evaluate initial guess; then solve nonlinear system.
	xx := x.Array()
	xx[0] = -1.2
	xx[1] = 1.0
	x.RestoreArray(xx)

	// Note: The user should initialize the vector, x, with the initial guess
	// for the nonlinear solver prior to calling SNESSolve().  In particular,
	// to employ an initial guess of zero, the user should explicitly set
	// this vector to zero by calling VecSet().

	snes.Solve(nil, x)
	x.View(petsc.ViewerStdOutWorld)
	its := snes.IterationNumber()
	reason := snes.ConvergedReason()
	// Some systems computes a residual that is identically zero, thus converging
	// due to FNORM_ABS, others converge due to FNORM_RELATIVE.  Here, we only
	// report the reason if the iteration did not converge so that the tests are
	// reproducible.
	var s string
	if reason > 0 {
		s = "CONVERGED"
	} else {
		s = petsc.ConvergedReason(reason)
	}
	petsc.Printf(petsc.CommWorld, "%s number of SNES iterations = %d\n", s, its)

	// Free work space. All PETSc objects should be destroyed when they
	// are no longer needed.
	x.Destroy()
	r.Destroy()
	J.Destroy()
	snes.Destroy()

	petsc.Finalize()
}

// FormFunction1 - Evaluates nonlinear function, F(x).
//
// Input Parameters:
//	snes - the SNES context
//	x    - input vector
//	ctx  - optional user-defined context
//
// Output Parameter:
//	f - function vector
func FormFunction1(snes *petsc.SNES, x, f *petsc.Vec, ctx interface{}) petsc.ErrorCode {
	// Get pointers to vector data.
	//	- For default PETSc vectors, VecGetArray() returns a pointer to
	//	the data array.  Otherwise, the routine is implementation dependent.
	//	- You MUST call VecRestoreArray() when you no longer need access to
	//	the array.
	xx := x.Array()
	ff := f.Array()

	// Compute function.
	ff[0] = -2.0 + 2.0*xx[0] + 400.0*xx[0]*xx[0]*xx[0] - 400.0*xx[0]*xx[1]
	ff[1] = -200.0*xx[0]*xx[0] + 200.0*xx[1]

	// Restore vectors.
	x.RestoreArray(xx)
	f.RestoreArray(ff)
	return petsc.ErrOK
}

// FormJacobian1 - Evaluates Jacobian matrix.
//
// Input Parameters:
//	snes - the SNES context
//	x - input vector
//	dummy - optional user-defined context (not used here)
//
// Output Parameters:
//	jac - Jacobian matrix
//	B - optionally different preconditioning matrix
//	flag - flag indicating matrix structure
func FormJacobian1(snes *petsc.SNES, x petsc.Vec, jac, B petsc.Mat, _ interface{}) petsc.ErrorCode {
	A := make([]float64, 4)
	idx := []int {0, 1}

	// Get pointer to vector data.
	xx := x.Array()

	// Compute Jacobian entries and insert into matrix.
	//	- Since this is such a small problem, we set all entries for
	//	the matrix at once.
	A[0]  = 2.0 + 1200.0*xx[0]*xx[0] - 400.0*xx[1]
	A[1]  = -400.0*xx[0]
	A[2]  = -400.0*xx[0]
	A[3]  = 200
	B.SetValues(idx, idx, A, petsc.InsertValues)
	//*flag = petsc.SameNonZeroPattern

	// Restore vector.
	x.RestoreArray(xx)

	// Assemble matrix.
	B.AssemblyBegin(petsc.MatFinalAssembly)
	B.AssemblyEnd(petsc.MatFinalAssembly)
	if jac != B {
		jac.AssemblyBegin(petsc.MatFinalAssembly)
		jac.AssemblyEnd(petsc.MatFinalAssembly)
	}
	return petsc.ErrOK
}
