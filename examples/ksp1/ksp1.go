package main

import (
	"github.com/rwl/petsc"
	"os"
)

func main() {
	const help = "Tests solving linear system on 0 by 0 matrix.\n\n"
	const N = 0

	petsc.Initialize(os.Args, "", help)

	// Create stiffness matrix.
	C := petsc.NewMat(petsc.CommWorld)
	C.SetSizes(petsc.Decide, petsc.Decide, N, N)
	C.SetFromOptions()
	C.SetUp()
	C.AssemblyBegin(petsc.MatFinalAssembly)
	C.AssemblyEnd(petsc.MatFinalAssembly)

	// Create right hand side and solution.
	u := petsc.NewVec(petsc.CommWorld)
	u.SetSizes(petsc.Decide, N)
	u.SetFromOptions()
	b := u.Duplicate()
	u.Set(0.0)
	b.Set(0.0)

	b.AssemblyBegin()
	b.AssemblyEnd()

	// Solve linear system.
	solver := petsc.NewKSP(petsc.CommWorld)
	solver.SetOperators(C, C, petsc.DifferentNonZeroPattern)
	solver.SetFromOptions()
	solver.Solve(b, u)

	x := C.Mult(u)
	x.AXPY(-1.0, b)
	norm := x.Norm(petsc.Norm2)
	petsc.Printf(petsc.CommWorld, "norm: %G", norm)

	solver.Destroy()
	u.Destroy()
	x.Destroy()
	b.Destroy()
	C.Destroy()

	petsc.Finalize()
}
