package main

import (
	"github.com/rwl/petsc"
	"os"
)

// Program usage: mpirun -np 1 rosenbrock1 [-help] [all TAO options]

const help = `This example demonstrates use of the TAO package to
solve an unconstrained minimization problem on a single processor.  We
minimize the extended Rosenbrock function:
    sum_{i=0}^{n/2-1} ( alpha*(x_{2i+1}-x_{2i}^2)^2 + (1-x_{2i})^2 ) \n`

// User-defined application context. Contains data needed by the
// application-provided call-back routines that evaluate the function,
// gradient, and hessian.
type AppCtx struct {
	n int // Dimension.
	alpha float64 // Condition parameter.
}

func main() {
	var ok bool
	user := &AppCtx{}

	// Initialize TAO and PETSc.
	petsc.Initialize(os.Args, "", help)
	size := petsc.Size(petsc.CommWorld)
	rank := petsc.Rank(petsc.CommWorld)

	if size > 1 {
		if rank == 0 {
			petsc.Printf(petsc.CommSelf, "This example is intended for single processor use!\n")
			petsc.SetErrQ(petsc.CommSelf, petsc.ErrorUser, "Incorrect number of processors")
		}
	}


	// Check for command line arguments to override defaults.
	user.n, ok = petsc.OptionsGetInt("", "-n")
	if !ok {
		user.n = 2
	}
	user.alpha, ok = petsc.OptionsGetReal("", "-alpha")
	if !ok {
		user.alpha = 99.0
	}

	// Allocate vectors for the solution and gradient.
	x := petsc.NewVecSeq(petsc.CommSelf, user.n)
	H := petsc.NewMatSeqBAIJ(petsc.CommSelf, 2, user.n, user.n, 1, nil)

	// TAO code begins here.

	// Create TAO solver with desired solution method.
	tao := petsc.NewTAO(petsc.CommSelf)
	tao.SetType(petsc.TAO_LMVM)

	// Set solution vec and an initial guess.
	x.Set(0.0)
	tao.SetInitialVector(x)

	// Set routines for function, gradient, hessian evaluation.
	tao.SetObjectiveAndGradientRoutine(FormFunctionGradient, &user)
	tao.SetHessianRoutine(H, H, FormHessian, &user)

	// Check for TAO command line options.
	tao.SetFromOptions()

	// Solve the application.
	tao.Solve()

	// Get termination information.
	reason := tao.ConvergedReason()
	if reason <= 0 {
		petsc.Printf(petsc.CommWorld, "Try a different TAO type, adjust some parameters, or check the function evaluation routines\n")
	}

	// Free TAO data structures.
	tao.Destroy()

	// Free PETSc data structures.
	x.Destroy()
	H.Destroy()

	petsc.Finalize()
}

// FormFunctionGradient evaluates the function, f(X), and gradient, G(X).
//
// Input Parameters:
//   tao  - the TaoSolver context
//   X    - input vector
//   ptr  - optional user-defined context, as set by SetFunctionGradient()
//
// Output Parameters:
//   G - vector containing the newly evaluated gradient
//   f - function value
//
// Note:
// Some optimization methods ask for the function and the gradient evaluation
// at the same time. Evaluating both at once may be more efficient that
// evaluating each separately.
func FormFunctionGradient(tao petsc.TAO, X petsc.Vec, f *float64, G petsc.Vec, ptr interface{}) petsc.ErrorCode {
	user := ptr.(*AppCtx)
	nn := user.n/2
	ff := 0.0
	alpha := user.alpha
	
	// Get pointers to vector data.
	x := X.Array()
	g := G.Array()
	
	// Compute G(X).
	for i := 0; i < nn; i++ {
		t1 := x[2*i+1]-x[2*i]*x[2*i]
		t2 := 1-x[2*i]
		ff += alpha*t1*t1 + t2*t2
		g[2*i] = -4*alpha*t1*x[2*i]-2.0*t2
		g[2*i+1] = 2*alpha*t1
	}
	
	// Restore vectors.
	X.RestoreArray(x)
	G.RestoreArray(g)
	*f = ff
	
	petsc.LogFlops(float64(nn*15))
	return 0
}

// FormHessian evaluates the Hessian matrix.
//
// Input Parameters:
//   tao   - the TaoSolver context
//   x     - input vector
//   ptr   - optional user-defined context, as set by SetHessian()
//
// Output Parameters:
//   H     - Hessian matrix
//
// Note: Providing the Hessian may not be necessary. Only some solvers
// require this matrix.
func FormHessian(tao petsc.TAO, X petsc.Vec, HH, Hpre *petsc.Mat, ptr interface{}) petsc.ErrorCode {
	user := ptr.(*AppCtx)
	nn := user.n/2
	alpha := user.alpha
	v := make([][]float64, 2)
	v[0] = make([]float64, 2)
	v[1] = make([]float64, 2)
	var ind []int
	H := HH

	// Zero existing matrix entries.
	assembled := H.Assembled()
	if assembled {
		H.ZeroEntries()
	}

	// Get a pointer to vector data.
	x := X.Array()

	// Compute H(X) entries.
	for i := 0; i < user.n/2; i++ {
		v[1][1] = 2*alpha
		v[0][0] = -4*alpha*(x[2*i+1]-3*x[2*i]*x[2*i]) + 2
		v[1][0] = -4.0*alpha*x[2*i]
		v[0][1] = -4.0*alpha*x[2*i]
		ind[0] = 2*i
		ind[1] = 2*i+1
		H.SetValues(ind, ind, v[0], petsc.InsertValues)
	}
	X.RestoreArray(x)

	// Assemble matrix.
	H.AssemblyBegin(petsc.MatFinalAssembly)
	H.AssemblyEnd(petsc.MatFinalAssembly)

	petsc.LogFlops(float64(nn*9))
	return petsc.ErrOK
}
