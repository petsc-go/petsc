package main

import (
	"os"
	"github.com/rwl/petsc"
)

func main() {
	const help = `Tests VecMax() with index.\n
  -n <length> : vector length\n\n`

	petsc.Initialize(os.Args, "", help)

	n, ok := petsc.OptionsGetInt("", "-n")
	if !ok {
		n = 5
	}

	// Create vector.
	x := petsc.NewVec(petsc.CommWorld)
	x.SetSizes(petsc.Decide, n)
	x.SetFromOptions()

	x.Set(1.0)
	x.SetValue(0, 0.0, petsc.InsertValues)
	x.SetValue(n-1, 2.0, petsc.InsertValues)
	x.AssemblyBegin()
	x.AssemblyEnd()

	x.View(petsc.ViewerStdOutWorld)
	idx, value := x.Max()
	petsc.Printf(petsc.CommWorld, "Maximum value %g index %d\n", value, idx)
	idx, value = x.Min()
	petsc.Printf(petsc.CommWorld, "Minimum value %g index %d\n", value, idx)

	x.Destroy()

	petsc.Finalize()
}
