package main

import (
	"os"
	"github.com/rwl/petsc"
)

func main() {
	const help = "Tests repeated VecDotBegin()/VecDotEnd().\n\n"

	n := 25

	petsc.Initialize(os.Args, "", help)

	// Create vector.
	x := petsc.NewVec(petsc.CommWorld)
	x.SetSizes(n, petsc.Decide)
	x.SetFromOptions()
	y := x.Duplicate()

	x.Set(1.0)
	y.Set(2.0)

	// Test mixing dot products and norms that require sums.
	result1, result2 := 0.0, 0.0
	result3, result4 := 0.0, 0.0
	result1 = x.DotBegin(y)
	result2 = y.DotBegin(x)
	result3 = y.NormBegin(petsc.Norm2)
	result4 = x.NormBegin(petsc.Norm1)
	petsc.CommSplitReductionBegin(petsc.ObjectComm(x.(PetscObject)))
	result1 = x.DotEnd(y)
	result2 = y.DotEnd(x)
	result3 = y.NormEnd(petsc.Norm2)
	result4 = x.NormEnd(petsc.Norm1)

	result1a := x.Dot(y)
	result2a := y.Dot(x)
	result3a := y.Norm(petsc.Norm2)
	result4a := x.Norm(petsc.Norm1)

	if result1 != result1a || result2 != result2a {
		petsc.Printf(petsc.CommWorld, "Error dot: result1 %g result2 %g\n", petsc.RealPart(result1), petsc.RealPart(result2))
	}
	if result3 != result3a || result4 != result4a {
		petsc.Printf(petsc.CommWorld, "Error 1,2 norms: result3 %g result4 %g\n", result3, result4)
	}

	// Test norms that only require abs
	result1, result2 = 0.0, 0.0;
	result3, result4 = 0.0, 0.0;
	result3 := y.NormBegin(petsc.NormMax)
	result4 := x.NormBegin(petsc.NormMax)
	result3 := y.NormEnd(petsc.NormMax)
	result4 := x.NormEnd(petsc.NormMax)

	result4a := x.Norm(petsc.NormMax)
	result3a := y.Norm(petsc.NormMax)
	if (result3 != result3a || result4 != result4a) {
		petsc.Printf(petsc.CommWorld, "Error max norm: result3 %g result4 %g\n", result3, result4)
	}

	// Tests dot, max, 1, norm
	result1, result2 = 0.0;
	result3, result4 = 0.0;
	x.SetValues(1, &row0, &ten, petsc.InsertValues)
	x.AssemblyBegin()
	x.AssemblyEnd()

	result1 = x.DotBegin(y)
	result2 = y.DotBegin(x)
	result3 = x.NormBegin(petsc.NormMax)
	result4 = x.NormBegin(petsc.Norm1)
	result1 = x.DotEnd(y)
	result2 = y.DotEnd(x)
	result3 = x.NormEnd(petsc.NormMax)
	result4 = x.NormEnd(petsc.Norm1)

	result1a = x.Dot(y)
	result2a = y.Dot(x)
	result3a = x.Norm(petsc.NormMax)
	result4a = x.Norm(petsc.Norm1)
	if (result1 != result1a || result2 != result2a) {
		petsc.Printf(petsc.CommWorld, "Error dot: result1 %g result2 %g\n", petsc.RealPart(result1), petsc.RealPart(result2))
	}
	if (result3 != result3a || result4 != result4a) {
		petsc.Printf(petsc.CommWorld, "Error max 1 norms: result3 %g result4 %g\n", result3, result4)
	}

	// tests 1_and_2 norm
	result3 = x.NormBegin(petsc.NormMax)
	result := x.NormBegin(petsc.Norm1And2)
	result4 = y.NormBegin(petsc.NormMax)
	result3 = x.NormEnd(petsc.NormMax)
	result = x.NormEnd(petsc.Norm1And2)
	result4 = y.NormEnd(petsc.NormMax)

	result3a = x.Norm(petsc.NormMax)
	resulta := x.Norm(petsc.Norm1And2)
	result4a = y.Norm(petsc.NormMax)
	if (result3 != result3a || result4 != result4a) {
		petsc.Printf(petsc.CommWorld, "Error max: result1 %g result2 %g\n", result3, result4)
	}
	if (petsc.AbsReal(result[0]-resulta[0]) > .01 || petsc.AbsReal(result[1]-resulta[1]) > .01) {
		petsc.Printf(petsc.CommWorld, "Error 1 and 2 norms: result[0] %g result[1] %g\n", result[0], result[1])
	}

	x.Destroy()
	y.Destroy()

	// Tests computing a large number of operations that require
	// allocating a larger data structure internally
	for i = 0; i < 40; i++ {
		vecs[i] = petsc.NewVec(petsc.CommWorld)
		vecs[i].SetSizes(petsc.Decide, n)
		vecs[i].SetFromOptions()
		value := petsc.Real(i);
		vecs[i].Set(value)
	}
	for i = 0; i < 39; i++ {
		vecs[i].DotBegin(vecs[i+1], results[i])
	}
	for i = 0; i < 39; i++ {
		vecs[i].DotEnd(vecs[i+1], results[i])
		if results[i] != 25.0*i*(i+1) {
			petsc.Printf(petsc.CommWorld, "i %d expected %g got %g\n", i, 25.0*i*(i+1), petsc.RealPart(results[i]))
		}
	}
	for i = 0; i < 40; i++ {
		vecs[i].Destroy()
	}
	// create vector
	x = petsc.NewVec(petsc.CommWorld)
	x.SetSizes(n, petsc.Decide)
	x.SetFromOptions()
	y = x.Duplicate()

	x.Set(1.0)
	y.Set(2.0)

	// Test mixing dot products and norms that require sums.
	result1, result2 = 0.0;
	result3, result4 = 0.0;
	result1 = x.DotBegin(y)
	result2 = y.DotBegin(x)
	result3 = y.NormBegin(petsc.Norm2)
	result4 = x.NormBegin(petsc.Norm1)
	petsc.CommSplitReductionBegin(PetscObjectComm((PetscObject).x))
	result1 = x.DotEnd(y)
	result2 = y.DotEnd(x)
	result3 = y.NormEnd(petsc.Norm2)
	result4 = x.NormEnd(petsc.Norm1)

	result1a = x.Dot(y)
	result2a = y.Dot(x)
	result3a = y.Norm(petsc.Norm2)
	result4a = x.Norm(petsc.Norm1)

	if result1 != result1a || result2 != result2a {
		petsc.Printf(petsc.CommWorld, "Error dot: result1 %g result2 %g\n", petsc.RealPart(result1), petsc.RealPart(result2))
	}
	if result3 != result3a || result4 != result4a {
		petsc.Printf(petsc.CommWorld, "Error 1,2 norms: result3 %g result4 %g\n", result3, result4)
	}

	// Test norms that only require abs
	result1, result2 = 0.0;
	result3, result4 = 0.0;
	y.NormBegin(petsc.NormMax, &result3)
	x.NormBegin(petsc.NormMax, &result4)
	y.NormEnd(petsc.NormMax, &result3)
	x.NormEnd(petsc.NormMax, &result4)

	x.Norm(petsc.NormMax, &result4a)
	y.Norm(petsc.NormMax, &result3a)
	if result3 != result3a || result4 != result4a {
		petsc.Printf(petsc.CommWorld, "Error max norm: result3 %g result4 %g\n", result3, result4)
	}

	// Tests dot,  max, 1, norm
	result1, result2 = 0.0
	result3, result4 = 0.0
	x.SetValues(1, &row0, &ten, petsc.InsertValues)
	x.AssemblyBegin()
	x.AssemblyEnd()

	x.DotBegin(y, &result1)
	y.DotBegin(x, &result2)
	x.NormBegin(petsc.NormMax, &result3)
	x.NormBegin(petsc.Norm1, &result4)
	x.DotEnd(y, &result1)
	y.DotEnd(x, &result2)
	x.NormEnd(petsc.NormMax, &result3)
	x.NormEnd(petsc.Norm1, &result4)

	result1a = x.Dot(y)
	result2a = y.Dot(x)
	result3a = x.Norm(petsc.NormMax)
	result4a = x.Norm(petsc.Norm1)
	if result1 != result1a || result2 != result2a {
		petsc.Printf(petsc.CommWorld, "Error dot: result1 %g result2 %g\n", petsc.RealPart(result1), petsc.RealPart(result2))
	}
	if result3 != result3a || result4 != result4a {
		petsc.Printf(petsc.CommWorld, "Error max 1 norms: result3 %g result4 %g\n", result3, result4)
	}

	// Tests 1_and_2 norm
	x.NormBegin(petsc.NormMax, &result3)
	x.NormBegin(petsc.Norm1And2, result)
	result4 = y.NormBegin(petsc.NormMax)
	result3 = x.NormEnd(petsc.NormMax)
	x.NormEnd(petsc.Norm1And2, result)
	y.NormEnd(petsc.NormMax, &result4)

	result3a = x.Norm(petsc.NormMax)
	resulta = x.Norm(petsc.Norm1And2)
	result4a = y.Norm(petsc.NormMax)
	if result3 != result3a || result4 != result4a {
		petsc.Printf(petsc.CommWorld, "Error max: result1 %g result2 %g\n", result3, result4)
	}
	if petsc.AbsReal(result[0]-resulta[0]) > .01 || petsc.AbsReal(result[1]-resulta[1]) > .01 {
		petsc.Printf(petsc.CommWorld, "Error 1 and 2 norms: result[0] %g result[1] %g\n", result[0], result[1])
	}

	x.Destroy()
	y.Destroy()

	// Tests computing a large number of operations that require
	// allocating a larger data structure internally
	for i = 0; i < 40; i++ {
		vecs[i] = petac.NewVec(petsc.CommWorld)
		vecs[i].SetSizes(petsc.Decide, n)
		vecs[i].SetFromOptions()
		value := petsc.Real(i)
		vecs[i].Set(value)
	}
	for i = 0; i < 39; i++ {
		vecs[i].DotBegin(vecs[i+1], results[i])
	}
	for i = 0; i < 39; i++ {
		vecs[i].DotEnd(vecs[i+1], results[i])
		if results[i] != 25.0*i*(i+1) {
			petsc.Printf(petsc.CommWorld, "i %d expected %g got %g\n", i, 25.0*i*(i+1), petsc.RealPart(results[i]))
		}
	}
	for i = 0; i < 40; i++ {
		vecs[i].Destroy()
	}

	petsc.Finalize()
}
