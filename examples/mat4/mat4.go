package main

import (
	"os"
	"github.com/rwl/petsc"
)

const help = "Creates a matrix, inserts some values, and tests MatGetSubMatrices() and MatZeroEntries().\n\n";

func main() {
//	Mat            mat,submat,*submatrices;
//	PetscInt       m = 10,n = 10,i = 4,tmp;
//	PetscErrorCode ierr;
//	IS             irkeep,ickeep;
//	PetscScalar    value = 1.0;
//	PetscViewer    sviewer;
	m, n/*, i*/ := 10, 10/*, 4*/
	value := 1.0

	petsc.Initialize(os.Args, "", help)

	petsc.ViewerStdOutWorld.SetFormat(petsc.ViewerASCIICommon)
	petsc.ViewerStdOutSelf.SetFormat(petsc.ViewerASCIICommon)

	mat := petsc.NewMat(petsc.CommWorld)
	mat.SetSizes(petsc.Decide, petsc.Decide, m, n)
	mat.SetFromOptions()
	for i := 0; i < m; i++ {
		value = float64(i+1)
		tmp := i % 5
		mat.SetValue(tmp, i, value, petsc.InsertValues)
	}
	mat.AssemblyBegin(petsc.MatFinalAssembly)
	mat.AssemblyEnd(petsc.MatFinalAssembly)
	petsc.ViewerStdOutWorld.ASCIIPrintf("Original matrix\n")
	mat.View(petsc.ViewerStdOutWorld)

	// Form submatrix with rows 2-4 and columns 4-8.
	irkeep := petsc.NewISStride(petsc.CommSelf, 3, 2, 1)
	ickeep := petsc.NewISStride(petsc.CommSelf, 5, 4, 1)
	submatrices := mat.SubMatrices(1, []petsc.IS {*irkeep}, []petsc.IS {*ickeep}, petsc.MatInitialMatrix)
	submat := submatrices[0]
//	petsc.Free(submatrices)

	// sviewer will cause the submatrices (one per processor) to be printed
	// in the correct order.
	petsc.ViewerStdOutWorld.ASCIIPrintf("Submatrices\n")
	petsc.ViewerStdOutWorld.ASCIISynchronizedAllow(true)
	sviewer := petsc.ViewerStdOutWorld.Singleton()
	submat.View(sviewer)
	petsc.ViewerStdOutWorld.RestoreSingleton(sviewer)
	petsc.ViewerStdOutWorld.Flush()

	// Zero the original matrix.
	petsc.ViewerStdOutWorld.ASCIIPrintf("Original zeroed matrix\n")
	mat.ZeroEntries()
	mat.View(petsc.ViewerStdOutWorld)

	irkeep.Destroy()
	ickeep.Destroy()
	submat.Destroy()
	mat.Destroy()
	petsc.Finalize();
}
