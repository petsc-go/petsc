package main

import (
	"os"
	"github.com/rwl/petsc"
)

const help = `Compares BLAS dots on different machines. Input
arguments are
-n <length> : local vector length\n`

func main() {
	petsc.Initialize(os.Args, "", help)
	n, ok := petsc.OptionsGetInt("", "-n")
	if !ok {
		n = 15
	} else if n < 5 {
		n = 5
	}

	// Create two vectors.
	x := petsc.NewVecSeq(petsc.CommSelf, n)
	y := petsc.NewVecSeq(petsc.CommSelf, n)

	for i := 0; i < n; i++ {
		v := float64(i) + 1.0/(float64(i) + 0.35)
		x.SetValue(i, v, petsc.InsertValues)
		v += 1.375547826473644376
		y.SetValue(i, v, petsc.InsertValues)
	}
	y.AssemblyBegin()
	y.AssemblyEnd()

	v := x.Dot(y)
	petsc.Printf(petsc.CommWorld, "Vector inner product %16.12e\n", v)

	x.Destroy()
	y.Destroy()

	petsc.Finalize()
}
