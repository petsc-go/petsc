package main

import (
	"os"
	"github.com/rwl/petsc"
)

const help = `Tests MatMult(), MatMultAdd(), MatMultTranspose().
Also MatMultTransposeAdd(), MatScale(), MatGetDiagonal(), and MatDiagonalScale().\n`

func main() {
//	Mat            C;
//	Vec            s,u,w,x,y,z;
//	PetscErrorCode ierr;
//	PetscInt       i,j,m = 8,n,rstart,rend,vstart,vend;
//	PetscScalar    one = 1.0,negone = -1.0,v,alpha=0.1;
//	PetscReal      norm;
//	PetscBool      flg;
	one, negone := 1.0, -1.0
	alpha := 0.1

	petsc.Initialize(os.Args, "", help)
	petsc.ViewerStdOutWorld.SetFormat(petsc.ViewerASCIICommon)
	m, ok := petsc.OptionsGetInt("", "-m")
	if !ok {
		m = 8
	}
	n    := m
	if petsc.OptionsHasName("", "-rectA") {
		n += 2
	}
	if petsc.OptionsHasName("", "-rectB") {
		n -= 2
	}

	/* ---------- Assemble matrix and vectors ----------- */

	C := petsc.NewMat(petsc.CommWorld)
	C.SetSizes(petsc.Decide, petsc.Decide, m, n)
	C.SetFromOptions()
	C.SetUp()
	rstart, rend := C.OwnershipRange()
	x := petsc.NewVec(petsc.CommWorld)
	x.SetSizes(petsc.Decide, m)
	x.SetFromOptions()
	z := x.Duplicate()
	w := x.Duplicate()
	y := petsc.NewVec(petsc.CommWorld)
	y.SetSizes(petsc.Decide, n)
	y.SetFromOptions()
	u := y.Duplicate()
	s := y.Duplicate()
	vstart, vend := y.OwnershipRange()

	// Assembly
	for i := rstart; i < rend; i++ {
		v    := float64(100*(i+1))
		z.SetValue(i, v, petsc.InsertValues)
		for j:=0; j<n; j++ {
			v    = float64(10*(i+1)+j+1)
			C.SetValue(i, j, v, petsc.InsertValues)
		}
	}

	// Flush off proc Vec values and do more assembly.
	z.AssemblyBegin()
	for i:=vstart; i<vend; i++ {
		v    := one*(float64(i))
		y.SetValue(i, v, petsc.InsertValues)
		v    = float64(100.0*i)
		u.SetValue(i, v, petsc.InsertValues)
	}

	// Flush off proc Mat values and do more assembly.
	C.AssemblyBegin(petsc.MatFlushAssembly)
	for i:=rstart; i<rend; i++ {
		for j:=0; j<n; j++ {
			v    := float64(10*(i+1)+j+1)
			C.SetValue(i, j, v, petsc.InsertValues)
		}
	}
	// Try overlap Communication with the next stage SetValues.
	z.AssemblyEnd()
	C.AssemblyEnd(petsc.MatFlushAssembly)
//	petsc.CHKMEMQ
	// The Assembly for the second Stage.
	C.AssemblyBegin(petsc.MatFinalAssembly)
	C.AssemblyEnd(petsc.MatFinalAssembly)
	y.AssemblyBegin()
	y.AssemblyEnd()
	C.Scale(alpha)
	u.AssemblyBegin()
	u.AssemblyEnd()

	// Test Mult(), MultAdd()

	petsc.Printf(petsc.CommWorld, "testing MatMult()\n")
//	petsc.CHKMEMQ
	C.Mult(y, x)
//	petsc.CHKMEMQ
	x.View(petsc.ViewerStdOutWorld)
	petsc.Printf(petsc.CommWorld, "testing MatMultAdd()\n")
	C.MultAdd(y, z, w)
	x.AXPY(one, z)
	x.AXPY(negone, w)
	norm := x.Norm(petsc.Norm2)
	if norm > 1.e-8 {
		petsc.Printf(petsc.CommWorld, "Norm of error difference = %g\n", norm)
	}

	// Test MultTranspose(), MultTransposeAdd()

	for i:=rstart; i<rend; i++ {
		v    := one*(float64(i))
		x.SetValue(i, v, petsc.InsertValues)
	}
	x.AssemblyBegin()
	x.AssemblyEnd()
	petsc.Printf(petsc.CommWorld, "testing MatMultTranspose()\n")
	C.MultTranspose(x, y)
	y.View(petsc.ViewerStdOutWorld)

	petsc.Printf(petsc.CommWorld, "testing MatMultTransposeAdd()\n")
	C.MultTransposeAdd(x, u, s)
	y.AXPY(one, u)
	y.AXPY(negone, s)
	norm = y.Norm(petsc.Norm2)
	if norm > 1.e-8 {
		petsc.Printf(petsc.CommWorld, "Norm of error difference = %g\n", norm)
	}

	// Test GetDiagonal()

	petsc.Printf(petsc.CommWorld, "testing MatGetDiagonal(), MatDiagonalScale()\n")
	C.View(petsc.ViewerStdOutWorld)
	x.Set(one)
	C.Diagonal(x)
	x.View(petsc.ViewerStdOutWorld)
	for i:=vstart; i<vend; i++ {
		v    := one*(float64((i+1)))
		y.SetValue(i, v, petsc.InsertValues)
	}

	// Test DiagonalScale()

	if petsc.OptionsHasName("", "-test_diagonalscale") {
		C.ScaleDiagonal(x, y)
		C.View(petsc.ViewerStdOutWorld)
	}
	// Free data structures.
	u.Destroy()
	s.Destroy()
	w.Destroy()
	x.Destroy()
	y.Destroy()
	z.Destroy()
	C.Destroy()

	petsc.Finalize()
}
