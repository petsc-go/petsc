package main

import (
	"os"
	"io/ioutil"
	"regexp"
	"bytes"
	"fmt"
	"html/template"
	"net/http"
	"os/exec"
	"strings"
	"path"
	"time"
)

const examplesDef = `
<!doctype html>
<html>
<head lang="en">
  <meta charset="UTF-8">
  <title>PETSc</title>
  <style>
    body {
      font-family: RobotoDraft, 'Helvetica Neue', Helvetica, Arial;
    }
    td.heading {
      text-align: center;
    }
    td.examples {
      vertical-align: top;
    }
    ul {
      margin-top: 0px;
    }
    li {
      white-space: nowrap;
    }
  </style>
</head>
<body>

<table>
<thead><tr>{{range .}}<th colspan="2">{{.Name}}</th>{{end}}<tr></thead>
<tr>
  {{range .}}
    <td class="heading">Tests</td>
    <td class="heading">Tutorials</td>
  {{end}}
<tr>
<tr>
  {{range .}}
  {{$n := .Name}}
  {{$group := .}}
  <td class="examples">
    <ul>
      {{range $i, $target := .RunTests}}
        <li>
          <a href="/{{printf $n}}/tests/{{.}}">{{.}}</a>
          <a href="/{{printf $n}}/tests/{{index $group.BuildTests $i}}.c">.c</a>
        </li>
      {{end}}
    </ul>
  </td>
  <td class="examples">
    <ul>
      {{range .RunTutorials}}
        <li>
          <a href="/{{printf $n}}/tutorials/{{.}}">{{.}}</a>
          <a href="/{{printf $n}}/tutorials/{{.}}.c">.c</a>
        </li>
      {{end}}
    </ul>
  </td>
  {{end}}
</tr>
</table>

</body>
`

const outputDef = `
<!doctype html>
<html>
<head lang="en">
  <meta charset="UTF-8">
  <title>PETSc</title>
</head>
<body>
<h3>Build:</h3>
Completed in: {{.BuildDuration}}
<pre>{{.BuildOutput}}</pre>

<h3>Run:</h3>
Completed in: {{.RunDuration}}
<pre>{{.RunOutput}}</pre>
</body>
`

var examplesTmpl = template.Must(template.New("examples").Parse(examplesDef))
var outputTmpl = template.Must(template.New("output").Parse(outputDef))

//type Example struct {
//	Name    string
//	Path    string
//	Command string
//}

type ExampleGroup struct {
	Name string
	Dir string
	RunTests []string
	RunTutorials []string
	BuildTests []string
	BuildTutorials []string
}

var vecTests = &ExampleGroup{
	Name: "vec",
	Dir: "src/vec/vec/examples",
}

var matTests = &ExampleGroup{
	Name: "mat",
	Dir: "src/mat/examples",
}

var snesTests = &ExampleGroup{
	Name: "snes",
	Dir: "src/snes/examples/",
}

var kspTests = &ExampleGroup{
	Name: "ksp",
	Dir: "src/ksp/ksp/examples",
}

var dmTests = &ExampleGroup{
	Name: "dm",
	Dir: "src/dm/examples",
}

var sysTests = &ExampleGroup{
	Name: "sys",
	Dir: "src/sys/examples",
}

var tsTests = &ExampleGroup{
	Name: "ts",
	Dir: "src/ts/examples",
}

var groups = []*ExampleGroup{
	vecTests, matTests, snesTests, kspTests, dmTests, sysTests, tsTests,
}

var runTarget = regexp.MustCompile(`(?m)^run(ex\w*):\n\t-@\$\{MPIEXEC\}\s+-n\s+\d+\s+\./(ex\w*)`)

type ExampleResult struct {
	BuildOutput string
	RunOutput string
	BuildDuration time.Duration
	RunDuration time.Duration
}

func readMakefiles(petscDir string, subdir string) {
	for i, group := range groups {
		os.Chdir(path.Join(petscDir, group.Dir, subdir))
		makefile, err := ioutil.ReadFile("makefile")
		if err != nil {
			fmt.Fprintf(os.Stderr, "petsc: makefile error %s\n", err)
			os.Exit(1)
		}
		all := runTarget.FindAllSubmatch(makefile, -1)
		runTargets := make([]string, 0, len(all))
		buildTargets := make([]string, 0, len(all))
		for _, submatches := range all {
			if len(submatches) != 3 {
				fmt.Fprintf(os.Stderr, "petsc: %s %#v\n", group.Name, submatches)
				continue
			}
			runMatch := string(submatches[1])
			buildMatch := string(submatches[2])
			runTargets = append(runTargets, runMatch)
			buildTargets = append(buildTargets, buildMatch)
		}
		if subdir == "tests" {
			groups[i].RunTests = runTargets
			groups[i].BuildTests = buildTargets
		} else {
			groups[i].RunTutorials = runTargets
			groups[i].BuildTutorials = buildTargets
		}
	}
}

func rootHandler(petscDir string) func(http.ResponseWriter, *http.Request) {
	readMakefiles(petscDir, "tests")
	readMakefiles(petscDir, "tutorials")

	examplesPage := &bytes.Buffer{}
	err := examplesTmpl.Execute(examplesPage, groups)
	if err != nil {
		fmt.Fprintf(os.Stderr, "petsc: template error %s\n", err)
		os.Exit(1)
	}
	return func(w http.ResponseWriter, _ *http.Request) {
		fmt.Fprintf(w, examplesPage.String())
	}
}

func exampleHandler(group *ExampleGroup, petscDir, subdir, build, run string) func(http.ResponseWriter, *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		exdir := path.Join(petscDir, group.Dir, subdir)
		err := os.Chdir(exdir)
		if err != nil {
			fmt.Fprintf(os.Stderr, "petsc: %s\n", err)
			http.Redirect(w, r, "/", 301)
			return
		}

		// build
		cmd := exec.Command("make", build)

		cmd.Env = os.Environ()
		cmd.Env = append(cmd.Env, "PETSC_DIR="+petscDir)
		cmd.Env = append(cmd.Env, "PETSC_ARCH=arch-linux2-c-debug")

		fmt.Printf("==> Executing: %s$ %s\n", exdir, strings.Join(cmd.Args, " "))
		start := time.Now()
		buildout, err := cmd.CombinedOutput()
		if err != nil {
			fmt.Fprintf(os.Stderr, "petsc: %s\n", err)
		}
		buildDuration := time.Since(start)


		// run
		cmd = exec.Command("make", run)

		cmd.Env = os.Environ()
		cmd.Env = append(cmd.Env, "PETSC_DIR="+petscDir)
		cmd.Env = append(cmd.Env, "PETSC_ARCH=arch-linux2-c-debug")

		fmt.Printf("==> Executing: %s$ %s\n", exdir, strings.Join(cmd.Args, " "))

//		output := &bytes.Buffer{}
//		cmd.Stdout = output

//		err = cmd.Run()
		start = time.Now()
		output, err := cmd.CombinedOutput()
		if err != nil {
			fmt.Fprintf(os.Stderr, "petsc: %s\n", err)
		}
		runDuration := time.Since(start)

		exampleOutput := &ExampleResult{
			string(buildout),
			string(output),
			buildDuration,
			runDuration,
		}

		err = outputTmpl.Execute(w, exampleOutput)
		if err != nil {
			fmt.Fprintf(os.Stderr, "petsc: template error %s\n", err)
			os.Exit(1)
		}
	}
}

func main() {

	petscDir := os.Getenv("PETSC_DIR")
	if len(petscDir) == 0 {
//		fmt.Fprintf(os.Stderr, "petsc: empty PETSC_DIR value\n")
//		os.Exit(2)
		petscDir = "/home/rwl/tmp/petsc-3.5.2"
	}

	http.HandleFunc("/", rootHandler(petscDir))

	for _, group := range groups {
		for i, runTarget := range group.RunTests {
			p := path.Join("/", group.Name, "tests", runTarget)
			http.HandleFunc(p, exampleHandler(group, petscDir, "tests",
					group.BuildTests[i], "run" + runTarget))
		}

		for i, runTarget := range group.RunTutorials {
			p := path.Join("/", group.Name, "tutorials", runTarget)
			http.HandleFunc(p, exampleHandler(group, petscDir, "tutorials",
					group.BuildTutorials[i], "run" + runTarget))
		}
	}
	if err := http.ListenAndServe(":8080", nil); err != nil {
		fmt.Fprintf(os.Stderr, "petsc: server error %s\n", err)
		os.Exit(1)
	}
}
