package main

// Formatted test for TS routines.
//
//     Solves U_t=F(t,u)
//     Where:
//
//             [2*u1+u2
//     F(t,u)= [u1+2*u2+u3
//             [   u2+2*u3
//
// We can compare the solutions from euler, beuler and SUNDIALS to
// see what is the difference.

import (
	"github.com/rwl/petsc"
	"os"
)

const help ="Solves a nonlinear ODE.\n\n"

func main() {
	var A_structure petsc.MatStructure
	petsc.Initialize(os.Args, "", help)

	time_steps, set = petsc.OptionsGetInt("", "-time")
	if !set {
		time_steps = 100
	}

	// Set initial conditions.
	global := petsc.NewVec(petsc.CommWorld)
	global.SetSizes(petsc.Decide, 3)
	global.SetFromOptions()
	Initial(global, nil)

	// Make timestep context.
	ts := petsc.NewTS()
	ts.ProblemType(petsc.TSNonLinear)
	ts.Monitor(Monitor, nil, nil)

	dt := 0.1

	// User provides the RHS and Jacobian.
	ts.RHSFunction(nil, RHSFunction, nil)
	A := petsc.NewMat(petsc.CommWorld)
	A.Sizes(petsc.Decide, petsc.Decide, 3, 3)
	A.FromOptions()
	A.SetUp()
	ts.RHSJacobian(0.0, global, &A, &A, &A_structure, nil)
	ts.RHSJacobian(A, A, RHSJacobian, nil)

	ts.FromOptions()

	ts.InitialTimeStep(0.0, dt)
	ts.Duration(time_steps, 1)
	ts.Solution(global)

	ts.Solve(global)
	ftime := ts.SolveTime()
	steps := ts.TimeStepNumber()

	// Free data structures.
	ts.Destroy()
	global.Destroy()
	A.Destroy()

	petsc.Finalize()
}

// This test problem has initial values (1, 1, 1).
func Initial(global *petsc.Vec, ctx interface{}) {
	// Determine starting point of each processor.
	mybase, myend := global.OwnershipRange()
	locsize := global.LocalSize()

	// Initialize the array.
	local := global.Array()
	for i := 0; i < locsize; i++ {
		local[i] = 1.0
	}

	if mybase == 0 {
		local[0] = 1.0
	}

	global.RestoreArray(&local)
}

func Monitor(ts *petsc.TS, step int, time float64, global *petsc.Vec, ctx interface{}) {
	// Get the size of the vector.
	n := global.Size()

	// Set the index sets.
	idx := make([]int, n)
	for i := 0; i < n; i++ {
		idx[i] = i
	}

	// Create local sequential vectors.
	tmp_vec := petsc.NewVecSeqComm(petsc.CommSelf, n)

	// Create scatter context.
	from := petsc.NewISGeneralComm(petsc.CommSelf, n, idx, petsc.CopyValues)
	to := petsc.NewISGeneralComm(petsc.CommSelf, n, idx, petsc.CopyValues)
	scatter := petsc.NewVecScatter(global, from, tmp_vec, to)
	scatter.Begin(global, tmp_vec, petsc.InsertValues, petsc.ScatterForward)
	scatter.End(global, tmp_vec, petsc.InsertValues, petsc.ScatterForward)

	tmp := tmp_vec.Array()
	petsc.Printf(petsc.CommWorld, "At t =%14.6e u = %14.6e  %14.6e  %14.6e \n", time, petsc.RealPart(tmp[0]), petsc.RealPart(tmp[1]), petsc.RealPart(tmp[2]))
	petsc.Printf(petsc.CommWorld, "At t =%14.6e errors = %14.6e  %14.6e  %14.6e \n",
		time, petsc.RealPart(tmp[0]-solx(time)), petsc.RealPart(tmp[1]-soly(time)), petsc.RealPart(tmp[2]-solz(time)))
	tmp_vec.RestoreArray(&tmp)
	scatter.Destroy()
	from.Destroy()
	to.Destroy()
	idx = nil
	tmp_vec.Destroy()
}

func RHSFunction(ts *petsc.TS, t float64, globalin *Vec, globalout *Vec, ctx interface{}) {
	// Get the length of parallel vector.
	n := global.Size()

	// Set the index sets.
	idx := make([]int, n)
	for i := 0; i < n; i++ {
		idx[i] = i
	}

	// Create local sequential vectors.
	tmp_in := petsc.NewVecSeqComm(petsc.CommSelf, n)
	tmp_out := tmp_in.Duplicate()

	// Create scatter context.
	from := petsc.NewISGeneral(petsc.CommSelf, n, idx, petsc.CopyValues)
	to := petsc.NewISGeneral(petsc.CommSelf, n, idx, petsc.CopyValues)
	scatter := petsc.NewVecScatter(globalin, from, tmp_in, to)
	scatter.Begin(globalin, tmp_in, petsc.InsertValues, petsc.ScatterForward)
	scatter.End(globalin, tmp_in, petsc.InsertValues, petsc.ScatterForward)
	scatter.Destroy()

	// Extract income array.
	in := tmp_in.Array()

	// Extract outcome array.
	out := tmp_out.Array()

	outptr[0] = 2.0*inptr[0]+inptr[1]
	outptr[1] = inptr[0]+2.0*inptr[1]+inptr[2]
	outptr[2] = inptr[1]+2.0*inptr[2]

	tmp_in.RestoreArray(&inptr)
	tmp_out.RestoreArray(&outptr)

	scatter := NewVecScatter(tmp_out, from, globalout, to)
	scatter.Begin(tmp_out, globalout, petsc.InsertValues, petsc.ScatterForward)
	scatter.End(tmp_out, globalout, petsc.InsertValues, petsc.ScatterForward)

	// Destroy idx aand scatter.
	from.Destroy()
	to.Destroy()
	scatter.Destroy()
	tmp_in.Destroy()
	tmp_out.Destroy()
	idx = nil
}

func RHSJacobian(ts *petsc.TS, t float64, x *petsc.Vec, AA ,BB *petsc.Mat, str *petsc.MatStructure, ctx interface{}) {
	*str = petsc.SameNonZeroPattern

	idx := []int{0, 1, 2}
	tmp := x.Array()
	
	i := []int{0}
	v := []float64{2.0, 1.0, 0.0}
	AA.SetValues(i, idx, v, petsc.InsertValues)
	
	i = []int{1}
	v = []float64{1.0, 2.0, 1.0}
	AA.SetValues(i, idx, v, petsc.InsertValues)
	
	i = []int{2}
	v = []float64{0.0, 1.0, 2.0}
	AA.SetValues(i, idx, v, petsc.InsertValues)
	
	AA.AssemblyBegin(petsc.MatFinalAssembly)
	AA.AssemblyEnd(petsc.MatFinalAssembly)
	
	x.RestoreArray(&tmp)
}

var root2 = math.Sqrt(2.0)

// The exact solutions
func solx(t float64) float64 {
	return exp((2.0 - root2)*t)/2.0 - exp((2.0 - root2)*t)/(2.0*root2) +
		exp((2.0 + root2)*t)/2.0 + exp((2.0 + root2)*t)/(2.0*root2)
}

func soly(t float64) float64 {
	return exp((2.0 - root2)*t)/2.0 - exp((2.0 - root2)*t)/root2 +
		exp((2.0 + root2)*t)/2.0 + exp((2.0 + root2)*t)/root2
}

func solz(t float64) float64 {
	return exp((2.0 - root2)*t)/2.0 - exp((2.0 - root2)*t)/(2.0*root2) +
		exp((2.0 + root2)*t)/2.0 + exp((2.0 + root2)*t)/(2.0*root2)
}
