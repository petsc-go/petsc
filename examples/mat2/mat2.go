package main

import (
	"fmt"
	"os"
	"github.com/rwl/petsc"
)

const help = "Tests MatTranspose(), MatNorm(), MatAXPY() and MatAYPX().\n\n"

func main() {
	rect := false
	var tmat *petsc.Mat
	var alpha float64

	petsc.Initialize(os.Args, "", help)

//	petsc.ViewerStdOutWorld.SetFormat(petsc.ViewerASCIICommon)
	m, ok := petsc.OptionsGetInt("", "-m")
	if !ok {
		m = 7
	}
	n := m;
	flg := petsc.OptionsHasName("", "-rectA")
	if flg {
		n += 2
		rect = true
	}
	flg = petsc.OptionsHasName("", "-rectB")
	if flg {
		n -= 2
		rect = true
	}

	// Assemble matrix, test MatValid().
	mat := petsc.NewMat(petsc.CommWorld)
	mat.SetSizes(petsc.Decide, petsc.Decide, m, n)
	mat.SetFromOptions()
	mat.SetUp()
	rstart, rend := mat.OwnershipRange()
	for i := rstart; i < rend; i++ {
		for j := 0; j < n; j++ {
			v := float64(10.0*i+j)
			mat.SetValues([]int{i}, []int{j}, []float64{v}, petsc.InsertValues)
		}
	}
	mat.AssemblyBegin(petsc.MatFinalAssembly)
	mat.AssemblyEnd(petsc.MatFinalAssembly)
	
	// Test MatNorm().
	normf := mat.Norm(petsc.NormFrobenius)
	norm1 := mat.Norm(petsc.Norm1)
	normi := mat.Norm(petsc.NormInfinity)
	//petsc.Printf(petsc.CommWorld, "original A: Frobenious norm = %G, one norm = %G, infinity norm = %G\n", normf, norm1, normi)
	fmt.Printf("original A: Frobenious norm = %g, one norm = %g, infinity norm = %g\n", normf, norm1, normi)
	mat.View(petsc.ViewerStdOutWorld)
	
	// Test MatTranspose().
	flg = petsc.OptionsHasName("", "-in_place")
	if !rect && flg {
		mat = mat.Transpose(petsc.MatReuseMatrix) // In-place transpose.
		tmat = mat
		mat = nil
	} else {  // Out-of-place transpose.
		tmat = mat.Transpose(petsc.MatInitialMatrix)
	}
	
	// Test MatNorm().
	// Print info about transpose matrix.
	normf = tmat.Norm(petsc.NormFrobenius)
	norm1 = tmat.Norm(petsc.Norm1)
	normi = tmat.Norm(petsc.NormInfinity)
	//petsc.Printf(petsc.CommWorld, "B = A^T: Frobenious norm = %G, one norm = %G, infinity norm = %G\n", normf, norm1, normi)
	fmt.Printf("B = A^T: Frobenious norm = %g, one norm = %g, infinity norm = %g\n", normf, norm1, normi)
	tmat.View(petsc.ViewerStdOutWorld)
	
	// Test MatAXPY(), MatAYPX().
	if mat != nil && !rect {
		alpha, ok := petsc.OptionsGetScalar("", "-alpha")
		if !ok {
			alpha = 1.0
		}
		petsc.Printf(petsc.CommWorld, "MatAXPY:  B = B + alpha * A\n")
		tmat.AXPY(alpha, mat, petsc.DifferentNonZeroPattern)
		tmat.View(petsc.ViewerStdOutWorld)

		petsc.Printf(petsc.CommWorld, "MatAYPX:  B = alpha*B + A\n")
		tmat.AYPX(alpha, mat, petsc.DifferentNonZeroPattern)
		tmat.View(petsc.ViewerStdOutWorld)
	}
	
	{
		alpha = 1.0
		//petsc.Printf(petsc.CommWorld, "MatAXPY:  C = C + alpha * A, C=A, SAME_NONZERO_PATTERN\n")
		fmt.Printf("MatAXPY:  C = C + alpha * A, C=A, SAME_NONZERO_PATTERN\n")
		C := mat.Duplicate(petsc.MatCopyValues)
		C.AXPY(alpha, mat, petsc.SameNonZeroPattern)
		C.View(petsc.ViewerStdOutWorld)
		C.Destroy()
	}
	
	{
		// Get matB that has nonzeros of mat in all even numbers of row
		// and col.
		matB := petsc.NewMat(petsc.CommWorld)
		matB.SetSizes(petsc.Decide, petsc.Decide, m, n)
		matB.SetFromOptions()
		matB.SetUp()
		rstart, rend := matB.OwnershipRange()
		if rstart % 2 != 0 {
			rstart++
		}
		for i := rstart; i < rend; i += 2 {
			for j := 0; j < n; j += 2 {
				v := float64(10.0*i+j)
				matB.SetValues([]int{i}, []int{j}, []float64{v}, petsc.InsertValues)
			}
		}
		matB.AssemblyBegin(petsc.MatFinalAssembly)
		matB.AssemblyEnd(petsc.MatFinalAssembly)
		//petsc.Printf(petsc.CommWorld, " A: original matrix:\n")
		fmt.Printf(" A: original matrix:\n")
		mat.View(petsc.ViewerStdOutWorld)
		//petsc.Printf(petsc.CommWorld, " B(a subset of A):\n")
		fmt.Printf(" B(a subset of A):\n")
		matB.View(petsc.ViewerStdOutWorld)
		//petsc.Printf(petsc.CommWorld, "MatAXPY:  B = B + alpha * A, SUBSET_NONZERO_PATTERN\n")
		fmt.Printf("MatAXPY:  B = B + alpha * A, SUBSET_NONZERO_PATTERN\n")
		mat.AXPY(alpha, matB, petsc.SubSetNonZeroPattern)
		mat.View(petsc.ViewerStdOutWorld)
		matB.Destroy()
	}
	
	// Free data structures.
	if mat != nil {
		mat.Destroy()
	}
	if tmat != nil {
		tmat.Destroy()
	}

	petsc.Finalize()
}
