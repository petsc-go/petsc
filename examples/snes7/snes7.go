package main

import (
	"github.com/rwl/petsc"
	"os"
	"math"
)

const help = `Solves u'' + u^{2} = f with Newton-like methods. Using\n\
		matrix-free techniques with user-provided explicit preconditioner matrix.\n\n`

type MonitorCtx struct {
	viewer *petsc.Viewer
}

type AppCtx struct {
	precond *petsc.Mat
	variant bool
}

func main() {
	// Monitoring context.
	monP := new(MonitorCtx)
	// User-defined work context.
	user := new(AppCtx)

	// Default nonlinear solution method.
	var typ petsc.SNESType = petsc.SNESNewtonLS
	// Jacobian matrix-free.
	var J *petsc.Mat

	petsc.Initialize(os.Args, "", help)

	n, ok := petsc.OptionsGetInt("", "-n")
	if !ok {
		n = 5
	}
	user.variant = petsc.OptionsHasName("", "-variant")
	h := float64(1.0/(n-1))

	// Set up data structures.
	monP.viewer = petsc.ViewerDrawOpen(petsc.CommSelf, "", "", 0, 0, 400, 400)
	x := petsc.NewVecSeq(petsc.CommSelf, n)
	x.SetName("Approximate Solution")
	r := x.Duplicate()
	F := x.Duplicate()
	U := x.Duplicate()
	U.SetName("Exact Solution")

	// Create explict matrix preconditioner.
	B := petsc.NewMatSeqAIJ(petsc.CommSelf, n, n, 3, nil)
	user.precond = B

	// Store right-hand-side of PDE and exact solution.
	xp := 0.0
	for i := 0; i < n; i++ {
		v := 6.0*xp + math.Pow(xp+1.e-12, 6.0) // +1.e-12 is to prevent 0^6
		F.SetValues([]int{i}, []float64{v}, petsc.InsertValues)
		v = xp*xp*xp;
		U.SetValues([]int{i}, []float64{v}, petsc.InsertValues)
		xp += float64(h)
	}
	
	/* Create nonlinear solver */
	snes := petsc.NewSNES(petsc.CommWorld)
	snes.SetType(typ)
	
	// Set various routines and options.
	snes.SetFunction(r, FormFunction, F)
	if user.variant {
		J = petsc.NewMatMFFD(petsc.CommWorld, n, n, n, n)
		ff := func(x, y *petsc.Vec) petsc.ErrorCode {
			petsc.SNESComputeFunction(nil, x, y, nil)
			return petsc.ErrOK
		}
		J.SetFunction(ff, snes)
	} else {
		// Create matrix free matrix for Jacobian.
		J = snes.NewMatMF()
	}
	
	// Set various routines and options.
	snes.SetJacobian(J, B, FormJacobian, &user)
	snes.MonitorSet(Monitor, monP, nil)
	snes.SetFromOptions()
	
	// Solve nonlinear system.
	FormInitialGuess(snes, x)
	snes.Solve(nil, x)
	its := snes.IterationNumber()
	petsc.Printf(petsc.CommSelf, "number of SNES iterations = %D\n\n", its)
	
	// Free data structures.
	x.Destroy()
	r.Destroy()
	U.Destroy()
	F.Destroy()
	J.Destroy()
	B.Destroy()
	snes.Destroy()
	monP.viewer.Destroy()

	petsc.Finalize()
}

//export GoFormFunction
//func GoFormFunction(snes C.SNES, x C.Vec, f C.Vec, ctx *C.void) C.PetscErrorCode {
//	F := petsc.Vec{C.Vec(ctx)}
//	return FormFunction(&petsc.SNES{SNES}, &petsc.Vec{x}, &petsc.Vec{f}, F)
//}

// Evaluate Function F(x)
func FormFunction(snes *petsc.SNES, x, f *petsc.Vec, ctx interface{}) petsc.ErrorCode {
	xx := x.Array()
	ff := f.Array()
	F := ctx.(*petsc.Vec)
	FF := F.Array()
	n := x.Size()
	d := float64(n - 1)
	d = d*d
	ff[0] = xx[0]
	for i := 1; i < n-1; i++ {
		ff[i] = d*(xx[i-1] - 2.0*xx[i] + xx[i+1]) + xx[i]*xx[i] - FF[i]
	}
	ff[n-1] = xx[n-1] - 1.0
	x.RestoreArray(xx)
	f.RestoreArray(ff)
	F.RestoreArray(FF)
	return 0
}

// Form initial approximation.
func FormInitialGuess(snes *petsc.SNES, x *petsc.Vec) petsc.ErrorCode {
	x.Set(0.5)
	return 0
}

// Evaluate Jacobian F'(x)
//
// Evaluates a matrix that is used to precondition the matrix-free
// jacobian. In this case, the explict preconditioner matrix is
// also EXACTLY the Jacobian. In general, it would be some lower
// order, simplified apprioximation */
func FormJacobian(snes *petsc.SNES, x petsc.Vec, jac, B petsc.Mat, ctx interface{}) petsc.ErrorCode {
	user := ctx.(*AppCtx)

	iter := snes.IterationNumber()

	if iter % 2 == 0 { // Compute new preconditioner matrix.
		petsc.Printf(petsc.CommSelf, "iter=%D, computing new preconditioning matrix\n", iter+1)
		B := user.precond
		xx := x.Array()
		n := x.Size()
		d := float64(n - 1)
		d = d*d

		A := make([]float64, 3)
		j := make([]int, 3)

		i := 0
		A[0] = 1.0
		B.SetValues([]int{i}, []int{i}, []float64{A[0]}, petsc.InsertValues)
		for i := 1; i < n-1; i++ {
			j[0] = i - 1
			j[1] = i
			j[2] = i + 1
			A[0] = d
			A[1] = -2.0*d + 2.0*xx[i]
			A[2] = d
			B.SetValues([]int{i}, j, A, petsc.InsertValues)
		}
		i = n - 1
		A[0] = 1.0
		B.SetValues([]int{i}, []int{i}, []float64{A[0]}, petsc.InsertValues)
		B.AssemblyBegin(petsc.MatFinalAssembly)
		B.AssemblyEnd(petsc.MatFinalAssembly)
		x.RestoreArray(xx)
		//*flag = petsc.SameNonZeroPattern
	} else { // Reuse preconditioner from last iteration.
		petsc.Printf(petsc.CommSelf, "iter=%D, using old preconditioning matrix\n", iter+1)
		//*flag = petsc.SamePreconditioner
	}
	if user.variant {
		jac.MFFDSetBase(&x, nil)
	}
	jac.AssemblyBegin(petsc.MatFinalAssembly)
	jac.AssemblyEnd(petsc.MatFinalAssembly)

	return 0
}

// User-defined monitor.
func Monitor(snes *petsc.SNES, its int, fnorm float64, ctx interface{}) petsc.ErrorCode {
	monP := ctx.(*MonitorCtx)

	comm := snes.Comm()
	petsc.Printf(comm, /*stdout,*/ "iter = %D, SNES Function norm %G \n", its, fnorm)
	x := snes.Solution()
	x.View(monP.viewer)
	return 0
}
