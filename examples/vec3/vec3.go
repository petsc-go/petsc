package main

import (
	"os"
	"github.com/rwl/petsc"
)

func main() {
	const help = `Tests parallel vector assembly.  Input arguments are\n
  -n <length> : local vector length\n\n`


	idx := []int{0}
	one := []float64{1.0}
	two := []float64{2.0}
	three := []float64{3.0}

	petsc.Initialize(os.Args, "", help)

	n, ok := petsc.OptionsGetInt("", "-n")
	if !ok || n < 5 {
		n = 5
	}

	size := petsc.Size(petsc.CommWorld)
	rank := petsc.Rank(petsc.CommWorld)

	if (size < 2) {
		petsc.SetErrQ(petsc.CommSelf, petsc.ErrorUser, "Must be run with at least two processors")
	}

	// Create two vectors.
	x := petsc.NewVecSeq(petsc.CommSelf, n)
	y := petsc.NewVec(petsc.CommWorld)
	y.SetSizes(n, petsc.Decide)
	y.SetFromOptions()
	x.Set(1.0)
	y.Set(2.0)

	if (rank == 1) {
		idx[0] = 2
		y.SetValues(idx, three, petsc.InsertValues)
		idx[0] = 0
		y.SetValues(idx, two, petsc.InsertValues)
		idx[0] = 0
		y.SetValues(idx, one, petsc.InsertValues)
	} else {
		idx[0] = 7
		y.SetValues(idx, three, petsc.InsertValues)
	}
	y.AssemblyBegin()
	y.AssemblyEnd()

	y.View(petsc.ViewerStdOutWorld)

	x.Destroy()
	y.Destroy()

	petsc.Finalize()
}
