package main

import (
	"os"
	"github.com/rwl/petsc"
)

func main() {
	const help = `Tests vector scatter-gather operations. Input arguments are\n
  -n <length> : vector length\n\n`

	idx1 := []int{0, 3}
	idx2 := []int{1, 4}

	petsc.Initialize(os.Args, "", help)

	n, ok := petsc.OptionsGetInt("", "-n")
	if !ok {
		n = 5
	}

	// Create two vectors.
	x := petsc.NewVecSeq(petsc.CommSelf, n)
	y := x.Duplicate()

	// Create two index sets.
	is1 := petsc.NewISGeneral(petsc.CommSelf, idx1, petsc.CopyValues)
	is2 := petsc.NewISGeneral(petsc.CommSelf, idx2, petsc.CopyValues)

	x.Set(1.0)
	y.Set(2.0)
	ctx := petsc.NewVecScatter(x, is1, y, is2)
	ctx.ScatterBegin(x, y, petsc.InsertValues, petsc.ScatterForward)
	ctx.ScatterEnd(x, y, petsc.InsertValues, petsc.ScatterForward)

	y.View(petsc.ViewerStdOutSelf)

	ctx.ScatterBegin(y, x, petsc.InsertValues, petsc.ScatterForward)
	ctx.ScatterEnd(y, x, petsc.InsertValues, petsc.ScatterForward)
	ctx.Destroy()

	petsc.Printf(petsc.CommSelf, "-------\n")
	x.View(petsc.ViewerStdOutSelf)

	is1.Destroy()
	is2.Destroy()

	x.Destroy()
	y.Destroy()

	petsc.Finalize()
	// Output:
	//Vector Object: 1 MPI processes
	//type: seq
	//2
	//1
	//2
	//2
	//1
	//-------
	//Vector Object: 1 MPI processes
	//type: seq
	//1
	//2
	//1
	//1
	//2
}
