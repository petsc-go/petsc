package petsc

/*
#cgo pkg-config: PETSc
#cgo LDFLAGS: -lpetsc -lmpi

#include "petsc.h"
*/
import "C"
import "unsafe"

func scalarArray(a *C.PetscScalar, n C.PetscInt) []float64 {
	var flts []float64
	p := uintptr(unsafe.Pointer(a))
	for i := 0; i < int(n); i++ {
		a = (*C.PetscScalar)(unsafe.Pointer(p))
		flts = append(flts, float64(*a))
		p += unsafe.Sizeof(p)
	}
	return flts
}

func scalarSlice(y []float64) []C.PetscScalar {
	yy := make([]C.PetscScalar, len(y))
	for i, z := range y {
		yy[i] = C.PetscScalar(z)
	}
	return yy
}

func intSlice(ix []int) []C.PetscInt {
	ixx := make([]C.PetscInt, len(ix))
	for i, x := range ix {
		ixx[i] = C.PetscInt(x)
	}
	return ixx
}

func petscBool(b C.PetscBool) bool {
	if b == C.PETSC_TRUE {
		return true
	}
	return false
}

func boolPetsc(b bool) C.PetscBool {
	if b {
		return C.PETSC_TRUE
	}
	return C.PETSC_FALSE
}
