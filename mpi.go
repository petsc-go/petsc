package petsc

/*
#cgo pkg-config: PETSc
#cgo LDFLAGS: -lmpi

#include "mpi.h"
*/
import "C"

// Size returns the size of the group associated with a communicator.
func Size(comm MPI_Comm) int {
	n := C.int(-1)
	C.MPI_Comm_size(C.MPI_Comm(comm), &n)
	return int(n)
}

// Rank returns the rank of the calling process in the communicator.
func Rank(comm MPI_Comm) int {
	n := C.int(-1)
	C.MPI_Comm_rank(C.MPI_Comm(comm), &n)
	return int(n)
}
