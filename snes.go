package petsc

/*
#cgo pkg-config: PETSc
#cgo LDFLAGS: -lpetsc -lmpi

#include "mpi.h"
#include "petscsnes.h"

extern PetscErrorCode GoSNESCallback(SNES snes, Vec x, Vec f, void *ctx);

static inline PetscErrorCode SetSNESFunction(SNES snes, Vec r, void* ctx) {
	return SNESSetFunction(snes, r, &GoSNESCallback, ctx);
}

extern PetscErrorCode GoSNESJacobianCallback(SNES snes, Vec x, Mat A, Mat P, void* ctx);

static inline PetscErrorCode SetSNESJacobian(SNES snes, Mat A, Mat P, void* ctx) {
	return SNESSetJacobian(snes, A, P, &GoSNESJacobianCallback, ctx);
}

static inline const char* GetConvergedReason(SNESConvergedReason reason) {
	return SNESConvergedReasons[reason];
}
*/
import "C"
import "unsafe"
import "runtime"

// Name of a PETSc SNES method.
type SNESType string

const (
	SNESNewtonLS SNESType = "newtonls"
	SNESNewtonTr     = "newtontr"
	SNESPython       = "python"
	SNESTest         = "test"
	SNESNRichardson  = "nrichardson"
	SNESKSPOnly      = "ksponly"
	SNESVINewtonRSLS = "vinewtonrsls"
	SNESVINewtonSSLS = "vinewtonssls"
	SNESNGMRES       = "ngmres"
	SNESQN           = "qn"
	SNESShell        = "shell"
	SNESGS           = "gs"
	SNESNCG          = "ncg"
	SNESFAS          = "fas"
	SNESMS           = "ms"
	SNESNASM         = "nasm"
	SNESAnderson     = "anderson"
	SNESASPIN        = "aspin"
)

// PETSc type that manages all nonlinear solves.
type SNES struct {
	snes_p C.SNES
	snesFn SNESFunction
	snesFnCtx interface{}
	snesJac SNESJacobian
	snesJacCtx interface{}
}

// NewSNES creates a nonlinear solver context.
func NewSNES(comm MPI_Comm) *SNES {
	s := &SNES{}
	runtime.SetFinalizer(s, destroySNES)
	errorCode := C.SNESCreate(C.MPI_Comm(comm), &s.snes_p)
	CheckError(errorCode)
	return s
}

func destroySNES(s *SNES) C.PetscErrorCode {
	return C.SNESDestroy(&s.snes_p)
}

// Destroy destroys the nonlinear solver context that was created
// with NewSNES().
func (s *SNES) Destroy() {
	errorCode := destroySNES(s)
	CheckError(errorCode)
}

// Comm gets the MPI communicator for the solver.
func (s *SNES) Comm() MPI_Comm {
	p := uintptr(unsafe.Pointer(&s.snes_p))
	obj := (C.PetscObject)(unsafe.Pointer(p))
	var comm C.MPI_Comm
	errorCode := C.PetscObjectGetComm(obj, &comm)
	CheckError(errorCode)
	return MPI_Comm(comm)
}

//export GoSNESCallback
func GoSNESCallback(snes C.SNES, x C.Vec, f C.Vec, ctx unsafe.Pointer) C.PetscErrorCode {
	s := (*SNES)(ctx)
	if s.snesFn != nil {
		err := s.snesFn(s, &Vec{x}, &Vec{f}, s.snesFnCtx)
		return C.PetscErrorCode(err)
	}
	return 0
}

type SNESFunction func(*SNES, *Vec, *Vec, interface{}) ErrorCode

// SetFunction sets the function evaluation routine and function
// vector for use by the SNES routines in solving systems of nonlinear
// equations.
func (s *SNES) SetFunction(r *Vec, f SNESFunction, ctx interface{}) {
	s.snesFn = f
	s.snesFnCtx = ctx
	errorCode := C.SetSNESFunction(s.snes_p, r.vec_p, unsafe.Pointer(s))
	CheckError(errorCode)

	//errorCode := C.SNESSetFunction(SNES snes,Vec r,PetscErrorCode (*SNESFunction)(SNES,Vec,Vec,void*),void *ctx)
	//CheckError(errorCode)
}

//export GoSNESJacobianCallback
func GoSNESJacobianCallback(snes C.SNES, x C.Vec, A C.Mat, P C.Mat, /*ms *C.MatStructure,*/ ctx unsafe.Pointer) C.PetscErrorCode {
	s := (*SNES)(ctx)
	if s.snesFn != nil {
		//gms := MatStructure(*ms)
		err := s.snesJac(s, Vec{x}, Mat{A}, Mat{P}, /*&gms,*/ s.snesJacCtx)
		//*ms = C.MatStructure(gms)
		return C.PetscErrorCode(err)
	}
	return 0
}

type SNESJacobian func(*SNES, Vec, Mat, Mat, interface{}) ErrorCode

// SetJacobian - Sets the function to compute Jacobian as well as the
// location to store the matrix.
func (s *SNES) SetJacobian(A, P *Mat, f SNESJacobian, ctx interface{}) {
	s.snesJac = f
	s.snesJacCtx = ctx
	//C.SetSNESJacobian(s.snes_p, A.mat_p, P.mat_p, unsafe.Pointer(s))
	errorCode := C.SetSNESJacobian(s.snes_p, A.mat_p, P.mat_p, unsafe.Pointer(s))
	CheckError(errorCode)

//	errorCode := C.SNESSetJacobian(SNES snes,Mat Amat,Mat Pmat,PetscErrorCode (*SNESJacobianFunction)(SNES,Vec,Mat*,Mat*,MatStructure*,void*),void *ctx)
//	CheckError(errorCode)
}

// KSP returns the KSP context for a SNES solver.
func (s *SNES) KSP() *KSP {
	var ksp KSP
	errorCode := C.SNESGetKSP(s.snes_p, &ksp.ksp_p)
	CheckError(errorCode)
	return &ksp
}

// SetFromOptions sets various SNES and KSP parameters from user options.
func (s *SNES) SetFromOptions() {
	errorCode := C.SNESSetFromOptions(s.snes_p)
	CheckError(errorCode)
}

// Sets the prefix used for searching for all SNES options in the database.
func (s *SNES) SetOptionsPrefix(prefix string) {
	p := C.CString(string(prefix))
	defer C.free(unsafe.Pointer(p))
	errorCode := C.SNESSetOptionsPrefix(s.snes_p, p)
	CheckError(errorCode)
}

// Solve solves a nonlinear system F(x) = b.
func (s *SNES) Solve(b, x *Vec) {
	if b != nil {
		errorCode := C.SNESSolve(s.snes_p, b.vec_p, x.vec_p)
		CheckError(errorCode)
	} else {
		errorCode := C.SNESSolve(s.snes_p, nil, x.vec_p)
		CheckError(errorCode)
	}
}

/* Intermediate */

// Solution returns the vector where the approximate solution is stored.
func (s *SNES) Solution() *Vec {
	var x Vec
	errorCode := C.SNESGetSolution(s.snes_p, &x.vec_p)
	CheckError(errorCode)
	return &x
}

type SNESMonitorFunction func(*SNES, int, float64, interface{}) ErrorCode

// MonitorSet sets an additional function that is to be used at every
// iteration of the nonlinear solver to display the iteration's
// progress.
func (s *SNES) MonitorSet(fn SNESMonitorFunction, mctx interface{}, monitordestroy func(interface{}) ErrorCode) {

}

// IterationNumber gets the number of nonlinear iterations completed
// at this time.
func (s *SNES) IterationNumber() int {
	var iter C.PetscInt
	errorCode := C.SNESGetIterationNumber(s.snes_p, &iter)
	CheckError(errorCode)
	return int(iter)
}

// SetType sets the method for the nonlinear solver.
func (s *SNES) SetType(typ SNESType) {
	t := C.CString(string(typ))
	defer C.free(unsafe.Pointer(t))
	errorCode := C.SNESSetType(s.snes_p, C.SNESType(t))
	CheckError(errorCode)
}

var (
	// ||F|| < atol
	SNESConvergedFNormAbs C.SNESConvergedReason = C.SNES_CONVERGED_FNORM_ABS
	// ||F|| < rtol*||F_initial||
	SNESConvergedFNormRelative C.SNESConvergedReason = C.SNES_CONVERGED_FNORM_RELATIVE
	// Newton computed step size small; || delta x || < stol || x ||
	SNESConvergedSNormRelative C.SNESConvergedReason = C.SNES_CONVERGED_SNORM_RELATIVE
	// Maximum iterations reached.
	SNESConvergedIts C.SNESConvergedReason = C.SNES_CONVERGED_ITS
	SNESConvergedTrDelta C.SNESConvergedReason = C.SNES_CONVERGED_TR_DELTA

	// Diverged
	// The new x location passed the function is not in the domain of F.
	SNESDivergedFunctionDomain C.SNESConvergedReason = C.SNES_DIVERGED_FUNCTION_DOMAIN
	SNESDivergedFunctionCount C.SNESConvergedReason = C.SNES_DIVERGED_FUNCTION_COUNT
	// The linear solve failed.
	SNESDivergedLinearSolve C.SNESConvergedReason = C.SNES_DIVERGED_LINEAR_SOLVE
	SNESDivergedFNormNan C.SNESConvergedReason = C.SNES_DIVERGED_FNORM_NAN
	SNESDivergedMaxIt C.SNESConvergedReason    = C.SNES_DIVERGED_MAX_IT
	// The line search failed.
	SNESDivergedLineSearh C.SNESConvergedReason = C.SNES_DIVERGED_LINE_SEARCH
	// Inner solve failed.
	SNESDivergedInner C.SNESConvergedReason = C.SNES_DIVERGED_INNER
	// || J^T b || is small, implies converged to local minimum of F()
	SNESDivergedLocalMin C.SNESConvergedReason = C.SNES_DIVERGED_LOCAL_MIN
	SNESConvergedIterating C.SNESConvergedReason = C.SNES_CONVERGED_ITERATING
)

// ConvergedReason gets the reason the SNES iteration was stopped.
// Negative value indicates diverged, positive value converged.
func (s *SNES) ConvergedReason() C.SNESConvergedReason {
	var reason C.SNESConvergedReason
	errorCode := C.SNESGetConvergedReason(s.snes_p, &reason)
	CheckError(errorCode)
	return reason
}

// ConvergedReason returns a string representation of a converged reason.
func ConvergedReason(reason C.SNESConvergedReason) string {
	//c := C.SNESConvergedReasons[reason]
	c := C.GetConvergedReason(reason)
	return C.GoString(c)
}

/* Advanced */

// Function returns the vector where the function is stored.
func (s *SNES) Function(r *Vec, f *func(SNES, Vec, Vec, interface{}), ctx *interface{}) {
	//C.SNESGetFunction(SNES snes,Vec *r,PetscErrorCode (**SNESFunction)(SNES,Vec,Vec,void*),void **ctx)
	//CheckError(errorCode)
}

/* Developer */

// SNESComputeFunction calls the function that has been set with SetFunction().
func SNESComputeFunction(snes *SNES, x, y *Vec, ctx interface{}) {
	//s := (*SNES)(snes)
}

// Computes the Jacobian using finite differences.
func SNESComputeJacobianDefault(snes *SNES, x1 Vec, J, B Mat, ctx interface{}) ErrorCode {
	//p := uintptr()
	errorCode := C.SNESComputeJacobianDefault(snes.snes_p, x1.vec_p, J.mat_p, B.mat_p, unsafe.Pointer(&ctx))
	CheckError(errorCode)
	return ErrOK
}
