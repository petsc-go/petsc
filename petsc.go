package petsc

/*
#cgo pkg-config: PETSc
#cgo LDFLAGS: -lpetsc -lmpi

#include "mpi.h"
#include "petsc.h"
#include "petscerror.h"

MPI_Comm get_PETSC_COMM_WORLD() {
	return (MPI_Comm)(PETSC_COMM_WORLD);
}

PetscViewer get_PETSC_VIEWER_STDOUT_WORLD() {
	return PETSC_VIEWER_STDOUT_WORLD;
}

PetscViewer get_PETSC_VIEWER_STDOUT_SELF() {
	return PETSC_VIEWER_STDOUT_SELF;
}

MPI_Comm get_PETSC_COMM_SELF() {
	return (MPI_Comm)(PETSC_COMM_SELF);
}

MPI_Comm get_PETSC_COMM_NULL() {
	return (MPI_Comm)(MPI_COMM_NULL);
}

static char** makeCharArray(int size) {
        return calloc(sizeof(char*), size);
}

static void setArrayString(char **a, char *s, int n) {
        a[n] = s;
}

static void freeCharArray(char **a, int size) {
        int i;
        for (i = 0; i < size; i++) {
                free(a[i]);
        }
        free(a);
}

static void chkErrQ(PetscErrorCode code) {
//	PetscErrorCode ec = CHKERRQ(code);
//	code = ec;
	do {
		if (PetscUnlikely(code)) {
			PetscError(PETSC_COMM_SELF, __LINE__, PETSC_FUNCTION_NAME, __FILE__, code, PETSC_ERROR_REPEAT, " ");
		}
	} while (0);
}

static PetscErrorCode setErrQ(MPI_Comm comm, PetscErrorCode errorcode, char *message) {
	return PetscError(comm, __LINE__, PETSC_FUNCTION_NAME, __FILE__, errorcode, PETSC_ERROR_INITIAL, message);
}

static PetscErrorCode setErrQ2(MPI_Comm comm, PetscErrorCode errorcode, char *message) {
	return PetscError(comm, __LINE__, PETSC_FUNCTION_NAME, __FILE__, errorcode, PETSC_ERROR_INITIAL, message, NULL, NULL);
}

static PetscErrorCode petscPrintf(MPI_Comm comm, char *s) {
	return PetscPrintf(comm, s);
}

static PetscErrorCode petscSynchronizedPrintf(MPI_Comm comm, char *s) {
	return PetscSynchronizedPrintf(comm, s);
}
*/
import "C"

import (
	"fmt"
	"unsafe"
)

func SetErrQ(comm MPI_Comm, code ErrorCode, message string) {
	m := C.CString(string(message))
	defer C.free(unsafe.Pointer(m))
	errorCode := C.setErrQ(C.MPI_Comm(comm), C.PetscErrorCode(code/*.code*/), m)
	CheckError(errorCode)
}

// SetErrQ2 is called when an error has been detected.
func SetErrQ2(comm MPI_Comm, code ErrorCode, message string, a ...interface{}) {
	m := fmt.Sprintf(message, a...)
	c := C.CString(m)
	defer C.free(unsafe.Pointer(c))
	errorCode := C.setErrQ2(C.MPI_Comm(comm), C.PetscErrorCode(code), c)
	CheckError(errorCode)
}

var CheckError = func(petscCode C.PetscErrorCode) {
	C.chkErrQ(petscCode)
	if petscCode > C.PETSC_ERR_MIN_VALUE {
		fmt.Printf("code: %s", petscCode)
//		panic(NewErrorCode(petscCode))
	}
}

//type ErrorCode struct {
//	code int
//}
type ErrorCode C.PetscErrorCode

//func newErrorCode(code int) ErrorCode {
//	return ErrorCode{code}
//}

//func NewErrorCode(petscCode C.PetscErrorCode) ErrorCode {
	//if petscCode == C.int(0) {
	//	return nil
	//}
//	return &ErrorCode{} //int(petscCode)}
//	return ErrorCode(petscCode)
//}

/*func (e ErrorCode) Error() string {
	s := errText[e.code]
	if s == "" {
		return fmt.Sprintf("error code %d", e.code)
	}
	return s
}*/

//var ErrOK = newErrorCode(0)
var ErrOK = ErrorCode(C.PetscErrorCode(0))

var (
	ErrorUser    = ErrorCode(C.PetscErrorCode(83)) // user has not provided needed function
	ErrorSupport = ErrorCode(C.PetscErrorCode(56)) // no support for requested operation
	ErrorArgWrong = ErrorCode(C.PetscErrorCode(62)) // wrong argument (but object probably ok)
)

/*
var (
	ErrMinValue ErrorCode = newErrorCode(54) // should always be one less then the smallest value
	ErrOK = ErrMinValue

	ErrorMemory            ErrorCode = newErrorCode(55) // unable to allocate requested memory
	ErrorSupport           ErrorCode = newErrorCode(56) // no support for requested operation
	ErrorSupportSystem     ErrorCode = newErrorCode(57) // no support for requested operation on this computer system
	ErrorOrder             ErrorCode = newErrorCode(58) // operation done in wrong order
	ErrorSignal            ErrorCode = newErrorCode(59) // signal received
	ErrorFloatingPoint     ErrorCode = newErrorCode(72) // floating point exception
	ErrorCorruptederror    ErrorCode = newErrorCode(74) // corrupted PETSc object
	ErrorLibrary           ErrorCode = newErrorCode(76) // error in library called by PETSc
	ErrorPETScLibrary      ErrorCode = newErrorCode(77) // PETSc library generated inconsistent data
	ErrorMemoryCorruption  ErrorCode = newErrorCode(78) // memory corruption
	ErrorConvergenceFailed ErrorCode = newErrorCode(82) // iterative method (KSP or SNES) failed
	ErrorUser              ErrorCode = newErrorCode(83) // user has not provided needed function
	ErrorSystem            ErrorCode = newErrorCode(88) // error in system call
	ErrorPointer           ErrorCode = newErrorCode(70) // pointer does not point to valid address

	ErrorArgSize         ErrorCode = newErrorCode(60) // nonconforming object sizes used in operation
	ErrorArgIdentity     ErrorCode = newErrorCode(61) // two arguments not allowed to be the same
	ErrorArgWrong        ErrorCode = newErrorCode(62) // wrong argument (but object probably ok)
	ErrorArgCorrupt      ErrorCode = newErrorCode(64) // null or corrupted PETSc object as argument
	ErrorArgOutOfRange   ErrorCode = newErrorCode(63) // input argument, out of range
	ErrorArgBadPointer   ErrorCode = newErrorCode(68) // invalid pointer argument
	ErrorArgNotSameType  ErrorCode = newErrorCode(69) // two args must be same object type
	ErrorArgNotSameComm  ErrorCode = newErrorCode(80) // two args must be same communicators
	ErrorArgWrongState   ErrorCode = newErrorCode(73) // object in argument is in wrong state, e.g. unassembled mat
	ErrorArgTypenotSet   ErrorCode = newErrorCode(89) // the type of the object has not yet been set
	ErrorArgIncompatible ErrorCode = newErrorCode(75) // two arguments are incompatible
	ErrorArgNull         ErrorCode = newErrorCode(85) // argument is null that should not be
	ErrorArgUnknownType  ErrorCode = newErrorCode(86) // type name doesn't match any registered type

	ErrorFileOpen       ErrorCode = newErrorCode(65) // unable to open file
	ErrorFileRead       ErrorCode = newErrorCode(66) // unable to read from file
	ErrorFileWrite      ErrorCode = newErrorCode(67) // unable to write to file
	ErrorFileUnexpected ErrorCode = newErrorCode(79) // unexpected data in file

	ErrorMatLUZeroPivot   ErrorCode = newErrorCode(71) // detected a zero pivot during LU factorization
	ErrorMatCholZeroPivot ErrorCode = newErrorCode(81) // detected a zero pivot during Cholesky factorization

	ErrorIntOverflow ErrorCode = newErrorCode(84) // should always be one less then the smallest value

	ErrorFlopCount    ErrorCode = newErrorCode(90)
	ErrorNotConverged ErrorCode = newErrorCode(91) // solver did not converge
	ErrorMaxValue     ErrorCode = newErrorCode(92) // this is always the one more than the largest error code
)
*/
var errText = map[int]string{
	55: "Out of memory",
	56: "No support for this operation for this object type",
	57: "No support for this operation on this system",
	58: "Operation done in wrong order",
	59: "Signal received",
	60: "Nonconforming object sizes",
	61: "Argument aliasing not permitted",
	62: "Invalid argument",
	63: "Argument out of range",
	64: "Corrupt argument:\nsee http://www.mcs.anl.gov/petsc/documentation/faq.html#valgrind",
	65: "Unable to open file",
	66: "Read from file failed",
	67: "Write to file failed",
	68: "Invalid pointer",
	69: "Arguments must have same type",
	70: "Attempt to use a pointer that does not point to a valid accessible location",
	71: "Detected zero pivot in LU factorization:\nsee http://www.mcs.anl.gov/petsc/documentation/faq.html#ZeroPivot",
	72: "Floating point exception",
	73: "Object is in wrong state",
	74: "Corrupted Petsc object",
	75: "Arguments are incompatible",
	76: "Error in external library",
	77: "Petsc has generated inconsistent data",
	78: "Memory corruption",
	79: "Unexpected data in file",
	80: "Arguments must have same communicators",
	81: "Detected zero pivot in Cholesky factorization:\nsee http://www.mcs.anl.gov/petsc/documentation/faq.html#ZeroPivot",
	84: "Overflow in integer operation:\nsee http://www.mcs.anl.gov/petsc/documentation/faq.html#64-bit-indices",
	85: "Null argument, when expecting valid pointer",
	86: "Unknown type. Check for miss-spelling or missing external package needed for type:\nsee http://www.mcs.anl.gov/petsc/documentation/installation.html#external",
	87: "Not used",
	88: "Error in system call",
	89: "Object Type not set:\nsee http://www.mcs.anl.gov/petsc/documentation/faq.html#objecttypenotset",
}

var Decide = int(C.PETSC_DECIDE)

type MPI_Comm C.MPI_Comm

var (
	CommSelf MPI_Comm
	CommWorld MPI_Comm
	CommNull MPI_Comm
)

func initVars() {
	CommSelf = MPI_Comm(C.get_PETSC_COMM_SELF())
	CommWorld = MPI_Comm(C.get_PETSC_COMM_WORLD())
	CommNull = MPI_Comm(C.get_PETSC_COMM_NULL())

	//CommSelf = MPIComm(C.PETSC_COMM_SELF)
	//CommWorld = MPIComm(C.PETSC_COMM_WORLD)
	//ViewerStdOutWorld = Viewer{C.PETSC_VIEWER_STDOUT_(C.MPI_COMM_WORLD)}
	//ViewerStdOutSelf = Viewer{C.PETSC_VIEWER_STDOUT_(C.MPI_COMM_SELF)}

	ViewerStdOutWorld = &Viewer{C.get_PETSC_VIEWER_STDOUT_WORLD()}
	ViewerStdOutSelf  = &Viewer{C.get_PETSC_VIEWER_STDOUT_SELF()}
}

func Initialize(args []string, file, help string) {
	cargc := C.int(len(args))
	cargs := C.makeCharArray(cargc)
	defer C.freeCharArray(cargs, cargc)
	for i, s := range args {
		C.setArrayString(cargs, C.CString(s), C.int(i))
	}

	/*cargc := C.int(len(args))
	ccmds := make([]*C.char, len(args))
	for i, a := range args {
		cstr := C.CString(a)
		defer C.free(unsafe.Pointer(cstr))
		ccmds[i] = cstr
	}
	var cargs *C.char = nil
	if len(args) > 0 {
		cargs = ccmds[0]
	}*/

	errorCode := C.PetscInitialize(&cargc, &cargs, nil /*C.CString(file)*/, C.CString(help))
	CheckError(errorCode)

	initVars()
}

func Finalize() {
	errorCode := C.PetscFinalize()
	CheckError(errorCode)
}

var (
	ViewerStdOutWorld *Viewer
	ViewerStdOutSelf  *Viewer
)

// Printf prints to standard out, only from the first
// processor in the communicator.
func Printf(comm MPI_Comm, format string, a ...interface{}) {
	s := fmt.Sprintf(format, a...)
	c := C.CString(s)
	defer C.free(unsafe.Pointer(c))
	errorCode := C.petscPrintf(C.MPI_Comm(comm), c)
	CheckError(errorCode)
}

// SynchronizedPrintf prints synchronized output from several processors.
// Output of the first processor is followed by that of the second, etc.
func SynchronizedPrintf(comm MPI_Comm, format string, a ...interface{}) {
	s := fmt.Sprintf(format, a...)
	c := C.CString(s)
	defer C.free(unsafe.Pointer(c))
	errorCode := C.petscSynchronizedPrintf(C.MPI_Comm(comm), c)
	CheckError(errorCode)
}

// SynchronizedFlush flushes to the screen output from all processors
// involved in previous SynchronizedPrintf() calls.
func SynchronizedFlush(comm MPI_Comm) {
	errorCode := C.PetscSynchronizedFlush(C.MPI_Comm(comm), nil)
	CheckError(errorCode)
}

// OptionsGetInt gets the integer value for a particular option in the
// database.
//
// Takes the string to prepend to the name, the option being sought and
// returns the value and true if found.
func OptionsGetInt(pre, name string) (int, bool) {
	p := C.CString(pre)
	defer C.free(unsafe.Pointer(p))
	n := C.CString(name)
	defer C.free(unsafe.Pointer(n))
	var ival C.PetscInt
	var set C.PetscBool
	errorCode := C.PetscOptionsGetInt(p, n, &ival, &set)
	CheckError(errorCode)
	return int(ival), petscBool(set)
}

// OptionsGetScalar gets the scalar value for a particular option in the
// database.
func OptionsGetScalar(pre, name string) (float64, bool) {
	p := C.CString(pre)
	defer C.free(unsafe.Pointer(p))
	n := C.CString(name)
	defer C.free(unsafe.Pointer(n))
	var dval C.PetscScalar
	var set C.PetscBool
	errorCode := C.PetscOptionsGetScalar(p, n, &dval, &set)
	CheckError(errorCode)
	return float64(dval), petscBool(set)
}

// OptionsGetReal gets the real value for a particular option in the
// database.
func OptionsGetReal(pre, name string) (float64, bool) {
	p := C.CString(pre)
	defer C.free(unsafe.Pointer(p))
	n := C.CString(name)
	defer C.free(unsafe.Pointer(n))
	var dval C.PetscReal
	var set C.PetscBool
	errorCode := C.PetscOptionsGetReal(p, n, &dval, &set)
	CheckError(errorCode)
	return float64(dval), petscBool(set)
}

// OptionsGetEnum gets the enum value for a particular option in the database.
/*func OptionsGetEnum(pre, name string, list []string) (C.PetscEnum, bool) {
	p := C.CString(pre)
	defer C.free(unsafe.Pointer(p))
	n := C.CString(name)
	defer C.free(unsafe.Pointer(n))
	var value C.PetscEnum
	var set C.PetscBool
	errorCode := C.PetscOptionsGetEnum(p, n, const char * const *list, &value, &set)
	CheckError(errorCode)
	return value, petscBool(set)
}*/

// OptionsGetBool gets the Logical (true or false) value for a particular
// option in the database.
func OptionsGetBool(pre, name string) (bool, bool) {
	p := C.CString(pre)
	defer C.free(unsafe.Pointer(p))
	n := C.CString(name)
	defer C.free(unsafe.Pointer(n))
	var value C.PetscBool
	var set C.PetscBool
	errorCode := C.PetscOptionsGetBool(p, n, &value, &set)
	CheckError(errorCode)
	return petscBool(value), petscBool(set)
}

// OptionsHasName Determines whether a certain option is given in the
// database.
func OptionsHasName(pre, name string) bool {
	p := C.CString(pre)
	defer C.free(unsafe.Pointer(p))
	n := C.CString(name)
	defer C.free(unsafe.Pointer(n))
	var set C.PetscBool
	errorCode := C.PetscOptionsHasName(p, n, &set)
	CheckError(errorCode)
	return petscBool(set)
}

// Determines how an array passed to certain functions is copied or retained.
type CopyMode C.PetscCopyMode

var (
	// The array values are copied into new space, the user is free to
	// reuse or delete the passed in array.
	CopyValues = CopyMode(C.PETSC_COPY_VALUES)

	// The array values are not copied, the object takes ownership of the
	// array and will free it later, the user cannot change or delete the
	// array.
	OwnPointer = CopyMode(C.PETSC_OWN_POINTER)

	// The array values are not copied, the object uses the array but does
	// not take ownership of the array. The user cannot use the array but
	// the user must delete the array after the object is destroyed.
	UsePointer = CopyMode(C.PETSC_USE_POINTER)
)

var (
	NotSetValues C.InsertMode = C.NOT_SET_VALUES

	// Put a value into a vector or matrix, overwrites any previous value.
	InsertValues C.InsertMode = C.INSERT_VALUES

	// Adds a value into a vector or matrix, if there previously was no
	// value, just puts the value into that location.
	AddValues C.InsertMode = C.ADD_VALUES

	// Puts the maximum of the scattered/gathered value and the current
	// value into each location.
	MaxValues C.InsertMode = C.MAX_VALUES

	InsertAllValues C.InsertMode = C.INSERT_ALL_VALUES
	AddAllValues C.InsertMode = C.ADD_ALL_VALUES
	InsertBCValues C.InsertMode = C.INSERT_BC_VALUES
	AddBCValues C.InsertMode = C.ADD_BC_VALUES
)

//var (
//	ViewerDefault C.PetscViewerFormat = C.PETSC_VIEWER_DEFAULT
//	ViewerASCIIMatlab C.PetscViewerFormat = C.PETSC_VIEWER_ASCII_MATLAB
//	ViewerASCIIMathematica C.PetscViewerFormat = C.PETSC_VIEWER_ASCII_MATHEMATICA
//	ViewerASCIIImpl C.PetscViewerFormat = C.PETSC_VIEWER_ASCII_IMPL
//	ViewerASCIIInfo C.PetscViewerFormat = C.PETSC_VIEWER_ASCII_INFO
//	ViewerASCIIInfoDetail C.PetscViewerFormat = C.PETSC_VIEWER_ASCII_INFO_DETAIL
//	ViewerASCIICommon C.PetscViewerFormat = C.PETSC_VIEWER_ASCII_COMMON
//	ViewerASCIISymmodu C.PetscViewerFormat = C.PETSC_VIEWER_ASCII_SYMMODU
//	ViewerASCIIIndex C.PetscViewerFormat = C.PETSC_VIEWER_ASCII_INDEX
//	ViewerAsciiDense C.PetscViewerFormat = C.PETSC_VIEWER_ASCII_DENSE
//	ViewerAsciiMatrixMarket C.PetscViewerFormat = C.PETSC_VIEWER_ASCII_MATRIXMARKET
//	ViewerASCII_VTK C.PetscViewerFormat = C.PETSC_VIEWER_ASCII_VTK
//	ViewerASCII_VTKCell C.PetscViewerFormat = C.PETSC_VIEWER_ASCII_VTK_CELL
//	ViewerASCII_VTKCoords C.PetscViewerFormat = C.PETSC_VIEWER_ASCII_VTK_COORDS
//	ViewerASCII_PCICE C.PetscViewerFormat = C.PETSC_VIEWER_ASCII_PCICE
//	ViewerASCIIPython C.PetscViewerFormat = C.PETSC_VIEWER_ASCII_PYTHON
//	ViewerASCIIFactorInfo C.PetscViewerFormat = C.PETSC_VIEWER_ASCII_FACTOR_INFO
//	ViewerASCIILatex C.PetscViewerFormat = C.PETSC_VIEWER_ASCII_LATEX
//	ViewerDrawBasic C.PetscViewerFormat = C.PETSC_VIEWER_DRAW_BASIC
//	ViewerDrawLG C.PetscViewerFormat = C.PETSC_VIEWER_DRAW_LG
//	ViewerDrawContour C.PetscViewerFormat = C.PETSC_VIEWER_DRAW_CONTOUR
//	ViewerDrawPorts C.PetscViewerFormat = C.PETSC_VIEWER_DRAW_PORTS
//	ViewerVTK_VTS C.PetscViewerFormat = C.PETSC_VIEWER_VTK_VTS
//	ViewerVTK_VTU C.PetscViewerFormat = C.PETSC_VIEWER_VTK_VTU
//	ViewerBinaryMatlab C.PetscViewerFormat = C.PETSC_VIEWER_BINARY_MATLAB
//	ViewerNative C.PetscViewerFormat = C.PETSC_VIEWER_NATIVE
//	ViewerNoFormat C.PetscViewerFormat = C.PETSC_VIEWER_NOFORMAT
//)

// Any PETSc object, PetscViewer, Mat, Vec, KSP etc
type Object struct {
	obj_p *C.PetscObject
}

// SetName sets a string name associated with a PETSc object.
/*func SetName(obj *Object, name string) {
	n := C.CString(name)
	defer C.free(unsafe.Pointer(n))
	errorCode := C.PetscObjectSetName(*(obj.obj_p), n)
	CheckError(errorCode)
}*/

// Adds floating point operations to the global counter.
func LogFlops(f float64) {
	ff := C.PetscLogDouble(f)
	errorCode := C.PetscLogFlops(ff)
	CheckError(errorCode)
}
