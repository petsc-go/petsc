package petsc

/*
#cgo pkg-config: PETSc
#cgo LDFLAGS: -lpetsc -lmpi

#include "mpi.h"
#include "petscvec.h"
*/
import "C"

import (
	"runtime"
	"unsafe"
	"reflect"
)

// PETSc vector type.
type Vec struct {
	vec_p C.Vec
}

// NewVecComm creates an empty vector object. The type can then be set with
// SetType(), or SetFromOptions().
//
// If you never call SetType() or SetFromOptions() it will generate an
// error when you try to use the vector.
func NewVec(comm MPI_Comm) *Vec {
	v := &Vec{}
	runtime.SetFinalizer(v, destroyVec)
	errorCode := C.VecCreate(C.MPI_Comm(comm), &v.vec_p)
	CheckError(errorCode)
	return v
}

// NewVecSeq creates a standard, sequential array-style vector.
func NewVecSeq(comm MPI_Comm, n int) *Vec {
	v := &Vec{}
	runtime.SetFinalizer(v, destroyVec)
	errorCode := C.VecCreateSeq(C.MPI_Comm(comm), C.PetscInt(n), &v.vec_p)
	CheckError(errorCode)
	return v
}

// String with the name of a PETSc vector type.
type VecType string

const (
	VecSeq        VecType = "seq"
	VecMpi                = "mpi"
	VecStandard           = "standard" /* seq on one process and mpi on several */
	VecShared             = "shared"
	VecSieve              = "sieve"
	VecSeqCusp            = "seqcusp"
	VecMpiCusp            = "mpicusp"
	VecCusp               = "cusp" /* seqcusp on one process and mpicusp on several */
	VecNest               = "nest"
	VecSeqPThread         = "seqpthread"
	VecMpiPThread         = "mpipthread"
	VecPThread            = "pthread" /* seqpthread on one process and mpipthread on several */
)

var (
	Norm1         C.NormType = C.NORM_1         // The one norm, ||v|| = sum_i | v_i |. ||A|| = max_j || v_*j ||, maximum column sum.
	Norm2         C.NormType = C.NORM_2         // The two norm, ||v|| = sqrt(sum_i (v_i)^2) (vectors only).
	NormFrobenius C.NormType = C.NORM_FROBENIUS // ||A|| = sqrt(sum_ij (A_ij)^2), same as Norm2 for vectors.
	NormInfinity  C.NormType = C.NORM_INFINITY  // ||v|| = max_i |v_i|. ||A|| = max_i || v_i* ||, maximum row sum.
	Norm1And2     C.NormType = C.NORM_1_AND_2   // Computes both the 1 and 2 norm of a vector.

	NormMax C.NormType = NormInfinity
)

func destroyVec(v *Vec) C.PetscErrorCode {
	return C.VecDestroy(&v.vec_p)
}

// Destroy destroys a vector.
func (v *Vec) Destroy() {
	errorCode := destroyVec(v)
	CheckError(errorCode)
}

// AssemblyBegin begins assembling the vector. This routine should
// be called after completing all calls to SetValues().
func (v *Vec) AssemblyBegin() {
	errorCode := C.VecAssemblyBegin(v.vec_p)
	CheckError(errorCode)
}

// AssemblyEnd completes assembling the vector. This routine should
// be called after AssemblyBegin().
func (v *Vec) AssemblyEnd() {
	errorCode := C.VecAssemblyEnd(v.vec_p)
	CheckError(errorCode)
}

// Copy - Copies a vector. y <- x
func (x *Vec) Copy(y *Vec) {
	errorCode := C.VecCopy(x.vec_p, y.vec_p)
	CheckError(errorCode)
}

// This increases the reference count for the object by one.
func (x *Vec) Reference() {
	var obj C.PetscObject//TODO
	errorCode := C.PetscObjectReference(obj)
	CheckError(errorCode)
}

// Duplicate creates a new vector of the same type as an existing vector.
//
// Duplicate does not copy the vector entries, but rather allocates storage
// for the new vector. Use Copy() to copy a vector.
func (v *Vec) Duplicate() *Vec {
	dup := &Vec{}
	runtime.SetFinalizer(dup, destroyVec)
	errorCode := C.VecDuplicate(v.vec_p, &dup.vec_p)
	CheckError(errorCode)
	return dup
}

func (v *Vec) SetName(name string) {
	/*p := uintptr(unsafe.Pointer(&v.vec_p))
	obj := (C.PetscObject)(unsafe.Pointer(p))
	//obj := v.vec_p.(C.PetscObject)
	m := C.CString(name)
	defer C.free(unsafe.Pointer(m))
	errorCode := C.PetscObjectSetName(obj, m)
	CheckError(errorCode)*/
}

// Max determines the maximum vector component and its location.
func (v *Vec) Max() (int, float64) {
	var p C.PetscInt
	var val C.PetscReal
	errorCode := C.VecMax(v.vec_p, &p, &val)
	CheckError(errorCode)
	return int(p), float64(val)
}

// Min determines the minimum vector component and its location.
func (v *Vec) Min() (int, float64) {
	var p C.PetscInt
	var val C.PetscReal
	errorCode := C.VecMin(v.vec_p, &p, &val)
	CheckError(errorCode)
	return int(p), float64(val)
}

// Size returns the global number of elements of the vector.
func (v *Vec) Size() int {
	var size C.PetscInt
	errorCode := C.VecGetSize(v.vec_p, &size)
	CheckError(errorCode)
	return int(size)
}

// LocalSize returns the number of elements of the vector stored
// in local memory.
func (v *Vec) LocalSize() int {
	var l C.PetscInt
	errorCode := C.VecGetLocalSize(v.vec_p, &l)
	CheckError(errorCode)
	return int(l)
}

// Dot computes the vector dot product.
func (v *Vec) Dot(y *Vec) float64 {
	var val C.PetscScalar
	errorCode := C.VecDot(v.vec_p, y.vec_p, &val)
	CheckError(errorCode)
	return float64(val)
}

// Norm computes the vector norm.
func (v *Vec) Norm(typ C.NormType) float64 {
	var val C.PetscReal
	errorCode := C.VecNorm(v.vec_p, typ, &val)
	CheckError(errorCode)
	return float64(val)
}

// SubVector gets a vector representing part of another vector.
//
// The subvector Y should be returned with VecRestoreSubVector().
//
// This function may return a subvector without making a copy, therefore it
// is not safe to use the original vector while modifying the subvector.
// Other non-overlapping subvectors can still be obtained from X using this
// function.
func (v *Vec) SubVector(is *IS) *Vec {
	sub := &Vec{}
	runtime.SetFinalizer(sub, destroyVec)
	errorCode := C.VecGetSubVector(v.vec_p, is.is_p, &sub.vec_p)
	CheckError(errorCode)
	return sub
}

// RestoreSubVector restores a subvector extracted using SubVector().
func (v *Vec) RestoreSubVector(is *IS, sub *Vec) {
	errorCode := C.VecRestoreSubVector(v.vec_p, is.is_p, &sub.vec_p)
	CheckError(errorCode)
}

// Array returns an array that contains this processor's portion of the
// vector data. RestoreArray() must be called when access to the array
// is no longer needed.
func (v *Vec) Array() []float64 {
	rstart, rend := v.OwnershipRange()
	sz := rend-rstart

	var ca *C.PetscScalar
	errorCode := C.VecGetArray(v.vec_p, &ca)
	CheckError(errorCode)

	hdr := reflect.SliceHeader{
		Data: uintptr(unsafe.Pointer(ca)),
		Len:  sz,
		Cap:  sz,
	}
	//ga := *(*[]C.PetscScalar)(unsafe.Pointer(&hdr))
	ga := *(*[]float64)(unsafe.Pointer(&hdr))

	return ga
}

// RestoreArray restores a vector after Array() has been called.
func (v *Vec) RestoreArray(a []float64) {

	//h := *(*reflect.SliceHeader)(unsafe.Pointer(v.Addr()))

	//var p *C.PetscScalar = &a[0]
	p := (*C.PetscScalar)(unsafe.Pointer(&a[0]))
	errorCode := C.VecRestoreArray(v.vec_p, &p)
	CheckError(errorCode)
}

// PlaceArray allows one to replace the array in a vector.
func (v *Vec) PlaceArray(a []float64) {
	aa := scalarSlice(a)
	errorCode := C.VecPlaceArray(v.vec_p, &aa[0])
	CheckError(errorCode)
}

// ResetArray resets the vector to use its default memory.
func (v *Vec) ResetArray() {
	errorCode := C.VecResetArray(v.vec_p)
	CheckError(errorCode)
}

// SetValues inserts or adds values into certain locations of a vector.
//
// These values may be cached, so AssemblyBegin() and AssemblyEnd()
// must be called after all calls to SetValues() have been completed.
func (v *Vec) SetValues(ix []int, y []float64, iora C.InsertMode) {
	ixx := intSlice(ix)
	yy := scalarSlice(y)
	errorCode := C.VecSetValues(v.vec_p, C.PetscInt(len(ix)), &ixx[0], &yy[0], iora)
	CheckError(errorCode)
}

// GetValues gets values from certain locations of a vector.
//
// AssemblyBegin() and AssemblyEnd() must be called before calling this.
/*func (v *Vec) Values(ni int, ix []int) []float64 {
	var ixx *C.PetscInt
	C.PetscMalloc(ni * C.sizeof(C.PetscInt), &ixx)
	defer C.PetscFree(ixx)

	for i, x := range ix {
		ixx[i] = C.PetscInt(x)
	}

	var y *C.PetscScalar
	C.PetscMalloc(ni * C.sizeof(C.PetscScalar), &y)

	var yy [len(y)]C.PetscScalar
	var err C.PetscErrorCode = C.VecGetValues(x.vec_p, C.PetscInt(ni), ixx, y)
	CheckError(errorCode)
	for i, z := range yy {
		y[i] = float64(z)
	}
	return y
}*/

// SetValue sets a single entry value into a vector at row location. Either
// an InsertValues or AddValues mode may be specified.
//
// For efficiency one should use SetValues() and set several or
// many values simultaneously if possible.
//
// These values may be cached, so AssemblyBegin() and AssemblyEnd()
// must be called after all calls to SetValues() have been completed.
func (v *Vec) SetValue(i int, va float64, mode C.InsertMode) {
	errorCode := C.VecSetValue(v.vec_p, C.PetscInt(i), C.PetscScalar(va), mode)
	CheckError(errorCode)
}

// Gets a value from certain location of a vector. Currently can only get
// values on the same processor.
func (v *Vec) GetValue(i int) float64 {
	ix := C.PetscInt(i)
	var y C.PetscScalar
	errorCode := C.VecGetValues(v.vec_p, 1, &ix, &y)
	CheckError(errorCode)
	return float64(y)
}

// Gets values from certain locations of a vector. Currently can only get
// values on the same processor.
func (v *Vec) GetValues(ix []int) []float64 {
	if ix == nil {
		ix = make([]int, v.Size())
		for i := 0; i < v.Size(); i++ {
			ix[i] = i
		}
	}
	n := len(ix)
	ixx := intSlice(ix)
	y := make([]C.PetscScalar, n)
	errorCode := C.VecGetValues(v.vec_p, C.PetscInt(n), &ixx[0], &y[0])
	CheckError(errorCode)
	return scalarArray(&y[0], C.PetscInt(n))
}

// SetFromOptions configures the vector from the PETSc options database.
func (v *Vec) SetFromOptions() {
	errorCode := C.VecSetFromOptions(v.vec_p)
	CheckError(errorCode)
}

// Set sets all components of a vector to a single scalar value.
func (v *Vec) Set(alpha float64) {
	errorCode := C.VecSet(v.vec_p, C.PetscScalar(alpha))
	CheckError(errorCode)
}

// View views a vector object.
func (v *Vec) View(viewer *Viewer) {
	errorCode := C.VecView(v.vec_p, viewer.viewer_p)
	CheckError(errorCode)
}

/* Intermediate */

// SetSizes sets the local and global sizes, and checks to determine
// compatibility.
//
// The local size n may be petsc.Decide to have it calculated if N is given.
// The global size N may be petsc.Decide to have it calculated if n
// is given.
func (v *Vec) SetSizes(n, N int) {
	errorCode := C.VecSetSizes(v.vec_p, C.PetscInt(n), C.PetscInt(N))
	CheckError(errorCode)
}

// SetType builds a vector, for a particular vector implementation.
func (v *Vec) SetType(method VecType) {
	m := C.CString(string(method))
	defer C.free(unsafe.Pointer(m))
	errorCode := C.VecSetType(v.vec_p, m)
	CheckError(errorCode)
}

// Sets all components of the vector to random numbers.
func (v *Vec) SetRandom() {
	errorCode := C.VecSetRandom(v.vec_p, nil)
	CheckError(errorCode)
}

//func (v *Vec) GetType() {
//}

// OwnershipRange returns the range of indices owned by this processor.
func (v *Vec) OwnershipRange() (int, int) {
	var low C.PetscInt
	var high C.PetscInt
	errorCode := C.VecGetOwnershipRange(v.vec_p, &low, &high)
	CheckError(errorCode)
	return int(low), int(high)
}

// AXPY computes y = alpha x + y.
//
// Note that x and y must be different vectors
func (v *Vec) AXPY(alpha float64, x *Vec) {
	errorCode := C.VecAXPY(v.vec_p, C.PetscScalar(alpha), x.vec_p)
	CheckError(errorCode)
}

// AXPBY computes y = alpha x + beta y.
func (v *Vec) AXPBY(alpha, beta float64, x *Vec) {
	errorCode := C.VecAXPBY(v.vec_p, C.PetscScalar(alpha), C.PetscScalar(beta), x.vec_p)
	CheckError(errorCode)
}

// AYPX computes y = x + alpha y.
//
// Note that x and y MUST be different vectors.
func (v *Vec) AYPX(alpha float64, x *Vec) {
	errorCode := C.VecAYPX(v.vec_p, C.PetscScalar(alpha), x.vec_p)
	CheckError(errorCode)
}

// WAXPY computes w = alpha x + y.
//
// Note that w cannot be either x or y, but x and y can be the same.
func (v *Vec) WAXPY(alpha float64, x, y *Vec) {
	errorCode := C.VecWAXPY(v.vec_p, C.PetscScalar(alpha), x.vec_p, y.vec_p)
	CheckError(errorCode)
}

// AXPBYPCZ computes z = alpha x + beta y + gamma z.
//
// Note that x, y and z must be different vectors.
func (v *Vec) AXPBYPCZ(alpha, beta, gamma float64, x, y *Vec) {
	errorCode := C.VecAXPBYPCZ(v.vec_p, C.PetscScalar(alpha), C.PetscScalar(beta), C.PetscScalar(gamma), x.vec_p, y.vec_p)
	CheckError(errorCode)
}

// PointwiseMax computes the componentwise maximum w_i = max(x_i, y_i).
func (v *Vec) PointwiseMax(x, y *Vec) {
	errorCode := C.VecPointwiseMax(v.vec_p, x.vec_p, y.vec_p)
	CheckError(errorCode)
}

// PointwiseMaxAbs computes the componentwise maximum of the absolute values
// w_i = max(abs(x_i), abs(y_i)).
func (v *Vec) PointwiseMaxAbs(x, y *Vec) {
	errorCode := C.VecPointwiseMaxAbs(v.vec_p, x.vec_p, y.vec_p)
	CheckError(errorCode)
}

// PointwiseMin computes the componentwise minimum w_i = min(x_i, y_i).
func (v *Vec) PointwiseMin(x, y *Vec) {
	errorCode := C.VecPointwiseMin(v.vec_p, x.vec_p, y.vec_p)
	CheckError(errorCode)
}

// PointwiseMult computes the componentwise multiplication w = x*y.
func (v *Vec) PointwiseMult(x, y *Vec) {
	errorCode := C.VecPointwiseMult(v.vec_p, x.vec_p, y.vec_p)
	CheckError(errorCode)
}

// PointwiseDivide computes the componentwise division w = x/y.
func (v *Vec) PointwiseDivide(x, y *Vec) {
	errorCode := C.VecPointwiseDivide(v.vec_p, x.vec_p, y.vec_p)
	CheckError(errorCode)
}

// MaxPointwiseDivide computes the maximum of the componentwise division
// max = max_i abs(x_i/y_i).
//
// Notes that x and y may be the same vector. If a particular y_i is zero,
// it is treated as 1 in the above formula
func (v *Vec) MaxPointwiseDivide(y *Vec) float64 {
	var max C.PetscReal
	errorCode := C.VecMaxPointwiseDivide(v.vec_p, y.vec_p, &max)
	CheckError(errorCode)
	return float64(max)
}

// Scales a vector.
func (v *Vec) Scale(alpha float64) {
	errorCode := C.VecScale(v.vec_p, C.PetscScalar(alpha))
	CheckError(errorCode)
}

// Shift shifts all of the components of a vector by computing
// x[i] = x[i] + shift.
func (v *Vec) Shift(shift float64) {
	errorCode := C.VecShift(v.vec_p, C.PetscScalar(shift))
	CheckError(errorCode)
}

// Reciprocal replaces each component of a vector by its reciprocal.
func (v *Vec) Reciprocal() {
	errorCode := C.VecReciprocal(v.vec_p)
	CheckError(errorCode)
}

// Permute permutes a vector in place using the given ordering.
func (v *Vec) Permute(row *IS, inv bool) {
	errorCode := C.VecPermute(v.vec_p, row.is_p, boolPetsc(inv))
	CheckError(errorCode)
}

// SqrtAbs replaces each component of a vector by the square root of
// its magnitude.
func (v *Vec) SqrtAbs() {
	errorCode := C.VecSqrtAbs(v.vec_p)
	CheckError(errorCode)
}

// Log replaces each component of a vector by log(x_i), the natural logarithm.
func (v *Vec) Log() {
	errorCode := C.VecLog(v.vec_p)
	CheckError(errorCode)
}

// Exp replaces each component of a vector by e^x_i.
func (v *Vec) Exp() {
	errorCode := C.VecExp(v.vec_p)
	CheckError(errorCode)
}

// Abs replaces every element in a vector with its absolute value.
func (v *Vec) Abs() {
	errorCode := C.VecAbs(v.vec_p)
	CheckError(errorCode)
}
