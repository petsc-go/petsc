package petsc

/*
#cgo pkg-config: PETSc
#cgo LDFLAGS: -lpetsc -lmpi

#include "mpi.h"
#include "petscviewer.h"
*/
import "C"
import (
	"unsafe"
	"fmt"
)

// PETSc type that helps view (in ASCII, binary, graphically etc)
// other PETSc types.
type Viewer struct {
	viewer_p C.PetscViewer
}

// Destroy the viewer.
func (v *Viewer) Destroy() {
	errorCode := C.PetscViewerDestroy(&v.viewer_p)
	CheckError(errorCode)
}

// ViewerDrawOpen opens a window for use as a Viewer. If you want to
// do graphics in this window, you must call Draw() and perform the
// graphics on the Draw object.
func ViewerDrawOpen(comm MPI_Comm, display , title string, x, y, w, h int) *Viewer {
	d := C.CString(display)
	defer C.free(unsafe.Pointer(d))
	t := C.CString(title)
	defer C.free(unsafe.Pointer(t))
	v := &Viewer{}
	errorCode := C.PetscViewerDrawOpen(C.MPI_Comm(comm), d, t, C.int(x), C.int(y), C.int(w), C.int(h), &v.viewer_p)
	CheckError(errorCode)
	return v
}

type ViewerFormat C.PetscViewerFormat

var (
	ViewerDefault ViewerFormat = ViewerFormat(C.PETSC_VIEWER_DEFAULT)
	ViewerASCIIMATLAB = ViewerFormat(C.PETSC_VIEWER_ASCII_MATLAB)
	ViewerASCIIMathematica = ViewerFormat(C.PETSC_VIEWER_ASCII_MATHEMATICA)
	ViewerASCIIImpl = ViewerFormat(C.PETSC_VIEWER_ASCII_IMPL)
	ViewerASCIIInfo = ViewerFormat(C.PETSC_VIEWER_ASCII_INFO)
	ViewerASCIIInfoDetail = ViewerFormat(C.PETSC_VIEWER_ASCII_INFO_DETAIL)
	ViewerASCIICommon = ViewerFormat(C.PETSC_VIEWER_ASCII_COMMON)
	ViewerASCIISYMMODU = ViewerFormat(C.PETSC_VIEWER_ASCII_SYMMODU)
	ViewerASCIIIndex = ViewerFormat(C.PETSC_VIEWER_ASCII_INDEX)
	ViewerASCIIDense = ViewerFormat(C.PETSC_VIEWER_ASCII_DENSE)
	ViewerASCIIMatrixMarket = ViewerFormat(C.PETSC_VIEWER_ASCII_MATRIXMARKET)
	ViewerASCIIVTK = ViewerFormat(C.PETSC_VIEWER_ASCII_VTK)
	ViewerASCIIVTKCell = ViewerFormat(C.PETSC_VIEWER_ASCII_VTK_CELL)
	ViewerASCIIVTKCoords = ViewerFormat(C.PETSC_VIEWER_ASCII_VTK_COORDS)
	ViewerASCIIPCICE = ViewerFormat(C.PETSC_VIEWER_ASCII_PCICE)
	ViewerASCIIPython = ViewerFormat(C.PETSC_VIEWER_ASCII_PYTHON)
	ViewerASCIIFactorInfo = ViewerFormat(C.PETSC_VIEWER_ASCII_FACTOR_INFO)
	ViewerASCIILatex = ViewerFormat(C.PETSC_VIEWER_ASCII_LATEX)
	ViewerDrawBasic  = ViewerFormat(C.PETSC_VIEWER_DRAW_BASIC)
	ViewerDrawLG = ViewerFormat(C.PETSC_VIEWER_DRAW_LG)
	ViewerDrawContour = ViewerFormat(C.PETSC_VIEWER_DRAW_CONTOUR)
	ViewerDrawPorts = ViewerFormat(C.PETSC_VIEWER_DRAW_PORTS)
	ViewerVTKVTS = ViewerFormat(C.PETSC_VIEWER_VTK_VTS)
	ViewerVTKVTR = ViewerFormat(C.PETSC_VIEWER_VTK_VTR)
	ViewerVTKVTU = ViewerFormat(C.PETSC_VIEWER_VTK_VTU)
	ViewerBinaryMATLAB = ViewerFormat(C.PETSC_VIEWER_BINARY_MATLAB)
	ViewerNative = ViewerFormat(C.PETSC_VIEWER_NATIVE)
	ViewerHDF5VIZ = ViewerFormat(C.PETSC_VIEWER_HDF5_VIZ)
	ViewerNoFormat = ViewerFormat(C.PETSC_VIEWER_NOFORMAT)
)

// ViewerSetFormat sets the format for PetscViewers.
func (v *Viewer) SetFormat(format ViewerFormat) {
	errorCode := C.PetscViewerSetFormat(v.viewer_p, C.PetscViewerFormat(format))
	CheckError(errorCode)
}

// Prints to a file.
func (v *Viewer) ASCIIPrintf(format string, a ...interface{}) {
	s := fmt.Sprintf(format, a...)
	c := C.CString(s)
	defer C.free(unsafe.Pointer(c))
	//errorCode := C.PetscViewerASCIIPrintf(v.viewer_p, c)
	//CheckError(errorCode)
}

// Allows calls to ASCIISynchronizedPrintf() for this viewer.
func (v *Viewer) ASCIISynchronizedAllow(allow bool) {
	errorCode := C.PetscViewerASCIISynchronizedAllow(v.viewer_p, boolPetsc(allow))
	CheckError(errorCode)
}

// Singleton creates a new Viewer (same type as the old) that lives on a
// single processor.
func (v *Viewer) Singleton() *Viewer {
	viewer := &Viewer{}
	errorCode := C.PetscViewerGetSingleton(v.viewer_p, &viewer.viewer_p)
	CheckError(errorCode)
	return v
}

// RestoreSingleton restores a new Viewer obtained with Singleton.
func (v *Viewer) RestoreSingleton(outviewer *Viewer) {
	errorCode := C.PetscViewerRestoreSingleton(v.viewer_p, &outviewer.viewer_p)
	CheckError(errorCode)
}

// Flush the viewer.
func (v *Viewer) Flush() {
	errorCode := C.PetscViewerFlush(v.viewer_p)
	CheckError(errorCode)
}
