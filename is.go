package petsc

/*
#cgo pkg-config: PETSc
#cgo LDFLAGS: -lpetsc -lmpi

#include "mpi.h"
#include "petscvec.h"
*/
import "C"

import "runtime"

// An index set is a generalization of a subset of integers. Index sets
// are used for defining scatters and gathers.

// PETSc type that allows indexing.
type IS struct {
	is_p C.IS
}

// NewISComm creates an index set object.
func NewIS(comm MPI_Comm) *IS {
	is := &IS{}
	runtime.SetFinalizer(is, destroyIS)
	errorCode := C.ISCreate(C.MPI_Comm(comm), &is.is_p)
	CheckError(errorCode)
	return is
}

// NewISGeneral creates a data structure for an index set containing a
// list of integers.
func NewISGeneral(comm MPI_Comm, idx []int, mode CopyMode) *IS {
	n := len(idx)
	ints := make([]C.PetscInt, n)
	for i, s := range idx {
		ints[i] = C.PetscInt(s)
	}
	is := &IS{}
	runtime.SetFinalizer(is, destroyIS)
	errorCode := C.ISCreateGeneral(C.MPI_Comm(comm), C.PetscInt(n), &ints[0], C.PetscCopyMode(mode), &is.is_p)
	CheckError(errorCode)
	return is
}

// NewISStrideComm creates a data structure for an index set containing a list
// of evenly spaced integers.
func NewISStride(comm MPI_Comm, n, first, step int) *IS {
	is := &IS{}
	runtime.SetFinalizer(is, destroyIS)
	errorCode := C.ISCreateStride(C.MPI_Comm(comm), C.PetscInt(n), C.PetscInt(first), C.PetscInt(step), &is.is_p)
	CheckError(errorCode)
	return is
}

func destroyIS(is *IS) C.PetscErrorCode {
	return C.ISDestroy(&is.is_p)
}

// Destroy destroys an index set.
func (is *IS) Destroy() {
	errorCode := destroyIS(is)
	CheckError(errorCode)
}

// Complement generates the complement index set. That is all all indices
// that are not in the given set.
func (is *IS) Complement(nmin, nmax int) *IS {
	comp := &IS{}
	runtime.SetFinalizer(comp, destroyIS)
	errorCode := C.ISComplement(is.is_p, C.PetscInt(nmin), C.PetscInt(nmax), &comp.is_p)
	CheckError(errorCode)
	return comp
}

// View displays an index set.
func (is *IS) View(viewer *Viewer) {
	errorCode := C.ISView(is.is_p, viewer.viewer_p)
	CheckError(errorCode)
}

// ToGeneral converts an IS of any type to ISGENERAL type.
func (is *IS) ToGeneral() {
	errorCode := C.ISToGeneral(is.is_p)
	CheckError(errorCode)
}

// GeneralSetIndices sets the indices for an ISGENERAL index set.
func (is *IS) GeneralSetIndices(idx []int, mode C.PetscCopyMode) {
	ixx := intSlice(idx)
	errorCode := C.ISGeneralSetIndices(is.is_p, C.PetscInt(len(idx)), &ixx[0], mode)
	CheckError(errorCode)
}
