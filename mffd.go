package petsc

/*
#cgo pkg-config: PETSc
#cgo LDFLAGS: -lpetsc -lmpi

#include "mpi.h"
#include "petscsnes.h"
*/
import "C"
//import "unsafe"

// NewMatMFFD creates a matrix-free matrix.
func NewMatMFFD(comm MPI_Comm, m, n, M, N int) *Mat {
	J := &Mat{}
	errorCode := C.MatCreateMFFD(C.MPI_Comm(comm), C.PetscInt(m), C.PetscInt(n), C.PetscInt(M), C.PetscInt(N), &J.mat_p)
	CheckError(errorCode)
	return J
}

// NewSNESMF creates a matrix-free matrix context for use with
// a SNES solver.  This matrix can be used as the Jacobian argument for
// the routine SetJacobian().
func (snes *SNES) NewMatMF() *Mat {
	J := &Mat{}
	errorCode := C.MatCreateSNESMF(snes.snes_p, &J.mat_p)
	CheckError(errorCode)
	return J
}

type MFFDFunction func(*Vec, *Vec) ErrorCode

// SetFunction sets the function used in applying the matrix free.
func (m *Mat) SetFunction(fn MFFDFunction, ctx interface{}) {
	//m.mffdFn = fn
	//m.mffdCtx = ctx
	//errorCode = C.SetMatMFFDFunction(m.mat_p, unsafe.Pointer(m))
	//CheckError(errorCode)
}
