package main

import (
	"os"
	"fmt"
	"github.com/rwl/petsc"
)

func chkErr(err error) {
	if err != nil {
		fmt.Fprintf(os.Stderr, "petsc: %s\n", err)
		os.Exit(1)
	}
}

func CreateMatrix() *petsc.ZMat {
	x := petsc.NewZMat(petsc.CommSelf)
	x.SetRandom()
	return x
}

func main() {
	petsc.Initialize(os.Args, "", "")

	chkErr(TestNewZMat())

	petsc.Finalize()
}

func TestNewZMat() error {

}
