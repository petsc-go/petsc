package main

import (
	"github.com/rwl/petsc"
	"os"
	"fmt"
	"math"
)

const tol = 1e-10
const n = 2 * 17 * 5

func chkErr(err error) {
	if err != nil {
		fmt.Fprintf(os.Stderr, "petsc: %s\n", err)
		os.Exit(1)
	}
}

func CreateVector() *petsc.Vec {
	x := petsc.NewVecSeq(petsc.CommSelf, n)
	x.SetRandom()
	return x
}

func main() {
	petsc.Initialize(os.Args, "", "")

	chkErr(TestNewVec())
	chkErr(TestNewVecSeq())
	chkErr(TestCopy())
	chkErr(TestDuplicate())
	chkErr(TestMax())
	chkErr(TestMin())
	chkErr(TestDot())
	chkErr(TestNorm())

	petsc.Finalize()
}

func TestNewVec() error {
	x := petsc.NewVec(petsc.CommSelf)
	x.SetSizes(n, petsc.Decide)
	x.SetType(petsc.VecSeq)
	size := x.Size()
	if size != n {
		return fmt.Errorf("vector size expected %d actual %d", n, size)
	}
	x.Destroy()
	return nil
}

func TestNewVecSeq() error {
	x := petsc.NewVecSeq(petsc.CommSelf, n)
	size := x.Size()
	if size != n {
		return fmt.Errorf("vector size expected %d actual %d", n, size)
	}
	x.Destroy()
	return nil
}

func TestCopy() error {
	x := petsc.NewVecSeq(petsc.CommSelf, n)
	value := 6.0
	x.Set(value)
	y := x.Duplicate()
	x.Copy(y)
	copy0 := y.GetValue(0)
	if copy0 != value {
		return fmt.Errorf("copy value expected %g actual %g", value, copy0)
	}
	x.Destroy()
	y.Destroy()
	return nil
}

func TestDuplicate() error {
	x := petsc.NewVecSeq(petsc.CommSelf, n)
	y := x.Duplicate()
	size := y.Size()
	if size != n {
		return fmt.Errorf("vector size expected %d actual %d", n, size)
	}
	x.Destroy()
	y.Destroy()
	return nil
}

func TestMax() error {
	x := petsc.NewVecSeq(petsc.CommSelf, n)
	x.Set(0.0)
	value := 0.7
	x.SetValue(x.Size()/3, value, petsc.InsertValues)
	x.SetValue(x.Size()/2, 0.1, petsc.InsertValues)
	i, max := x.Max()
	if math.Abs(value - max) > tol {
		return fmt.Errorf("max value expected %g actual %g", value, max)
	}
	if x.Size()/3 != i {
		return fmt.Errorf("max index expected %d actual %d", x.Size()/3, i)
	}
	x.Destroy()
	return nil
}

func TestMin() error {
	x := petsc.NewVecSeq(petsc.CommSelf, n)
	x.Set(0.0)
	value := -0.7
	x.SetValue(x.Size()/3, value, petsc.InsertValues)
	x.SetValue(x.Size()/2, -0.1, petsc.InsertValues)
	i, min := x.Min()
	if math.Abs(value - min) > tol {
		return fmt.Errorf("min value expected %g actual %g", value, min)
	}
	if x.Size()/3 != i {
		return fmt.Errorf("expected:%d actual:%d", x.Size()/3, i)
	}
	x.Destroy()
	return nil
}

func TestDot() error {
	x := CreateVector()
	y := CreateVector()
	dot := x.Dot(y)

	xx := x.GetValues(nil)
	yy := y.GetValues(nil)
	expected := 0.0
	for i := 0; i < n; i++ {
		expected += xx[i]*yy[i]
	}
	if math.Abs(expected - dot) > tol {
		return fmt.Errorf("dot expected %g actual %g", expected, dot)
	}
	x.Destroy()
	y.Destroy()
	return nil
}

func TestNorm() error {
	x := petsc.NewVecSeq(petsc.CommSelf, 5)
	x.SetValues([]int{0, 1, 2, 3, 4}, []float64{-2, -1, 0, 2, 4}, petsc.InsertValues)
	norm2 := x.Norm(petsc.Norm2)
	if norm2 != 5.0 {
		return fmt.Errorf("norm-2 expected %g actual %g", 5.0, norm2)
	}
	normInf := x.Norm(petsc.NormInfinity)
	if normInf != 4.0 {
		return fmt.Errorf("norm-inf expected %g actual %g", 4.0, normInf)
	}
	x.Destroy()
	return nil
}

func TestSubVector() error {

}

func TestArray() error {

}

func TestSetValues() error {

}

func TestSetValue() error {

}

func TestGetValue() error {

}

func TestGetValues() error {

}

func TestSetFromOptions() error {

}

func TestSet() error {

}

func TestSetSizes() error {

}

func TestSetType() error {

}

func TestSetRandom() error {

}

func TestOwnershipRange() error {

}

func TestAXPY() error {

}

func TestAXPBY() error {

}

func TestAYPX() error {

}

func TestWAXPY() error {

}

func TestAXPBYPCZ() error {

}

func TestPointwiseMax() error {

}

func TestPointwiseMaxAbs() error {

}

func TestPointwiseMin() error {

}

func TestPointwiseMin() error {

}

func TestPointwiseMult() error {

}

func TestPointwiseDivide() error {

}

func TestMaxPointwiseDivide() error {

}

func TestShift() error {

}

func TestReciprocal() error {

}

func TestPermute() error {

}

func TestSqrtAbs() error {

}

func TestLog() error {

}

func TestExp() error {

}

func TestAbs() error {

}
