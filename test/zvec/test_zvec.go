package main

import (
	"github.com/rwl/petsc"
	"os"
	"fmt"
	"math"
)

const tol = 1e-10
const n = 2 * 17 * 5

func chkErr(err error) {
	if err != nil {
		fmt.Fprintf(os.Stderr, "petsc: %s\n", err)
		os.Exit(1)
	}
}

func CreateVector() *petsc.ZVec {
	x := petsc.NewZVecSeq(petsc.CommSelf, n)
	x.SetRandom()
	return x
}

func main() {
	petsc.Initialize(os.Args, "", "")

	chkErr(TestNewZVec())
	chkErr(TestNewZVecSeq())
//	chkErr(TestCopy())
//	chkErr(TestDuplicate())
//	chkErr(TestMax())
//	chkErr(TestMin())
//	chkErr(TestDot())
//	chkErr(TestNorm())

	petsc.Finalize()
}

func TestNewZVec() error {
	x := petsc.NewZVec(petsc.CommSelf)
	x.SetSizes(n, petsc.Decide)
	x.SetType(petsc.VecSeq)
	size := x.Size()
	if size != n {
		return fmt.Errorf("vector size expected %d actual %d", n, size)
	}
	x.Destroy()
	return nil
}

func TestNewZVecSeq() error {
	x := petsc.NewZVecSeq(petsc.CommSelf, n)
	size := x.Size()
	if size != n {
		return fmt.Errorf("vector size expected %d actual %d", n, size)
	}
	x.Destroy()
	return nil
}

func TestCopy() error {
	x := petsc.NewZVecSeq(petsc.CommSelf, n)
	value := 6.0
	x.Set(value)
	y := x.Duplicate()
	x.Copy(y)
	copy0 := y.GetValue(0)
	if copy0 != value {
		return fmt.Errorf("copy value expected %g actual %g", value, copy0)
	}
	x.Destroy()
	y.Destroy()
	return nil
}

func TestDuplicate() error {
	x := petsc.NewZVecSeq(petsc.CommSelf, n)
	y := x.Duplicate()
	size := y.Size()
	if size != n {
		return fmt.Errorf("vector size expected %d actual %d", n, size)
	}
	x.Destroy()
	y.Destroy()
	return nil
}

func TestSubVector() error {

}

func TestArray() error {

}

func TestSetValues() error {

}

func TestSetValue() error {

}

func TestGetValue() error {

}

func TestGetValues() error {

}

func TestSetFromOptions() error {

}

func TestSet() error {

}

func TestSetSizes() error {

}

func TestSetType() error {

}

func TestSetRandom() error {

}

func TestOwnershipRange() error {

}

func TestAXPY() error {

}

func TestAXPBY() error {

}

func TestAYPX() error {

}

func TestWAXPY() error {

}

func TestAXPBYPCZ() error {

}

func TestPointwiseMax() error {

}

func TestPointwiseMaxAbs() error {

}

func TestPointwiseMin() error {

}

func TestPointwiseMin() error {

}

func TestPointwiseMult() error {

}

func TestPointwiseDivide() error {

}

func TestMaxPointwiseDivide() error {

}

func TestShift() error {

}

func TestReciprocal() error {

}

func TestPermute() error {

}

func TestSqrtAbs() error {

}

func TestLog() error {

}

func TestExp() error {

}

func TestAbs() error {

}
