package petsc

/*
#cgo pkg-config: PETSc
#cgo LDFLAGS: -lpetsc -lmpi

#include "mpi.h"
#include "petscdm.h"
*/
import "C"
import "runtime"

type DMDABoundaryType int//C.DMDABoundaryType

/*var (
	DMDABoundaryNone DMDABoundaryType = C.DMDA_BOUNDARY_NONE
	DMDABoundaryGhosted  = C.DMDA_BOUNDARY_GHOSTED
	DMDABoundaryMirror   = C.DMDA_BOUNDARY_MIRROR
	DMDABoundaryPeriodic = C.DMDA_BOUNDARY_PERIODIC
)*/

var DMDABoundaryTypes = []string{"NONE","GHOSTED","PERIODIC","DMDA_BOUNDARY_"}

type DM struct {
	dm_p C.DM
}

// NewDMDA1D creates an type that will manage the communication of
// one-dimensional regular array data that is distributed across some
// processors.
func NewDMDA1D(comm MPI_Comm, bx DMDABoundaryType, M, dof, s int, lx []int) *DM {
	da := &DM{}
	runtime.SetFinalizer(da, destroyDM)
	//clx := intSlice(lx)
	//errorCode := C.DMDACreate1d(comm, C.DMDABoundaryType(bx), C.PetscInt(M), C.PetscInt(dof), C.PetscInt(s), clx, &da.dm_p)
	//CheckError(errorCode)
	return da
}

func destroyDM(dm *DM) C.PetscErrorCode {
	return C.DMDestroy(&dm.dm_p)
}

// Destroy destroys a vector packer or DMDA.
func (dm *DM) Destroy() {
	errorCode := destroyDM(dm)
	CheckError(errorCode)
}

// View views a vector packer or DMDA.
func (dm *DM) View(v *Viewer) {
	errorCode := C.DMView(dm.dm_p, v.viewer_p)
	CheckError(errorCode)
}

// NewGlobalVector creates a global vector from a DMDA or DMComposite type.
func (dm *DM) NewGlobalVector() *Vec {
	vec := &Vec{}
	errorCode := C.DMCreateGlobalVector(dm.dm_p, &vec.vec_p)
	CheckError(errorCode)
	return vec
}

// NewLocalVector creates a local vector from a DMDA or DMComposite type.
func (dm *DM) NewLocalVector() *Vec {
	vec := &Vec{}
	errorCode := C.DMCreateLocalVector(dm.dm_p, &vec.vec_p)
	CheckError(errorCode)
	return vec
}

// GlobalToLocalBegin begins updating local vectors from global vector.
func (dm *DM) GlobalToLocalBegin(g *Vec, mode C.InsertMode, l *Vec) {
	errorCode := C.DMGlobalToLocalBegin(dm.dm_p, g.vec_p, mode, l.vec_p)
	CheckError(errorCode)
}

// GlobalToLocalEnd ends updating local vectors from global vector.
func (dm *DM) GlobalToLocalEnd(g *Vec, mode C.InsertMode, l *Vec) {
	errorCode := C.DMGlobalToLocalEnd(dm.dm_p, g.vec_p, mode, l.vec_p)
	CheckError(errorCode)
}

// LocalToGlobalBegin updates global vectors from local vectors.
func (dm *DM) LocalToGlobalBegin(l *Vec, mode C.InsertMode, g *Vec) {
	errorCode := C.DMLocalToGlobalBegin(dm.dm_p, l.vec_p, mode, g.vec_p)
	CheckError(errorCode)
}

// LocalToGlobalEnd updates global vectors from local vectors.
func (dm *DM) LocalToGlobalEnd(l *Vec, mode C.InsertMode, g *Vec) {
	errorCode := C.DMLocalToGlobalEnd(dm.dm_p, l.vec_p, mode, g.vec_p)
	CheckError(errorCode)
}

// ISLocalToGlobalMapping maps from an arbitrary local ordering from
// 0 to n-1 to a global PETSc ordering used by a vector or matrix.
type ISLocalToGlobalMapping struct {
	ltog_p C.ISLocalToGlobalMapping
}

// View a local to global mapping.
func (is *ISLocalToGlobalMapping) View(viewer *Viewer) {
	errorCode := C.ISLocalToGlobalMappingView(is.ltog_p, viewer.viewer_p)
	CheckError(errorCode)
}

// LocalToGlobalMapping accesses the local-to-global mapping in a DM.
func (dm *DM) LocalToGlobalMapping() *ISLocalToGlobalMapping {
	ltog := &ISLocalToGlobalMapping{}
	errorCode := C.DMGetLocalToGlobalMapping(dm.dm_p, &ltog.ltog_p)
	CheckError(errorCode)
	return ltog
}
